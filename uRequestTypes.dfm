object frmRequestTypes: TfrmRequestTypes
  Left = 0
  Top = 0
  Caption = 'Request Types'
  ClientHeight = 305
  ClientWidth = 505
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 268
    Width = 505
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      505
      37)
    object Button1: TButton
      Left = 417
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Close'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
  end
  object grdRequestTypes: TcxGrid
    Left = 0
    Top = 0
    Width = 505
    Height = 268
    Align = alClient
    TabOrder = 1
    object dbtvRequestTypes: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Append.Visible = True
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = dsRequestTypes
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <
        item
          Links = <
            item
            end>
          SummaryItems = <
            item
              Kind = skCount
            end>
        end>
      OptionsData.Appending = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object dbtvRequestTypesType: TcxGridDBColumn
        DataBinding.FieldName = 'Type'
        Width = 50
      end
    end
    object grdRequestTypesLevel1: TcxGridLevel
      GridView = dbtvRequestTypes
    end
  end
  object mtRequestTypes: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 336
    Top = 24
    object mtRequestTypesType: TStringField
      FieldName = 'Type'
      Size = 50
    end
  end
  object dsRequestTypes: TDataSource
    DataSet = mtRequestTypes
    Left = 336
    Top = 72
  end
end
