unit uParamGroups;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.StorageBin, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Stan.StorageJSON, dxSkinsForm,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkroom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinOffice2019Colorful, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringtime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinTheBezier, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, Vcl.ExtCtrls, Vcl.StdCtrls, Spring,
  Spring.Collections;

type
  TfParamGroups = class(TForm)
    mtGroup: TFDMemTable;
    mtGroupGroup: TStringField;
    mtGroupRemark: TMemoField;
    mtGroupID: TIntegerField;
    mtParam: TFDMemTable;
    mtParamegroup_id: TIntegerField;
    mtParamName: TStringField;
    tvGroup: TcxGridDBTableView;
    grdDefLevel1: TcxGridLevel;
    grdDef: TcxGrid;
    dsGroup: TDataSource;
    tvGroupid: TcxGridDBColumn;
    tvGroupGroup: TcxGridDBColumn;
    tvGroupRemark: TcxGridDBColumn;
    dsParam: TDataSource;
    tvParam: TcxGridDBTableView;
    tvParamGroupID: TcxGridDBColumn;
    tvParamName: TcxGridDBColumn;
    tvParamValue: TcxGridDBColumn;
    grdDefLevel2: TcxGridLevel;
    tvParamid: TcxGridDBColumn;
    mtParamid: TIntegerField;
    btnUse: TButton;
    Button2: TButton;
    Panel1: TPanel;
    mtParamvalue: TStringField;
    btnAddFields: TButton;
    btnRemove: TButton;
    procedure btnAddFieldsClick(Sender: TObject);
    procedure btnRemoveClick(Sender: TObject);
    procedure btnUseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure mtParamAfterInsert(DataSet: TDataSet);
  private
    FAllFields: IList<string>;
    FAddParams: IList<string>;
    FRemoveParams: IList<string>;
  public
    class function Run(AAddParams, ARemoveParams, AAllFields: IList<string>): Boolean;
  end;

implementation

uses
  System.JSON, System.JSON.Serializers, System.IOUtils, cxGridDBDataDefinitions, uRecords,
  uAddFields {, uLooper}, uAPIInProg;

const
  PARAM_GROUPS_FILE_NAME = 'API_InProg_Groups.json';

{$R *.dfm}


procedure TfParamGroups.btnAddFieldsClick(Sender: TObject);
var
  FieldsChecks: IList<Tuple<string, string, Boolean>>;
  bmk: TBookmark;
  ft: Tuple<string, string, Boolean>;
  FieldName: string;
begin
  FieldsChecks := TCollections.CreateList<Tuple<string, string, Boolean>>;
  bmk := mtParam.Bookmark;
  mtParam.DisableControls;
  try
    FAllFields.ForEach(
      procedure(const e: string)
      var
        FieldValue: string;
      begin
        FieldName := e.Substring(e.IndexOf('.')+1);
        //FieldValue := STokenBegin + UpperCase(e) + STokenEnd;
        FieldValue := STokenBegin + e + STokenEnd;
        ///FieldsChecks.Add(Tuple.Create<string, Boolean>(UpperCase(e), mtParam.Locate('group_id;name',
        FieldsChecks.Add(Tuple.Create<string, string, Boolean>(e, FieldName, mtParam.Locate('group_id;value',
          VarArrayOf([mtGroupID.AsLargeInt, FieldValue]), [loCaseInsensitive])));
      end);

    if not TfAddFields.Run(FieldsChecks) then
      Exit;

    for ft in FieldsChecks do
    begin
      ///FieldName := PascalCase(ft.Value1.Substring(ft.Value1.IndexOf('.')+1));
      if ft.Value3
        and not mtParam.Locate('group_id;name', VarArrayOf([mtGroupID.AsLargeInt, ft.Value2]), []) then
      begin
        mtParam.Append;
        mtParamName.AsString := ft.Value2;
        //mtParamvalue.AsString := STokenBegin + UpperCase(ft.Value1) + STokenEnd;
        mtParamvalue.AsString := STokenBegin + ft.Value1 + STokenEnd;
        mtParam.Post;
      end;

      if not ft.Value3
        //and mtParam.Locate('group_id;name', VarArrayOf([mtGroupID.AsLargeInt, FieldName]), []) then
        and mtParam.Locate('group_id;value', VarArrayOf([mtGroupID.AsLargeInt, ft.Value1]), []) then
      begin
        mtParam.Delete;
      end;
    end;
  finally
    mtParam.Bookmark := bmk;
    mtParam.EnableControls;
  end;
end;

procedure TfParamGroups.btnUseClick(Sender: TObject);
var
  mi: Integer;
  adc: TcxGridDBDataController;
  i: Integer;
begin
  if mtGroup.State in [dsEdit, dsInsert] then
    mtGroup.Post;
  if mtParam.State in [dsEdit, dsInsert] then
    mtParam.Post;

  if mtGroup.IsEmpty then
    abort;

  mi := tvGroup.DataController.FocusedRecordIndex;
  adc := tvGroup.DataController.GetDetailDataController(mi, 0) as TcxGridDBDataController;
  for i := 0 to adc.RecordCount - 1 do
    FAddParams.Add(Format('%s=%s', [VarToStr(adc.Values[i, tvParamName.Index]),
      VarToStr(adc.Values[i, tvParamValue.Index])]));
  ModalResult := mrOk;
end;

procedure TfParamGroups.btnRemoveClick(Sender: TObject);
var
  mi: Integer;
  adc: TcxGridDBDataController;
  i: Integer;
begin
  if mtGroup.State in [dsEdit, dsInsert] then
    mtGroup.Post;
  if mtParam.State in [dsEdit, dsInsert] then
    mtParam.Post;

  if mtGroup.IsEmpty then
    abort;

  mi := tvGroup.DataController.FocusedRecordIndex;
  adc := tvGroup.DataController.GetDetailDataController(mi, 0) as TcxGridDBDataController;
  for i := 0 to adc.RecordCount - 1 do
    FRemoveParams.Add(Format('%s=%s', [VarToStr(adc.Values[i, tvParamName.Index]),
      VarToStr(adc.Values[i, tvParamValue.Index])]));
  ModalResult := mrOk;
end;


procedure TfParamGroups.FormClose(Sender: TObject; var Action: TCloseAction);
var
  js: TJSONObject;
begin
  mtGroup.MergeChangeLog;
  mtParam.MergeChangeLog;
  js := TJSONObject.Create;
  try
    js.AddPair(TJSONPair.Create('group', MT2JS(mtGroup)));
    js.AddPair(TJSONPair.Create('param', MT2JS(mtParam)));
    TFile.WriteAllText(PARAM_GROUPS_FILE_NAME, js.ToString);
  finally
    js.Free;
  end;
end;

procedure TfParamGroups.FormCreate(Sender: TObject);
var
  js: TJSONObject;
begin
  if TFile.Exists(PARAM_GROUPS_FILE_NAME) then
    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(PARAM_GROUPS_FILE_NAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js.Values['group'] as TJSONObject, mtGroup);
        JS2MT(js.Values['param'] as TJSONObject, mtParam);
      finally
        js.Free;
      end;
    except
      on e: Exception do
        MessageDlg('An error occured while loading "Groups" and/or "Parameters" dataset. Error: '+ e.Message,
                   TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
    end;

  mtGroup.Active := True;
  mtParam.Active := True;
  mtParam.First;
  while not mtParam.Eof do
  begin
    if mtParamid.IsNull then
    begin
      mtParam.Edit;
      mtParamid.AsInteger := mtParam.RecNo;
      mtParam.Post;
    end;
    mtParam.Next;
  end;
end;

procedure TfParamGroups.mtParamAfterInsert(DataSet: TDataSet);
begin
  DataSet['group_id'] := mtGroup['id'];
end;

class function TfParamGroups.Run(AAddParams, ARemoveParams, AAllFields: IList<string>): Boolean;
begin
  with Create(nil) do
    try
      FAllFields := AAllFields;
      FAddParams := AAddParams;
      FAddParams.Clear;
      FRemoveParams := ARemoveParams;
      FRemoveParams.Clear;
      Result := ShowModal = mrOk;
    finally
      Free;
    end;
end;

end.
