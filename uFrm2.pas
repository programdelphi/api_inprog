unit uFrm2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids;

type
  TForm2 = class(TForm)
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    FDMemTable1: TFDMemTable;
    DBGrid2: TDBGrid;
    FDMemTable2: TFDMemTable;
    DataSource2: TDataSource;
    Panel1: TPanel;
    DBGrid3: TDBGrid;
    DataSource3: TDataSource;
    FDMemTable3: TFDMemTable;
    btnDemoAPI1: TButton;
    Button1: TButton;
    btnSetup: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnSetupClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation
uses
uGlobal , uAPIInProg;

{$R *.dfm}



procedure TForm2.btnSetupClick(Sender: TObject);
begin
  uAPIInProg.Show_API_SetupForm(self);
end;

procedure TForm2.FormShow(Sender: TObject);
begin
  FillData(FDMemTable1);
  FillData(FDMemTable2);
  FillData(FDMemTable3);
end;

end.
