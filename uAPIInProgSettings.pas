unit uAPIInProgSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, dxDateRanges, dxScrollbarAnnotations, Data.DB,
  cxDBData, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxImageComboBox, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxLabel, Vcl.StdCtrls, cxGroupBox, Vcl.Samples.Spin,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxMemo, cxDBEdit, cxCheckBox, cxSpinEdit, Vcl.Menus,
  cxButtons, uRecords, dxScreenTip, dxCustomHint, cxHint, uRestAPIQueueeThread, cxDBLabel, dxLayoutContainer, dxLayoutControl,
  Vcl.ExtCtrls, dxBarBuiltInMenu, cxGridCustomPopupMenu, cxGridPopupMenu, APIJobAssignments;

type
  TfrmAPIInProgSettings = class(TForm)
    pnlSettings: TcxGroupBox;
    lblCaption: TcxLabel;
    lblType: TcxLabel;
    lblID: TcxLabel;
    lblGroupID: TcxLabel;
    lblOrder: TcxLabel;
    lblActive: TcxLabel;
    lblTimeOut: TcxLabel;
    lblResponseType: TcxLabel;
    mtAPIJobs: TFDMemTable;
    grdAPIJobs: TcxGrid;
    grdAPIJobsDBTV: TcxGridDBTableView;
    grdAPIJobsL: TcxGridLevel;
    mtAPIJobsFormClass: TStringField;
    mtAPIJobsCaption: TStringField;
    mtAPIJobsID: TGuidField;
    mtAPIJobsGroupID: TStringField;
    mtAPIJobsType: TStringField;
    mtAPIJobsResponseType: TStringField;
    mtAPIJobsActive: TBooleanField;
    mtAPIJobsOrder: TSmallintField;
    mtAPIJobsTimeout: TIntegerField;
    mtAPIJobsLocationHost: TStringField;
    gbButtons: TcxGroupBox;
    dteCaption: TcxDBTextEdit;
    dsAPIJobs: TDataSource;
    grdAPIJobsDBTVFormClass: TcxGridDBColumn;
    grdAPIJobsDBTVCaption: TcxGridDBColumn;
    grdAPIJobsDBTVID: TcxGridDBColumn;
    grdAPIJobsDBTVGroupID: TcxGridDBColumn;
    grdAPIJobsDBTVType: TcxGridDBColumn;
    grdAPIJobsDBTVResponseType: TcxGridDBColumn;
    grdAPIJobsDBTVActive: TcxGridDBColumn;
    grdAPIJobsDBTVOrder: TcxGridDBColumn;
    grdAPIJobsDBTVTimeout: TcxGridDBColumn;
    grdAPIJobsDBTVLocationHost: TcxGridDBColumn;
    mtAPIJobsRequestResource: TStringField;
    mtAPIJobsRequestParams: TStringField;
    mtAPIJobsSQL: TStringField;
    grdAPIJobsDBTVRequestResource: TcxGridDBColumn;
    dteID: TcxDBTextEdit;
    dteGroupID: TcxDBTextEdit;
    dcbType: TcxDBComboBox;
    dseOrder: TcxDBSpinEdit;
    dseTimeOut: TcxDBSpinEdit;
    dchbActive: TcxDBCheckBox;
    dcbResponseType: TcxDBComboBox;
    btnEditTypes: TcxButton;
    btnAdd: TcxButton;
    btnPost: TcxButton;
    btnCancel: TcxButton;
    btnDelete: TcxButton;
    btnReload: TcxButton;
    btnSave: TcxButton;
    cxHintStyleController1: TcxHintStyleController;
    btnShowTranslated: TcxButton;
    btnRunRequest: TcxButton;
    lblTranslatedText: TLabel;
    mmTranslatedRequest: TMemo;
    btnUIJobAssignments: TcxButton;
    mtAPIJobsSendsWindowsNotification: TBooleanField;
    grdAPIJobsDBTVSendsWindowsNotification: TcxGridDBColumn;
    pnlBottom: TPanel;
    pnlLocation: TcxGroupBox;
    dteLocationURL: TcxDBTextEdit;
    gbRequestResource: TcxGroupBox;
    dteResource: TcxDBTextEdit;
    btnSelectResAndPar: TcxButton;
    gbRequestParams: TcxGroupBox;
    dmmParameters: TcxDBMemo;
    btnAddAll: TcxButton;
    btnAddGroups: TcxButton;
    gbSQL: TcxGroupBox;
    dmmSQL: TcxDBMemo;
    mtAPIJobResponseFields: TFDMemTable;
    mtAPIJobResponseFieldsFormClass: TStringField;
    mtAPIJobResponseFieldsJobID: TGuidField;
    mtAPIJobResponseFieldsResponseFieldName: TStringField;
    mtAPIJobResponseFieldsShowInResponseGrid: TBooleanField;
    mtAPIJobResponseFieldsOrderNo: TSmallintField;
    dsAPIJobResponseFields: TDataSource;
    gbResponseSettings: TcxGroupBox;
    gbResponseFields: TcxGroupBox;
    grdResponseFields: TcxGrid;
    grdResponseFieldsDBTV: TcxGridDBTableView;
    grdResponseFieldsDBTVResponseFieldName: TcxGridDBColumn;
    grdResponseFieldsDBTVOrderNo: TcxGridDBColumn;
    grdResponseFieldsDBTVShowInResponseGrid: TcxGridDBColumn;
    grdResponseFieldsL: TcxGridLevel;
    mtAPIJobsSuccessNotificationTitle: TStringField;
    mtAPIJobsErrorNotificationTitle: TStringField;
    mtAPIJobsSuccessNotificationHeader: TStringField;
    mtAPIJobsErrorNotificationHeader: TStringField;
    mtAPIJobsSuccessNotificationMessage: TStringField;
    mtAPIJobsErrorNotificationMessage: TStringField;
    mtAPIJobsResponseGridWidth: TIntegerField;
    mtAPIJobsResponseGridHeight: TIntegerField;
    Label3: TLabel;
    dchbSendsWindowsNotification: TcxDBCheckBox;
    btnNotificationSettings: TcxButton;
    gbResponseGridSize: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxDBSpinEdit2: TcxDBSpinEdit;
    mtAPIJobResponseFieldsResponseGridColumnWidth: TIntegerField;
    grdResponseFieldsDBTVResponseGridColumnWidth: TcxGridDBColumn;
    grdAPIJobsDBTVRequestParams: TcxGridDBColumn;
    grdAPIJobsDBTVSuccessNotificationTitle: TcxGridDBColumn;
    grdAPIJobsDBTVSuccessNotificationHeader: TcxGridDBColumn;
    grdAPIJobsDBTVSuccessNotificationMessage: TcxGridDBColumn;
    grdAPIJobsDBTVErrorNotificationTitle: TcxGridDBColumn;
    grdAPIJobsDBTVErrorNotificationHeader: TcxGridDBColumn;
    grdAPIJobsDBTVErrorNotificationMessage: TcxGridDBColumn;
    grdAPIJobsDBTVResponseGridWidth: TcxGridDBColumn;
    grdAPIJobsDBTVResponseGridHeight: TcxGridDBColumn;
    procedure btnEditTypesClick(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnPostClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnReloadClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure mtAPIJobsNewRecord(DataSet: TDataSet);
    procedure btnSelectResAndParClick(Sender: TObject);
    procedure mtAPIJobsAfterPost(DataSet: TDataSet);
    procedure mtAPIJobsAfterDelete(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnAddAllClick(Sender: TObject);
    procedure btnAddGroupsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dmmSQLMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure dmmSQLMouseLeave(Sender: TObject);
    procedure mtAPIJobsAfterEdit(DataSet: TDataSet);
    procedure mtAPIJobsAfterCancel(DataSet: TDataSet);
    procedure mtAPIJobsAfterInsert(DataSet: TDataSet);
    procedure btnShowTranslatedClick(Sender: TObject);
    procedure btnRunRequestClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnUIJobAssignmentsClick(Sender: TObject);
    procedure mtAPIJobResponseFieldsAfterEdit(DataSet: TDataSet);
    procedure mtAPIJobResponseFieldsAfterPost(DataSet: TDataSet);
    procedure mtAPIJobResponseFieldsAfterDelete(DataSet: TDataSet);
    procedure mtAPIJobResponseFieldsAfterCancel(DataSet: TDataSet);
    procedure mtAPIJobsBeforePost(DataSet: TDataSet);
    procedure mtAPIJobsBeforeDelete(DataSet: TDataSet);
    procedure btnNotificationSettingsClick(Sender: TObject);
  private
    { Private declarations }
    RequestFormConn: TFDConnection;
    procedure LoadTypes;
    procedure LoadSettings;
    procedure SaveSettings;
    function GetCurrentParams: IDictionaryStringString;
    function GetDomainFieldsList: IListString;
    function GetCurrentFieldParams: IDictionaryStringString;
  public
    { Public declarations }
    RequestForm: TForm;
  end;

var
  frmAPIInProgSettings: TfrmAPIInProgSettings;

const
  STokenRECNO = '##RECNO##';
  STokenBegin = '##';
  STokenEnd = '##';
  STokenFunc = '##!';
  SParamSeparator = '&';

implementation

{$R *.dfm}

uses
  System.Types, System.JSON, System.IOUtils, System.StrUtils, System.Generics.Collections, uRequestTypes,
  uResAndPar, uParamGroups, uAPIInProg, uAddFields, Spring, Spring.Collections, uAPIJobNotificationSettings;

procedure TfrmAPIInProgSettings.btnAddAllClick(Sender: TObject);
var
  FieldsChecks: IList<Tuple<string, string, Boolean>>;
  AllFields: IListString;
  Params: IListString;
  Current: IDictionaryStringString;
  //fld: TField;
  ns, {sql,} ParamName: string;
  Exists: Boolean;
begin
  Params := TCollections.CreateList<string>(SplitString(mtAPIJobsRequestParams.AsString, SParamSeparator));
  Params.RemoveAll(
    function(const e: string): Boolean
    begin
      Result := e = '';
    end);

  Current := GetCurrentFieldParams();

  AllFields := GetDomainFieldsList;

  FieldsChecks := TCollections.CreateList<Tuple<string, string, Boolean>>();
  AllFields.ForEach(
    procedure(const f: string)
    begin
      //FieldsChecks.Add(Tuple.Create<string, string, Boolean>(f, f, Params.Any(
      //  function(const p, f: string): Boolean
      //  begin
      //    //Result := Pos(STokenBegin + UpperCase(f) + STokenEnd, p) <> 0;
      //    Result := Pos(STokenBegin + f + STokenEnd, p) <> 0;
      //  end)));
      Exists := Current.ContainsKey(STokenBegin + f + STokenEnd);
      if Exists then
        ParamName := Current[STokenBegin + f + STokenEnd]
      else
        ParamName := f.Substring(f.IndexOf('.')+1); // Field name without domain name


      FieldsChecks.Add(Tuple.Create<string, string, Boolean>(f, ParamName, Exists));
    end);

  if not TfAddFields.Run(FieldsChecks) then
    Exit;

  ns := '';
  // all other params first
  Params.Where(
    function(const p: string): Boolean
    begin
      Result := AllFields.All(
        function(const f: string): Boolean
        begin
          //Result := Pos(STokenBegin + UpperCase(f) + STokenEnd, p) = 0;
          Result := Pos(STokenBegin + f + STokenEnd, p) = 0;
        end)
    end).ForEach(
    procedure(const p: string)
    begin
      ns := ns + SParamSeparator + p;
    end);

  // from checks
  FieldsChecks.Where(
    function(const T: Tuple<string, string, Boolean>): Boolean
    begin
      Result := T.Value3;
    end).ForEach(
    procedure(const T: Tuple<string, string, Boolean>)
    //var
    //  FieldName: string;
    begin
      //ns := ns + SParamSeparator + PascalCase(T.Value1) + '=' + STokenBegin + UpperCase(T.Value1) + STokenEnd;
      //FieldName := T.Value1.Substring(T.Value1.IndexOf('.')+1);
      ns := ns + SParamSeparator + PascalCase(T.Value2) + '=' + STokenBegin + T.Value1 + STokenEnd;
    end);

  if ns <> '' then
    Delete(ns, 1, Length(SParamSeparator));

  if not (mtAPIJobs.State in [dsInsert, dsEdit]) then
    mtAPIJobs.Edit;

  mtAPIJobsRequestParams.AsString := ns;
end;

procedure TfrmAPIInProgSettings.btnAddClick(Sender: TObject);
begin
  if mtAPIJobs.State in [TDataSetState.dsInsert, dsEdit] then
    raise System.SysUtils.Exception.Create('The changes in the request settings record are not posted. Please post or cancel the changes in the record!');
  mtAPIJobs.Insert;
end;

procedure TfrmAPIInProgSettings.btnAddGroupsClick(Sender: TObject);
var
  AddParams: IListString;
  RemoveParams: IListString;
  Current: IListString;
  AllFields: IListString;
  //fld: TField;
  np: string;
  s: string;
  //ADictRestAPIDomain : TDictionary<string, TDictionary<string, TField>>;
  ///ADictRestAPIDomainItem: TPair<string, TDictionary<string, TField>>;
  //ADictRestAPIField : TDictionary<string, TField>;
  //ADictRestAPIFieldItem : TPair<string, TField>;
begin
  AllFields := GetDomainFieldsList;

  Current := TCollections.CreateList<string>(SplitString(mtAPIJobsRequestParams.AsString, SParamSeparator));
  AddParams := TCollections.CreateList<string>;
  RemoveParams := TCollections.CreateList<string>;

  if TfParamGroups.Run(AddParams, RemoveParams, AllFields) then
  begin

    // add params from current if not exist
    if AddParams.Count > 0 then
    begin
      Current.RemoveAll(
        function(const c: string): Boolean
        begin
          Result := AddParams.Any(
            function(const p: string): Boolean
            begin
              Result := ParamToPair(c).Key = ParamToPair(p).Key;
            end);
        end);
      Current.AddRange(AddParams);
    end;

    // remove params
    if RemoveParams.Count > 0 then
    begin
      Current.RemoveAll(
        function(const c: string): Boolean
        begin
          Result := RemoveParams.Any(
            function(const p: string): Boolean
            begin
              Result := ParamToPair(c).Key = ParamToPair(p).Key;
            end)
        end);
    end;

    np := '';
    for s in Current do
      np := np + SParamSeparator + s;
    if np <> '' then
      Delete(np, 1, Length(SParamSeparator));

    if not (mtAPIJobs.State in [dsInsert, dsEdit]) then
      mtAPIJobs.Edit;

    mtAPIJobsRequestParams.AsString := np;
  end;
end;

procedure TfrmAPIInProgSettings.btnCancelClick(Sender: TObject);
begin
  if (mtAPIJobs.RecordCount = 0) then
    if mtAPIJobs.State <> dsInsert then
      raise Exception.Create('The record is not in insert or edit state!')
    else
  else
  if not (mtAPIJobs.State in [TDataSetState.dsInsert, dsEdit]) then
    raise Exception.Create('The record is not in insert or edit state!');
  mtAPIJobs.Cancel;
end;

procedure TfrmAPIInProgSettings.btnDeleteClick(Sender: TObject);
begin
  if mtAPIJobs.State in [TDataSetState.dsInsert, dsEdit] then
    raise Exception.Create('The changes in the request settings record are not posted. Please post or cancel the changes in the record!');

  if (mtAPIJobs.RecordCount = 0) then
    raise Exception.Create('There is no record to delete!');

  mtAPIJobs.Delete;
end;

procedure TfrmAPIInProgSettings.btnEditTypesClick(Sender: TObject);
begin
  TfrmRequestTypes.Edit;
  LoadTypes;
end;

procedure TfrmAPIInProgSettings.btnNotificationSettingsClick(Sender: TObject);
begin
  frmAPIJobNotificationSettings := TfrmAPIJobNotificationSettings.Create(self);
  try
    frmAPIJobNotificationSettings.dsNotificationSettings.DataSet := mtAPIJobs;
    frmAPIJobNotificationSettings.JobID := mtAPIJobsID.AsGuid;
    frmAPIJobNotificationSettings.SourceForm := RequestForm;
    frmAPIJobNotificationSettings.dcbCustomizeNotification.Visible := False;
    frmAPIJobNotificationSettings.dcbCustomizeNotification.DataBinding.DataSource := nil;
    frmAPIJobNotificationSettings.ShowModal;
  finally
    frmAPIJobNotificationSettings.Free;
  end;
end;

procedure TfrmAPIInProgSettings.btnPostClick(Sender: TObject);
begin
  if not (mtAPIJobs.State in [TDataSetState.dsInsert, dsEdit]) then
    raise Exception.Create('There is nothing to be posted!');
  mtAPIJobs.Post;
end;

procedure TfrmAPIInProgSettings.btnReloadClick(Sender: TObject);
begin
  LoadSettings;
  btnSave.Enabled := False;
end;

procedure TfrmAPIInProgSettings.btnSaveClick(Sender: TObject);
begin
  SaveSettings;
  btnSave.Enabled := False;
end;

procedure TfrmAPIInProgSettings.btnSelectResAndParClick(Sender: TObject);
var
  Resource: string;
  Params: IListString;
  Current: IDictionaryStringString;
  s: string;
  pair: TPair<string, string>;
begin
  if not (mtAPIJobs.State in [dsInsert, dsEdit]) then
    mtAPIJobs.Edit;

  Params := TCollections.CreateList<string>;

  if TfResAndPar.List(mtAPIJobsRequestParams.AsString, Resource, Params) then
  begin
    mtAPIJobsRequestResource.AsString := Resource;

    Current := GetCurrentParams();
    Params.ForEach(
      procedure(const n: string)
      var
        p: Integer;
      begin
        p := Pos('=', n);
        if p < 0 then
          Exit;
        Current[Copy(n, 1, p - 1)] := Copy(n, p + 1);
      end);

    s := '';
    for pair in Current do
      s := s + pair.Key + '=' + pair.Value + '&';

    while RightStr(s, 1) = '&' do
      Delete(s, Length(s), 1);

    mtAPIJobsRequestParams.AsString := s;
  end;
end;

procedure TfrmAPIInProgSettings.btnShowTranslatedClick(Sender: TObject);
var
  s: string;
begin
  s :=
    TdmAPIInProg.StringTranslate(dteLocationURL.Text + dteResource.Text + '?' + dmmParameters.Text,
                                     frmAPIInProgSettings.RequestForm, dmmSQL.Text);
  mmTranslatedRequest.Text := s;
  mmTranslatedRequest.SelectAll;
  mmTranslatedRequest.SetFocus;

//  try
//    UseLatestCommonDialogs := false;
//    ShowMessage(s);
//  finally
//    UseLatestCommonDialogs := true;
//  end;
end;

procedure TfrmAPIInProgSettings.btnUIJobAssignmentsClick(Sender: TObject);
begin
  frmAPIJobAssignments := TfrmAPIJobAssignments.Create(self);
  try
    frmAPIJobAssignments.frmAPIInProgSettings := self;
    frmAPIJobAssignments.ShowModal;
  finally
    frmAPIJobAssignments.Free;
  end;
end;

procedure TfrmAPIInProgSettings.btnRunRequestClick(Sender: TObject);
begin
  if btnSave.Enabled or (mtAPIJobs.State in [dsInsert, dsEdit]) then
    raise Exception.Create('Before running request, changes in the form must be saved!');
  dmAPIInProg.btnAPIJobClick(Sender);
end;

procedure TfrmAPIInProgSettings.dmmSQLMouseLeave(Sender: TObject);
begin
  cxHintStyleController1.HideHint;
end;

procedure TfrmAPIInProgSettings.dmmSQLMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if dmmSQL.Hint <> '' then
    with dmmSQL.ClientToScreen(Point(X, Y)) do
    cxHintStyleController1.ShowHint(X, Y, '', dmmSQL.Hint);
end;

procedure TfrmAPIInProgSettings.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if (btnSave.Enabled or (mtAPIJobs.State in [dsInsert, dsEdit])) and (MessageBox(0, PChar('Save changes ?'), PChar('Close'), MB_ICONWARNING or MB_YESNO) = ID_YES) then
  begin
    if (mtAPIJobs.State in [dsInsert, dsEdit])  then
      mtAPIJobs.Post;
    btnSave.Click;
    CanClose := CanClose and not btnSave.Enabled;
    if CanClose then
      Close;
  end;
end;

procedure TfrmAPIInProgSettings.FormCreate(Sender: TObject);
var
  grdAPIStatus: TcxGrid;
  grdPos: TPoint;
begin
  if not Assigned(dmAPIInProg ) then
    dmAPIInProg := TdmAPIInProg.Create(Application);

  PrepareRequestResultControls(self, grdAPIStatus);

  grdPos := Self.ScreenToClient(TCustomButton(btnRunRequest).ClientToScreen(Point(1, TCustomButton(btnRunRequest).Height)));

  if grdPos.Y + grdAPIStatus.Height > Self.Height then
    grdPos.Y := grdPos.Y - TCustomButton(Sender).Height - grdAPIStatus.Height;

  if grdPos.X + grdAPIStatus.Width > Self.Width then
    grdPos.X := grdPos.X - grdAPIStatus.Width;

  grdAPIStatus.Left := grdPos.X;
  grdAPIStatus.Top := grdPos.Y;
  //grdAPIStatus.Visible := False;
end;

procedure TfrmAPIInProgSettings.FormShow(Sender: TObject);
begin
  LoadSettings;

  if (not DictFormRestAPIConnection.TryGetValue(RequestForm, RequestFormConn)) or (RequestFormConn = nil)  then
  begin
    dmmSQL.Properties.ReadOnly := RequestFormConn = nil;
    dmmSQL.Hint := 'Connection is not registered related to form. SQL can not be edited!';
  end;
end;

function TfrmAPIInProgSettings.GetCurrentParams: IDictionaryStringString;
var
  r: IDictionaryStringString;
begin
  r := TCollections.CreateDictionary<string, string>();
  TCollections.CreateList<string>(SplitString(mtAPIJobsRequestParams.AsString, SParamSeparator)).ForEach(
    procedure(const n: string)
    var
      p: Integer;
    begin
      p := Pos('=', n);
      if p > 0 then
        r[Copy(n, 1, p - 1)] := Copy(n, p + 1);
    end
    );
  Result := r;
end;

function TfrmAPIInProgSettings.GetCurrentFieldParams: IDictionaryStringString;
var
  r: IDictionaryStringString;
begin
  r := TCollections.CreateDictionary<string, string>();
  TCollections.CreateList<string>(SplitString(mtAPIJobsRequestParams.AsString, SParamSeparator)).ForEach(
    procedure(const n: string)
    var
      p: Integer;
    begin
      p := Pos('=', n);
      if p > 0 then
        //r[Copy(n, 1, p - 1)] := Copy(n, p + 1);
        r[Copy(n, p + 1)] := Copy(n, 1, p - 1);
    end
    );
  Result := r;
end;

function TfrmAPIInProgSettings.GetDomainFieldsList: IListString;
var
  ADictRestAPIDomain : TDictionary<string, TDictionary<string, TField>>;
  //ADictRestAPIDomainItem: TPair<string, TDictionary<string, TField>>;
  ADictRestAPIField : TDictionary<string, TField>;
  //ADictRestAPIFieldItem : TPair<string, TField>;
  Query:  TFDQuery;
  sql, ch, expr, prevexpr: string;
  fld: TField;
  DomainList, Fldlist: TList<string>;
  i, j, idx, BracketCnt, whereidx, whereendidx, groupbyidx, orderbyidx: Integer;
  DomainName, FldName: string;
begin
  Result := TCollections.CreateList<string>();

//  for fld in mtData.Fields do
//    if fld.FieldName <> SFieldAPIStatus then
//      Fields.Add(fld.FieldName);

  if (RequestForm <> nil) and (DictFormRestAPIDomain <> nil) then
  begin
    if DictFormRestAPIDomain.TryGetValue(RequestForm, ADictRestAPIDomain) then
    begin
      DomainList := TList<string>.Create(ADictRestAPIDomain.Keys);
      Domainlist.Sort;
      for i := 0 to DomainList.Count - 1 do
      begin
        DomainName := DomainList[i];
        ADictRestAPIField := ADictRestAPIDomain[DomainName];

        FldList := TList<string>.Create(ADictRestAPIField.Keys);
        FldList.Sort;
        for j := 0 to FldList.Count - 1 do
        begin
          FldName := FldList[j];
          Result.Add(DomainName + '.' + FldName);
        end;
      end;
    end;
  end;

  try
    sql := mtAPIJobsSQL.AsString.Trim;
    if (RequestFormConn <> nil) and (sql <> '') then
    begin
      Query := TFDQuery.Create(self);
      Query.Connection := RequestFormConn;

      //sql := SQL.ToUpperInvariant;
      sql := sql.replace(#13#10, ' ', [rfReplaceAll]);
      sql := sql.replace(#13, ' ', [rfReplaceAll]).Replace(#10, ' ', [rfReplaceAll]).Replace(#9, ' ', [rfReplaceAll]).
        Replace('  ', ' ', [rfReplaceAll]);

      whereidx := -1;
      groupbyidx := -1;
      orderbyidx := -1;

      BracketCnt := 0;

      idx := 0;
      whereidx := -1;
      whereendidx := -1;

      while idx <= sql.Length - 1 do
      begin
        if sql.Substring(idx, 1) = '(' then
          BracketCnt := BracketCnt + 1
        else if sql.Substring(idx, 1) = ')' then
          BracketCnt := BracketCnt - 1;

        if BracketCnt = 0 then
        begin
          expr := '';
          ch := sql.Substring(idx, 1);
          while (idx <= sql.Length - 1) and (ch <> ' ') and (ch <> '(') and (ch <> ')') do
          begin
            expr := expr + ch;
            idx := idx + 1;
            if idx <= sql.Length - 1 then
              ch := sql.Substring(idx, 1);
          end;
          expr := expr.ToUpperInvariant;

          if expr = 'WHERE' then
            whereidx := idx + 1
          else if expr = 'BY' then
          if ((prevexpr = 'GROUP') or (prevexpr = 'ORDER')) then
          begin
            if prevexpr = 'GROUP' then
              groupbyidx := idx - 8
            else if prevexpr = 'ORDER' then
              orderbyidx := idx - 8;

            if (whereidx > -1) and (whereendidx = -1) then
            begin
              if idx > sql.length - 1 then
                whereendidx := idx
              else
                whereendidx := idx - 1;
              break;
            end;
          end;
          prevexpr := expr;
        end;
        idx := idx + 1;
      end;

      if (whereidx > -1) and (whereendidx = -1) then
        whereendidx := sql.Length;

      if whereidx > -1 then
      begin
        sql := sql.Insert(whereendidx, ')');
        sql := sql.insert(whereidx, '1=0 AND (');
      end
      else
      begin
        if groupbyidx > -1 then
          whereendidx := groupbyidx - 1
        else if orderbyidx > -1 then
          whereendidx := orderbyidx - 1
        else
          whereidx := sql.Length;
        sql := sql.insert(whereidx, ' WHERE 1=0 ');
      end;

      Query.SQL.Text := sql;
      Query.Open();

      for fld in Query.Fields do
        Result.Add('SQL.'+fld.FieldName);
    end;
  except
    on e: Exception do
    begin
      ShowMessageFmt('An error occured during getting the fields ofd sql query. Error: %s', [e.Message]);
    end;
  end;
end;

procedure TfrmAPIInProgSettings.LoadTypes;
var
  TypeItem, ReqType: string;
begin
  ReqType := VarToStr(dcbType.EditValue);
  dcbType.Properties.Items.Clear;

  for TypeItem in TfrmRequestTypes.List do
  begin
    dcbType.Properties.Items.Add(TypeItem);
  end;

  if dcbType.Properties.items.IndexOf(ReqType) > -1 then
    dcbType.EditValue := ReqType;
end;

procedure TfrmAPIInProgSettings.mtAPIJobResponseFieldsAfterCancel(DataSet: TDataSet);
begin
  btnSave.Enabled := mtAPIJobs.UpdatesPending or mtAPIJobResponseFields.UpdatesPending;
end;

procedure TfrmAPIInProgSettings.mtAPIJobResponseFieldsAfterDelete(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIInProgSettings.mtAPIJobResponseFieldsAfterEdit(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIInProgSettings.mtAPIJobResponseFieldsAfterPost(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIInProgSettings.mtAPIJobsAfterCancel(DataSet: TDataSet);
begin
  btnSave.Enabled := mtAPIJobs.UpdatesPending or mtAPIJobResponseFields.UpdatesPending;
end;

procedure TfrmAPIInProgSettings.mtAPIJobsAfterDelete(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIInProgSettings.mtAPIJobsAfterEdit(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIInProgSettings.mtAPIJobsAfterInsert(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIInProgSettings.mtAPIJobsAfterPost(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIInProgSettings.mtAPIJobsBeforeDelete(DataSet: TDataSet);
begin
  if dmAPIInProg.mtFormButtonJobAssignments.locate('JobID', mtAPIJobsID.AsString, []) then
    if dmAPIInProg.mtAPIJobResponseFields.RecordCount > 0 then
      raise Exception.Create('Button job assignment exists for the job. API job can not be deleted!');

  mtAPIJobResponseFields.First;
  while not mtAPIJobResponseFields.Eof do
     mtAPIJobResponseFields.Delete;
end;

procedure TfrmAPIInProgSettings.mtAPIJobsBeforePost(DataSet: TDataSet);
begin
  if mtAPIJobsFormClass.AsString = '' then
    raise Exception.Create('Form class of API Job cann not be empty!');
  if mtAPIJobsCaption.AsString = '' then
    raise Exception.Create('Caption of API Job cann not be empty!');
  if mtAPIJobsResponseType.AsString = '' then
    raise Exception.Create('Response type of API Job cann not be empty!');
  if mtAPIJobsSendsWindowsNotification.AsBoolean then
  begin
    if mtAPIJobsSuccessNotificationTitle.AsString = '' then
      raise Exception.Create('While "Sends windows notification" is selected, "Success Notification Title" can not be empty!'#13#10+
        'You can show notification message settings form by clicking "Notification Message Settings" button.');
    if mtAPIJobsSuccessNotificationMessage.AsString = '' then
      raise Exception.Create('While "Sends windows notification" is selected, "Success Notification Message" can not be empty!'#13#10+
        'You can show notification message settings form by clicking "Notification Message Settings" button.');

    if mtAPIJobsErrorNotificationTitle.AsString = '' then
      raise Exception.Create('While "Sends windows notification" is selected, "Error Notification Title" can not be empty!'#13#10+
        'You can show notification message settings form by clicking "Notification Message Settings" button.');
//    if mtAPIJobsErrorNotificationMessage.AsString = '' then
//      raise Exception.Create('While "Sends windows notification" is selected, "Error Notification Message" can not be empty!'#13#10+
//        'You can show notification message settings form by clicking "Notification Message Settings" button.');
  end;
end;

procedure TfrmAPIInProgSettings.mtAPIJobsNewRecord(DataSet: TDataSet);
var
  ID: TGUID;
begin
  if RequestForm <> nil then
    mtAPIJobsFormClass.AsString := RequestForm.ClassName;
  CreateGUID(ID);
  mtAPIJobsID.AsGuid := ID;
  if dcbResponseType.Properties.Items.Count > 0 then
    mtAPIJobsResponseType.AsString := dcbResponseType.Properties.Items[0];
  mtAPIJobsOrder.AsInteger := mtAPIJobs.RecordCount + 1;
end;

procedure TfrmAPIInProgSettings.LoadSettings;
var
  SavedID: string;
  js: TJSONObject;
begin
  //if TFile.Exists(REQUEST_SETTINGS_FILENAME) then
  //begin
    if mtAPIJobs.RecordCount > 0 then
      SavedID := mtAPIJobsID.AsString
    else
      SavedID := '';

    mtAPIJobs.Filtered := False;

//    try
//      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(REQUEST_SETTINGS_FILENAME, TEncoding.UTF8)) as TJSONObject;
//      try
//        JS2MT(js, mtAPIJobs);
//      finally
//        js.Free;
//      end;
//    except
//    end;

    mtAPIJobs.CloneCursor(dmAPIInProg.mtAPIJobs, False, True);

    if RequestForm <> nil then
      mtAPIJobs.Filter := 'FormClass = ' + QuotedStr(RequestForm.ClassName)
    else
      mtAPIJobs.Filter := '1=0';
    mtAPIJobs.Filtered := mtAPIJobs.Filter <> '';

    if (SavedID <> '') and mtAPIJobs.Active  then
      mtAPIJobs.Locate('ID', SavedID, []);
  //end;

  ///if not mtAPIJobs.Active  then
  //  mtAPIJobs.Active := True;

  //if TFile.Exists(RESPONSE_FIELDS_FILENAME) then
  //begin
    mtAPIJobResponseFields.MasterSource := nil;
//    js := TJSONObject.ParseJSONValue(TFile.ReadAllText(RESPONSE_FIELDS_FILENAME, TEncoding.UTF8)) as TJSONObject;
//    try
//      JS2MT(js, mtAPIJobResponseFields);
//    finally
//      js.Free;
//    end;
  ///end;

  mtAPIJobResponseFields.CloneCursor(dmAPIInProg.mtAPIJobResponseFields, False, True);

//  if not mtAPIJobResponseFields.Active then
//    mtAPIJobResponseFields.Active := True;

  if mtAPIJobResponseFields.MasterSource = nil then
    mtAPIJobResponseFields.MasterSource := dsAPIJobs;
end;

procedure TfrmAPIInProgSettings.SaveSettings;
var
  js: TJSONObject;
begin
  js := MT2JS(mtAPIJobs);
  try
    TFile.WriteAllText(REQUEST_SETTINGS_FILENAME, js.ToString);
  finally
    js.Free;
  end;

  if mtAPIJobs.UpdatesPending then
    mtAPIJobs.CommitUpdates;

  js := MT2JS(mtAPIJobResponseFields);
  try
    TFile.WriteAllText(RESPONSE_FIELDS_FILENAME, js.ToString);
  finally
    js.Free;
  end;

  if mtAPIJobResponseFields.UpdatesPending then
    mtAPIJobResponseFields.CommitUpdates;

// Updates reflected to datasets in the dmAPIInProg
//  if dmAPIInProg <> nil then
//  begin
//    if dmAPIInProg.mtAPIJobs.Active then
//      dmAPIInProg.mtAPIJobs.Active := False;
//    if dmAPIInProg.mtAPIJobResponseFields.Active then
//      dmAPIInProg.mtAPIJobResponseFields.Active := False;
//  end;
end;

end.
