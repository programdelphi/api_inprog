object fAddFields: TfAddFields
  Left = 0
  Top = 0
  Caption = 'Add fields'
  ClientHeight = 411
  ClientWidth = 390
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object cxGroupBox1: TcxGroupBox
    Left = 0
    Top = 357
    Align = alBottom
    PanelStyle.Active = True
    Style.BorderStyle = ebsNone
    TabOrder = 0
    ExplicitWidth = 274
    Height = 27
    Width = 390
    object btnUnselect: TcxButton
      Left = 92
      Top = 2
      Width = 206
      Height = 23
      Align = alClient
      Caption = 'Unselect All'
      TabOrder = 0
      OnClick = btnUnselectClick
      ExplicitWidth = 90
    end
    object btnSelect: TcxButton
      Left = 2
      Top = 2
      Width = 90
      Height = 23
      Align = alLeft
      Caption = 'Select All'
      TabOrder = 1
      OnClick = btnSelectClick
    end
    object btnReverse: TcxButton
      Left = 298
      Top = 2
      Width = 90
      Height = 23
      Align = alRight
      Caption = 'Reverse'
      TabOrder = 2
      OnClick = btnReverseClick
      ExplicitLeft = 182
    end
  end
  object cxGroupBox2: TcxGroupBox
    Left = 0
    Top = 384
    Align = alBottom
    PanelStyle.Active = True
    Style.BorderStyle = ebsNone
    TabOrder = 1
    ExplicitWidth = 274
    Height = 27
    Width = 390
    object cxButton4: TcxButton
      Left = 140
      Top = 1
      Width = 84
      Height = 23
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 0
    end
    object cxButton5: TcxButton
      Left = 44
      Top = 1
      Width = 90
      Height = 23
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 1
    end
  end
  object grdFields: TcxGrid
    Left = 0
    Top = 0
    Width = 390
    Height = 357
    Align = alClient
    TabOrder = 2
    ExplicitWidth = 274
    object viewFields: TcxGridTableView
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmManual
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      FilterRow.Visible = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object colCheck: TcxGridColumn
        Caption = 'Check'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.OnValidate = colCheckPropertiesValidate
        Width = 67
      end
      object colName: TcxGridColumn
        Caption = 'Field name'
        PropertiesClassName = 'TcxTextEditProperties'
        Options.Editing = False
        Width = 200
      end
      object colParamName: TcxGridColumn
        Caption = 'Param Name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnValidate = colParamNamePropertiesValidate
        Width = 121
      end
    end
    object grdFieldsLevel1: TcxGridLevel
      GridView = viewFields
    end
  end
end
