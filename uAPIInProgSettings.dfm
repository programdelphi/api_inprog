object frmAPIInProgSettings: TfrmAPIInProgSettings
  Left = 0
  Top = 0
  Caption = 'API In Program Settings'
  ClientHeight = 689
  ClientWidth = 966
  Color = clWindow
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlSettings: TcxGroupBox
    Left = 0
    Top = 237
    Align = alTop
    Caption = 'Settings'
    TabOrder = 0
    Height = 156
    Width = 966
    object lblTranslatedText: TLabel
      Left = 507
      Top = 98
      Width = 101
      Height = 13
      AutoSize = False
      Caption = 'Translated Request :'
      FocusControl = mmTranslatedRequest
    end
    object Label3: TLabel
      Left = 770
      Top = 19
      Width = 139
      Height = 13
      Caption = 'Sends Windows Notification :'
    end
    object lblCaption: TcxLabel
      Left = 8
      Top = 17
      AutoSize = False
      Caption = 'Caption :'
      Properties.Alignment.Horz = taRightJustify
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      Height = 17
      Width = 105
      AnchorX = 113
      AnchorY = 26
    end
    object lblType: TcxLabel
      Left = 9
      Top = 98
      AutoSize = False
      Caption = 'Type :'
      Style.Shadow = False
      Style.TransparentBorder = True
      Properties.Alignment.Horz = taRightJustify
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      Height = 17
      Width = 105
      AnchorX = 114
      AnchorY = 107
    end
    object lblID: TcxLabel
      Left = 8
      Top = 43
      AutoSize = False
      Caption = 'ID :'
      Properties.Alignment.Horz = taRightJustify
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      Height = 17
      Width = 105
      AnchorX = 113
      AnchorY = 52
    end
    object lblGroupID: TcxLabel
      Left = 8
      Top = 71
      AutoSize = False
      Caption = 'Group ID :'
      Properties.Alignment.Horz = taRightJustify
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      Height = 17
      Width = 105
      AnchorX = 113
      AnchorY = 80
    end
    object lblOrder: TcxLabel
      Left = 448
      Top = 38
      AutoSize = False
      Caption = 'Order :'
      Properties.Alignment.Horz = taRightJustify
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      Height = 17
      Width = 160
      AnchorX = 608
      AnchorY = 47
    end
    object lblActive: TcxLabel
      Left = 448
      Top = 15
      AutoSize = False
      Caption = 'Active :'
      Properties.Alignment.Horz = taRightJustify
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      Height = 17
      Width = 160
      AnchorX = 608
      AnchorY = 24
    end
    object lblTimeOut: TcxLabel
      Left = 448
      Top = 71
      AutoSize = False
      Caption = 'Time Out:'
      Properties.Alignment.Horz = taRightJustify
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      Height = 17
      Width = 160
      AnchorX = 608
      AnchorY = 80
    end
    object lblResponseType: TcxLabel
      Left = 9
      Top = 125
      AutoSize = False
      Caption = 'Response Type :'
      Style.Shadow = False
      Style.TransparentBorder = True
      Properties.Alignment.Horz = taRightJustify
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      Height = 17
      Width = 105
      AnchorX = 114
      AnchorY = 134
    end
    object dteCaption: TcxDBTextEdit
      Left = 119
      Top = 16
      DataBinding.DataField = 'Caption'
      DataBinding.DataSource = dsAPIJobs
      TabOrder = 0
      Width = 233
    end
    object dteID: TcxDBTextEdit
      Left = 119
      Top = 43
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dsAPIJobs
      TabOrder = 2
      Width = 233
    end
    object dteGroupID: TcxDBTextEdit
      Left = 119
      Top = 70
      DataBinding.DataField = 'GroupID'
      DataBinding.DataSource = dsAPIJobs
      TabOrder = 4
      Width = 233
    end
    object dcbType: TcxDBComboBox
      Left = 120
      Top = 98
      DataBinding.DataField = 'Type'
      DataBinding.DataSource = dsAPIJobs
      Properties.DropDownListStyle = lsEditFixedList
      TabOrder = 7
      Width = 233
    end
    object dseOrder: TcxDBSpinEdit
      AlignWithMargins = True
      Left = 614
      Top = 38
      Margins.Top = 9
      Margins.Bottom = 9
      DataBinding.DataField = 'Order'
      DataBinding.DataSource = dsAPIJobs
      TabOrder = 14
      Width = 100
    end
    object dseTimeOut: TcxDBSpinEdit
      AlignWithMargins = True
      Left = 614
      Top = 70
      Margins.Top = 9
      Margins.Bottom = 9
      DataBinding.DataField = 'Timeout'
      DataBinding.DataSource = dsAPIJobs
      TabOrder = 16
      Width = 100
    end
    object dchbActive: TcxDBCheckBox
      AlignWithMargins = True
      Left = 614
      Top = 15
      DataBinding.DataField = 'Active'
      DataBinding.DataSource = dsAPIJobs
      Style.TransparentBorder = False
      TabOrder = 12
    end
    object dcbResponseType: TcxDBComboBox
      Left = 120
      Top = 125
      DataBinding.DataField = 'ResponseType'
      DataBinding.DataSource = dsAPIJobs
      Properties.DropDownListStyle = lsEditFixedList
      Properties.Items.Strings = (
        'Silence'
        'Blocking'
        'Non-blocking')
      TabOrder = 10
      Width = 233
    end
    object btnEditTypes: TcxButton
      AlignWithMargins = True
      Left = 359
      Top = 95
      Width = 83
      Height = 26
      Caption = 'Edit Types'
      TabOrder = 8
      OnClick = btnEditTypesClick
    end
    object mmTranslatedRequest: TMemo
      Left = 614
      Top = 98
      Width = 349
      Height = 48
      ParentColor = True
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 17
    end
    object dchbSendsWindowsNotification: TcxDBCheckBox
      Left = 914
      Top = 17
      AutoSize = False
      BiDiMode = bdRightToLeft
      DataBinding.DataField = 'SendsWindowsNotification'
      DataBinding.DataSource = dsAPIJobs
      ParentBiDiMode = False
      Style.TransparentBorder = False
      TabOrder = 18
      Height = 17
      Width = 18
    end
    object btnNotificationSettings: TcxButton
      Left = 770
      Top = 40
      Width = 162
      Height = 25
      Caption = 'Notification Message Settings'
      TabOrder = 19
      OnClick = btnNotificationSettingsClick
    end
  end
  object grdAPIJobs: TcxGrid
    Left = 0
    Top = 0
    Width = 966
    Height = 200
    Align = alTop
    TabOrder = 1
    object grdAPIJobsDBTV: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = dsAPIJobs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.GroupByBox = False
      object grdAPIJobsDBTVFormClass: TcxGridDBColumn
        DataBinding.FieldName = 'FormClass'
        Visible = False
        Width = 80
      end
      object grdAPIJobsDBTVCaption: TcxGridDBColumn
        DataBinding.FieldName = 'Caption'
        Width = 250
      end
      object grdAPIJobsDBTVActive: TcxGridDBColumn
        DataBinding.FieldName = 'Active'
        Width = 36
      end
      object grdAPIJobsDBTVOrder: TcxGridDBColumn
        DataBinding.FieldName = 'Order'
      end
      object grdAPIJobsDBTVTimeout: TcxGridDBColumn
        DataBinding.FieldName = 'Timeout'
        Width = 52
      end
      object grdAPIJobsDBTVID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 191
      end
      object grdAPIJobsDBTVGroupID: TcxGridDBColumn
        DataBinding.FieldName = 'GroupID'
        Width = 100
      end
      object grdAPIJobsDBTVType: TcxGridDBColumn
        DataBinding.FieldName = 'Type'
        Width = 200
      end
      object grdAPIJobsDBTVResponseType: TcxGridDBColumn
        DataBinding.FieldName = 'ResponseType'
        Width = 100
      end
      object grdAPIJobsDBTVLocationHost: TcxGridDBColumn
        DataBinding.FieldName = 'LocationHost'
        Width = 200
      end
      object grdAPIJobsDBTVRequestResource: TcxGridDBColumn
        DataBinding.FieldName = 'RequestResource'
        Width = 200
      end
      object grdAPIJobsDBTVRequestParams: TcxGridDBColumn
        DataBinding.FieldName = 'RequestParams'
        Width = 200
      end
      object grdAPIJobsDBTVSendsWindowsNotification: TcxGridDBColumn
        DataBinding.FieldName = 'SendsWindowsNotification'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Width = 141
      end
      object grdAPIJobsDBTVSuccessNotificationTitle: TcxGridDBColumn
        DataBinding.FieldName = 'SuccessNotificationTitle'
        Width = 195
      end
      object grdAPIJobsDBTVSuccessNotificationHeader: TcxGridDBColumn
        DataBinding.FieldName = 'SuccessNotificationHeader'
        Width = 212
      end
      object grdAPIJobsDBTVSuccessNotificationMessage: TcxGridDBColumn
        DataBinding.FieldName = 'SuccessNotificationMessage'
        Width = 243
      end
      object grdAPIJobsDBTVErrorNotificationTitle: TcxGridDBColumn
        DataBinding.FieldName = 'ErrorNotificationTitle'
        Width = 139
      end
      object grdAPIJobsDBTVErrorNotificationHeader: TcxGridDBColumn
        DataBinding.FieldName = 'ErrorNotificationHeader'
        Width = 165
      end
      object grdAPIJobsDBTVErrorNotificationMessage: TcxGridDBColumn
        DataBinding.FieldName = 'ErrorNotificationMessage'
        Width = 272
      end
      object grdAPIJobsDBTVResponseGridWidth: TcxGridDBColumn
        DataBinding.FieldName = 'ResponseGridWidth'
        Width = 122
      end
      object grdAPIJobsDBTVResponseGridHeight: TcxGridDBColumn
        DataBinding.FieldName = 'ResponseGridHeight'
        Width = 136
      end
    end
    object grdAPIJobsL: TcxGridLevel
      GridView = grdAPIJobsDBTV
    end
  end
  object gbButtons: TcxGroupBox
    Left = 0
    Top = 200
    Align = alTop
    PanelStyle.Active = True
    TabOrder = 2
    Height = 37
    Width = 966
    object btnAdd: TcxButton
      Left = 19
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Add'
      TabOrder = 0
      OnClick = btnAddClick
    end
    object btnPost: TcxButton
      Left = 100
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Post'
      TabOrder = 1
      OnClick = btnPostClick
    end
    object btnCancel: TcxButton
      Left = 181
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Cancel'
      TabOrder = 2
      OnClick = btnCancelClick
    end
    object btnDelete: TcxButton
      Left = 262
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Delete'
      TabOrder = 3
      OnClick = btnDeleteClick
    end
    object btnReload: TcxButton
      Left = 355
      Top = 6
      Width = 94
      Height = 25
      Caption = '&Reload Settings'
      TabOrder = 4
      OnClick = btnReloadClick
    end
    object btnSave: TcxButton
      Left = 455
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Save'
      Enabled = False
      TabOrder = 5
      OnClick = btnSaveClick
    end
    object btnShowTranslated: TcxButton
      Left = 576
      Top = 6
      Width = 138
      Height = 25
      Caption = 'Show Translated Request'
      TabOrder = 6
      OnClick = btnShowTranslatedClick
    end
    object btnRunRequest: TcxButton
      Left = 720
      Top = 6
      Width = 121
      Height = 25
      Caption = 'Run Request'
      TabOrder = 7
      OnClick = btnRunRequestClick
    end
    object btnUIJobAssignments: TcxButton
      Left = 857
      Top = 6
      Width = 106
      Height = 25
      Caption = 'UI Job Assignments'
      TabOrder = 8
      OnClick = btnUIJobAssignmentsClick
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 393
    Width = 517
    Height = 296
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alClient
    TabOrder = 3
    object pnlLocation: TcxGroupBox
      Left = 1
      Top = 1
      Align = alTop
      Caption = 'Location URL'
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.NativeStyle = True
      TabOrder = 0
      DesignSize = (
        515
        57)
      Height = 57
      Width = 515
      object dteLocationURL: TcxDBTextEdit
        AlignWithMargins = True
        Left = 5
        Top = 23
        Margins.Left = 6
        Margins.Right = 6
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'LocationHost'
        DataBinding.DataSource = dsAPIJobs
        TabOrder = 0
        Width = 423
      end
    end
    object gbRequestResource: TcxGroupBox
      Left = 1
      Top = 58
      Align = alTop
      Caption = 'Request Resource'
      TabOrder = 1
      DesignSize = (
        515
        50)
      Height = 50
      Width = 515
      object dteResource: TcxDBTextEdit
        AlignWithMargins = True
        Left = 5
        Top = 18
        Margins.Left = 6
        Margins.Right = 6
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'RequestResource'
        DataBinding.DataSource = dsAPIJobs
        TabOrder = 1
        Width = 423
      end
      object btnSelectResAndPar: TcxButton
        AlignWithMargins = True
        Left = 434
        Top = 18
        Width = 73
        Height = 26
        Anchors = [akTop, akRight]
        Caption = 'Select'
        TabOrder = 0
        OnClick = btnSelectResAndParClick
      end
    end
    object gbRequestParams: TcxGroupBox
      Left = 1
      Top = 108
      Align = alTop
      Caption = 'Request Parameters'
      TabOrder = 2
      DesignSize = (
        515
        89)
      Height = 89
      Width = 515
      object dmmParameters: TcxDBMemo
        AlignWithMargins = True
        Left = 5
        Top = 17
        Margins.Left = 6
        Margins.Right = 6
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'RequestParams'
        DataBinding.DataSource = dsAPIJobs
        Properties.ScrollBars = ssVertical
        TabOrder = 1
        Height = 63
        Width = 423
      end
      object btnAddAll: TcxButton
        AlignWithMargins = True
        Left = 434
        Top = 9
        Width = 73
        Height = 26
        Margins.Left = 6
        Margins.Top = 6
        Margins.Right = 6
        Margins.Bottom = 6
        Anchors = [akTop, akRight]
        Caption = 'Add fields'
        TabOrder = 0
        OnClick = btnAddAllClick
      end
      object btnAddGroups: TcxButton
        AlignWithMargins = True
        Left = 434
        Top = 47
        Width = 73
        Height = 26
        Margins.Left = 6
        Margins.Top = 6
        Margins.Right = 6
        Margins.Bottom = 6
        Anchors = [akTop, akRight]
        Caption = 'Groups'
        TabOrder = 2
        OnClick = btnAddGroupsClick
      end
    end
    object gbSQL: TcxGroupBox
      Left = 1
      Top = 197
      Align = alClient
      Caption = 'SQL'
      TabOrder = 3
      Height = 98
      Width = 515
      object dmmSQL: TcxDBMemo
        Left = 2
        Top = 18
        Align = alClient
        DataBinding.DataField = 'SQL'
        DataBinding.DataSource = dsAPIJobs
        Properties.ScrollBars = ssVertical
        TabOrder = 0
        OnMouseLeave = dmmSQLMouseLeave
        OnMouseMove = dmmSQLMouseMove
        Height = 78
        Width = 511
      end
    end
  end
  object gbResponseSettings: TcxGroupBox
    Left = 517
    Top = 393
    Align = alRight
    Caption = 'Response Grid Settings'
    TabOrder = 4
    Height = 296
    Width = 449
    object gbResponseFields: TcxGroupBox
      Left = 2
      Top = 52
      Align = alClient
      Caption = 'Response Fields'
      TabOrder = 0
      Height = 242
      Width = 445
      object grdResponseFields: TcxGrid
        AlignWithMargins = True
        Left = 5
        Top = 19
        Width = 435
        Height = 218
        Margins.Top = 1
        Align = alClient
        TabOrder = 0
        object grdResponseFieldsDBTV: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.Insert.Visible = False
          Navigator.Buttons.Delete.Visible = False
          Navigator.Visible = True
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = dsAPIJobResponseFields
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          OptionsView.HeaderHeight = 40
          object grdResponseFieldsDBTVResponseFieldName: TcxGridDBColumn
            DataBinding.FieldName = 'ResponseFieldName'
            Options.Editing = False
            Width = 188
          end
          object grdResponseFieldsDBTVOrderNo: TcxGridDBColumn
            DataBinding.FieldName = 'OrderNo'
            Width = 67
          end
          object grdResponseFieldsDBTVShowInResponseGrid: TcxGridDBColumn
            DataBinding.FieldName = 'ShowInResponseGrid'
            Width = 81
          end
          object grdResponseFieldsDBTVResponseGridColumnWidth: TcxGridDBColumn
            DataBinding.FieldName = 'ResponseGridColumnWidth'
            Width = 91
          end
        end
        object grdResponseFieldsL: TcxGridLevel
          GridView = grdResponseFieldsDBTV
        end
      end
    end
    object gbResponseGridSize: TcxGroupBox
      Left = 2
      Top = 18
      Align = alTop
      PanelStyle.Active = True
      Style.BorderStyle = ebsNone
      Style.Edges = [bLeft, bTop, bRight, bBottom]
      TabOrder = 1
      Height = 34
      Width = 445
      object Label1: TLabel
        Left = 20
        Top = 9
        Width = 120
        Height = 13
        Caption = 'Width of Response Grid :'
        FocusControl = cxDBSpinEdit1
      end
      object Label2: TLabel
        Left = 234
        Top = 9
        Width = 123
        Height = 13
        Caption = 'Height of Response Grid :'
        FocusControl = cxDBSpinEdit2
      end
      object cxDBSpinEdit1: TcxDBSpinEdit
        Left = 146
        Top = 6
        DataBinding.DataField = 'ResponseGridWidth'
        DataBinding.DataSource = dsAPIJobs
        TabOrder = 0
        Width = 51
      end
      object cxDBSpinEdit2: TcxDBSpinEdit
        Left = 362
        Top = 6
        DataBinding.DataField = 'ResponseGridHeight'
        DataBinding.DataSource = dsAPIJobs
        TabOrder = 1
        Width = 51
      end
    end
  end
  object mtAPIJobs: TFDMemTable
    AfterInsert = mtAPIJobsAfterInsert
    AfterEdit = mtAPIJobsAfterEdit
    BeforePost = mtAPIJobsBeforePost
    AfterPost = mtAPIJobsAfterPost
    AfterCancel = mtAPIJobsAfterCancel
    BeforeDelete = mtAPIJobsBeforeDelete
    AfterDelete = mtAPIJobsAfterDelete
    OnNewRecord = mtAPIJobsNewRecord
    CachedUpdates = True
    IndexFieldNames = 'ID'
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 336
    Top = 73
    object mtAPIJobsFormClass: TStringField
      DisplayLabel = 'Form Class'
      FieldName = 'FormClass'
      Size = 100
    end
    object mtAPIJobsCaption: TStringField
      FieldName = 'Caption'
      Size = 100
    end
    object mtAPIJobsID: TGuidField
      FieldName = 'ID'
      Size = 38
    end
    object mtAPIJobsGroupID: TStringField
      DisplayLabel = 'Group ID'
      FieldName = 'GroupID'
      Size = 100
    end
    object mtAPIJobsType: TStringField
      FieldName = 'Type'
      Size = 50
    end
    object mtAPIJobsResponseType: TStringField
      DefaultExpression = 'true'
      DisplayLabel = 'Response Type'
      FieldName = 'ResponseType'
      Size = 50
    end
    object mtAPIJobsSendsWindowsNotification: TBooleanField
      DefaultExpression = 'false'
      DisplayLabel = 'Sends Windows Notification'
      FieldName = 'SendsWindowsNotification'
    end
    object mtAPIJobsActive: TBooleanField
      DefaultExpression = 'true'
      FieldName = 'Active'
    end
    object mtAPIJobsOrder: TSmallintField
      FieldName = 'Order'
    end
    object mtAPIJobsTimeout: TIntegerField
      DefaultExpression = '30'
      DisplayLabel = 'Time Out'
      FieldName = 'Timeout'
    end
    object mtAPIJobsLocationHost: TStringField
      DisplayLabel = 'Location Host'
      FieldName = 'LocationHost'
      Size = 200
    end
    object mtAPIJobsRequestResource: TStringField
      DisplayLabel = 'Request Resource'
      FieldName = 'RequestResource'
      Size = 200
    end
    object mtAPIJobsRequestParams: TStringField
      DisplayLabel = 'Request Params'
      FieldName = 'RequestParams'
      Size = 400
    end
    object mtAPIJobsSQL: TStringField
      FieldName = 'SQL'
      Size = 500
    end
    object mtAPIJobsSuccessNotificationTitle: TStringField
      DisplayLabel = 'Success Notification Title'
      FieldName = 'SuccessNotificationTitle'
      Size = 100
    end
    object mtAPIJobsSuccessNotificationHeader: TStringField
      DisplayLabel = 'Success Notification Header'
      FieldName = 'SuccessNotificationHeader'
      Size = 200
    end
    object mtAPIJobsSuccessNotificationMessage: TStringField
      DisplayLabel = 'Success Notification Message'
      FieldName = 'SuccessNotificationMessage'
      Size = 300
    end
    object mtAPIJobsErrorNotificationTitle: TStringField
      DisplayLabel = 'Error Notification Title'
      FieldName = 'ErrorNotificationTitle'
      Size = 100
    end
    object mtAPIJobsErrorNotificationHeader: TStringField
      DisplayLabel = 'Error Notification Header'
      FieldName = 'ErrorNotificationHeader'
      Size = 200
    end
    object mtAPIJobsErrorNotificationMessage: TStringField
      DisplayLabel = 'Error Notification Message'
      FieldName = 'ErrorNotificationMessage'
      Size = 300
    end
    object mtAPIJobsResponseGridWidth: TIntegerField
      DisplayLabel = 'Width of Response Grid '
      FieldName = 'ResponseGridWidth'
    end
    object mtAPIJobsResponseGridHeight: TIntegerField
      DisplayLabel = 'Height of Response Grid'
      FieldName = 'ResponseGridHeight'
    end
  end
  object dsAPIJobs: TDataSource
    DataSet = mtAPIJobs
    Left = 424
    Top = 80
  end
  object cxHintStyleController1: TcxHintStyleController
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    Left = 432
    Top = 536
  end
  object mtAPIJobResponseFields: TFDMemTable
    AfterEdit = mtAPIJobResponseFieldsAfterEdit
    AfterPost = mtAPIJobResponseFieldsAfterPost
    AfterCancel = mtAPIJobResponseFieldsAfterCancel
    AfterDelete = mtAPIJobResponseFieldsAfterDelete
    CachedUpdates = True
    IndexFieldNames = 'FormClass;JobID;OrderNo'
    Aggregates = <
      item
        Name = 'MaxOrderNo'
        Expression = 'Max(OrderNo)'
        GroupingLevel = 2
        Active = True
      end>
    AggregatesActive = True
    MasterSource = dsAPIJobs
    MasterFields = 'FormClass;ID'
    DetailFields = 'FormClass;JobID'
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 312
    Top = 136
    object mtAPIJobResponseFieldsFormClass: TStringField
      DisplayLabel = 'Form Class'
      FieldName = 'FormClass'
      Size = 100
    end
    object mtAPIJobResponseFieldsJobID: TGuidField
      DisplayLabel = 'Job ID'
      FieldName = 'JobID'
      Size = 38
    end
    object mtAPIJobResponseFieldsResponseFieldName: TStringField
      DisplayLabel = 'Response Field Name'
      FieldName = 'ResponseFieldName'
      Size = 100
    end
    object mtAPIJobResponseFieldsShowInResponseGrid: TBooleanField
      DisplayLabel = 'Show In Response Grid'
      FieldName = 'ShowInResponseGrid'
    end
    object mtAPIJobResponseFieldsOrderNo: TSmallintField
      DisplayLabel = 'Order No'
      FieldName = 'OrderNo'
    end
    object mtAPIJobResponseFieldsResponseGridColumnWidth: TIntegerField
      DisplayLabel = 'Response Grid Column Width'
      FieldName = 'ResponseGridColumnWidth'
    end
  end
  object dsAPIJobResponseFields: TDataSource
    DataSet = mtAPIJobResponseFields
    Left = 432
    Top = 152
  end
end
