unit uRecords;

interface

uses
  Spring.Collections, FireDAC.Comp.Client, System.JSON, System.Generics.Collections, VCL.Forms, VCL.Menus, VCL.StdCtrls;

type
  IDictionaryStringString = IDictionary<string, string>;
  IListString = IList<string>;

  TRestCall = record
    PrimaryKeyValue: string;
    APIJobID: TGUID;
    SourceForm: TForm;
    FromMenuItem: Boolean;
    Button: TCustomButton;
    MenuItem: TMenuItem;
    BaseURL: string;
    Resource: string;
    Params: string;
    ResponseType: string;
    ResponseText: string;
    function FlatKV(): TDictionary<string, string>;
  end;

  TExitOnDone = (eodNever, eodAlways, eodSuccess);

function MT2JS(ATable: TFDMemTable): TJSONObject;
procedure JS2MT(AJSON: TJSONObject; mt: TFDMemTable);

function PascalCase(const AName: string): string;

function ParamToPair(const AParam: string): TPair<string, string>;

function CreateDictionaryStringString(): IDictionaryStringString; overload;
function CreateDictionaryStringString(const AKey, AValue: string): IDictionaryStringString; overload;
function CreateListString(const arr: array of string): IListString;

implementation

uses
  System.Classes, FireDAC.Stan.Intf, System.Types, System.SysUtils, System.Generics.Defaults;

function MT2JS(ATable: TFDMemTable): TJSONObject;
var
  ms: TMemoryStream;
begin
  ms := TMemoryStream.Create;
  try
    ATable.SaveToStream(ms, sfJSON);
    Result := TJSONObject.ParseJSONValue(PByte(ms.Memory), 0, ms.Size, [TJSONObject.TJSONParseOption.IsUTF8])
      as TJSONObject;
  finally
    ms.Free;
  end;
end;

procedure JS2MT(AJSON: TJSONObject; mt: TFDMemTable);
var
  ms: TMemoryStream;
  b: TBytes;
begin
  ms := TMemoryStream.Create;
  try
    b := TEncoding.UTF8.GetBytes(AJSON.ToString);
    ms.Write(b, Length(b));
    ms.Seek(0, soFromBeginning);
    mt.LoadFromStream(ms, sfJSON);
  finally
    ms.Free;
  end;
end;

function PascalCase(const AName: string): string;
begin
  Result := UpperCase(Copy(AName, 1, 1)) + LowerCase(Copy(AName, 2));
end;

function ParamToPair(const AParam: string): TPair<string, string>;
begin
  Result := TPair<string, string>.Create(Copy(AParam, 1, Pos('=', AParam) - 1), Copy(AParam, Pos('=', AParam) + 1));
end;

{ TRestCall }

function TRestCall.FlatKV: TDictionary<string, string>;
var
  dict: TDictionary<string, string>;

  procedure Traverse(json: TJSONValue);
  var
    p: TJSONPair;
    v: TJSONValue;
  begin
    if json = nil then
      Exit;

    if json.InheritsFrom(TJSONObject) then
      for p in TJSONObject(json) do
        if (p.JsonValue.ClassType = TJSONString)
          or (p.JsonValue.ClassType = TJSONNumber)
          or (p.JsonValue.ClassType = TJSONBool)
          or (p.JsonValue.ClassType = TJSONNull) then
          dict.AddOrSetValue(p.JsonString.Value, p.JsonValue.Value)
        else
          Traverse(p.JsonValue)
      else if json.InheritsFrom(TJSONArray) then
        for v in TJSONArray(json) do
          Traverse(v);
  end;
var
  js: TJSONValue;
begin
  //dict := TDictionary<string, string>.Create;
  dict := TDictionary<string, string>.Create(TIStringComparer.Ordinal);
  if ResponseText = '' then
    Exit;
  js := TJSONObject.ParseJSONValue(ResponseText, True, False);
  try
    Traverse(js);
  finally
    js.Free;
  end;
  Result := dict;
end;

function CreateDictionaryStringString(): IDictionaryStringString; overload;
begin
  Result := TCollections.CreateDictionary<string, string>;
end;

function CreateDictionaryStringString(const AKey, AValue: string): IDictionaryStringString; overload;
begin
  Result := CreateDictionaryStringString();
  Result[AKey] := AValue;
end;

function CreateListString(const arr: array of string): IListString;
begin
  Result := TCollections.CreateList<string>(arr);
end;
end.

