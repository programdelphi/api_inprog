unit uLooper;

interface

uses
  VCL.TMSLogging, TMSLoggingUtils, TMSLoggingTextOutputHandler, uDataManager, uRecords, Spring, Spring.Collections,
  System.IniFiles, Data.DB, System.SysUtils, atPascal, uSentry, System.Generics.Collections;

type
  ELooperError = class(Exception);

  ILooperJob = interface
  ['{441FED98-E911-4447-87AA-B97CAEBE5FA9}']
    function Run: Boolean;
  end;

  TLooperJob = class(TInterfacedObject, ILooperJob)
  private
    FSQL: string;
    //FBaseUrl: string;
    FURLs: TArray<System.string>;
    FURLPath: string;
    FResource: string;
    FParameters: string;
    FTasks: Integer;
    FSentryDSN: string;
    FSentryTags: string;
    FScripter: TatPascalScripter;
    FRetries: Integer;
    FSentryHandler: TSentryOutputHandler;
    FAPIErrorCount: Integer;
    //procedure ErrorRequestAPI(const Host: string; const ARestCall: TRestCall; const AErrorMessage: string);
    procedure ErrorRequestAPI(const ARestCall: TRestCall; const AErrorMessage: string);

  public
    constructor Create(const AIni: string);
    destructor Destroy; override;

    function Run: Boolean;

    class function DictTrans(const ATemplate: string; AData: TDataSet; ADict: IDictionaryStringString; AScripter:
        TatPascalScripter): string;
    class procedure UpdateDict(AData: TDataSet; ADict: IDictionaryStringString);
    class function GetRestCall(AData: TDataSet; ADict: IDictionaryStringString; AScripter: TatPascalScripter; const
      APrimaryKeyValue, ABaseURL, AResource, AParameters: string): TRestCall;
    class function AllParams: string; static;
  end;

const
  SIniDefaultFileName = 'api.src';
  SIniSectionSQL = 'SQL';
  SIniKeyRunMode = 'RunMode';
  SIniKeyPrimaryKey = 'PrimaryKey';
  SIniSectionAPI = 'API';
  SIniKeySQL = 'SQL';
  SIniKeyCannedNameFmt = 'CannedName%d';
  SIniKeyCannedSQLFmt  = 'CanndeSQL%d';

//  SIniKeyURL = 'URL';
  SIniKeyURLHost = 'URLHost';
  SIniKeyEnterprise = 'Enterprise';
  SIniKeyURLHosts = 'URLHosts';

  SIniKeyURLPath = 'URLPath';
  SIniKeyResource = 'Resource';
  SIniKeyParameters = 'Parameters';
  SIniSectionSETTINGS = 'SETTINGS';
  SIniKeyEXITONDONE = 'EXITONDONE';
  SIniKeyLOGGING = 'LOGGING';
  SIniKeySeparateLoggingFile = 'SeparateLoggingFileForHosts';
  SIniKeySLEEP = 'SLEEP';
  SIniKeyRETRIES = 'RETRIES';
  SIniKeyTASKS = 'TASKS';
  SIniKeySENTRYDSN = 'SENTRYDSN';
  SIniKeySENTRYTAGS = 'SENTRYTAGS';
  SIniDefaultSQL = 'select * from';
  SIniDefaultAPIURLPath = '/api/v1/TServerMethods1/';
  SIniKeyURI = 'URI';

  STokenRECNO = '##RECNO##';
  STokenBegin = '##';
  STokenEnd = '##';
  STokenFunc = '##!';
  SParamSeparator = '&';

  SPasExt = '.pas';

  //SFieldAPIStatus = 'APIStatus';
  SFieldAPIStatus = 'Status';
  SFieldCode = 'CODE';
  SFieldCode1 = 'Code';
  SFieldCode2 = 'code';

  SLogDir = '.\logs\';
  SLogExt = '.log';
  SLogTimeStampFormat = '{%"yyyy-mm-dd hh:nn:ss.zz"dt}'#9;
  SLogLogLevelFormat = '{%s}'#9;
  SLogValueFormat = '{%s}';

  SFmtError = 'Error %s';

var
  GTextHandler : TTMSLoggerTextOutputHandler;

implementation

uses
  VCL.ScripterInit,
  ap_Classes, ap_Consts, ap_DB, ap_DBConsts, ap_DBClient,
  ap_System, ap_StrUtils, ap_DateUtils, ap_SysUtils, ap_Types,
  ap_Windows,
  ap_Variants, ap_VarUtils,
  atScript,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.ADS,
  FireDAC.Phys.ADSDef, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  System.StrUtils, System.Variants, System.IOUtils, System.Diagnostics,
  uRestAPIQueueeThread, System.Classes, JclStrings, System.NetEncoding;

const
  SEFmtIniDoesNotExist = 'File %s does not exists!';

  SLogJobStart = 'start job %s';
  SLogJobEndProcessedElapsed = 'end job, processed=%d, elapsed=%s';

{ TLooperJob }

constructor TLooperJob.Create(const AIni: string);
var
  IniFile: TIniFile;
  PasFileName: string;
begin
  if not FileExists(AIni) then
    raise ELooperError.CreateFmt(SEFmtIniDoesNotExist, [AIni]);

  IniFile := TIniFile.Create(ExtractFilePath(ParamStr(0)) + AIni);
  try
    FSQL := StrEscapedToString(iniFile.ReadString(SIniSectionSQL, SIniKeySQL, SIniDefaultSQL));
    //FBaseUrl := iniFile.ReadString(SIniSectionAPI, SIniKeyURLHost, '') + iniFile.ReadString(SIniSectionAPI, SIniKeyURLPath, '');
    FURLs := iniFile.ReadString(SIniSectionAPI, SIniKeyURLHosts, '').Split([',',';']);
    FURLPath := iniFile.ReadString(SIniSectionAPI, SIniKeyURLPath, '');
    FResource := iniFile.ReadString(SIniSectionAPI, SIniKeyResource, '');
    FParameters := iniFile.ReadString(SIniSectionAPI, SIniKeyParameters, '');
    FTasks := iniFile.ReadInteger(SIniSectionSETTINGS, SIniKeyTASKS, 0);
    FRetries := iniFile.ReadInteger(SIniSectionSETTINGS, SIniKeyRETRIES, 3);
    FSentryDSN := iniFile.ReadString(SIniSectionSETTINGS, SIniKeySENTRYDSN, '');
    FSentryTags := iniFile.ReadString(SIniSectionSETTINGS, SIniKeySENTRYTAGS, '');
  finally
    IniFile.Free;
  end;

  FScripter := TatPascalScripter.Create(nil);
  PasFileName := ExtractFilePath(ParamStr(0)) + AIni + SPasExt;
  if FileExists(PasFileName) then
    FScripter.SourceCode.LoadFromFile(PasFileName);

  if FSentryDSN <> '' then
  begin
    FSentryHandler := TMSLogger.RegisterOutputHandlerClass(TSentryOutputHandler, []) as TSentryOutputHandler;
    FSentryHandler.DSN := FSentryDSN;
    FSentryHandler.Tags := 'job,' + ParamStr(1) + ',' + FSentryTags;
    FSentryHandler.LogLevelFilters := FSentryHandler.LogLevelFilters -
      [TTMSLoggerLogLevel.Debug, TTMSLoggerLogLevel.Trace];
  end;

end;

destructor TLooperJob.Destroy;
begin
  FScripter.Free;
end;

class function TLooperJob.DictTrans(const ATemplate: string; AData: TDataSet; ADict: IDictionaryStringString; AScripter:
    TatPascalScripter): string;
var
  Key, Value: string;
  PosStart, PosEnd: Integer;
  Func: string;
  FuncValue: Variant;
begin
  Result := ATemplate;
  for Key in ADict.Keys do
    if Pos(Key, Result) > 0 then
    begin
      Value := EncodeUrl(ADict[Key]);
      Result := StringReplace(Result, Key, Value, [rfIgnoreCase, rfReplaceAll]);
    end;

  PosStart := Pos(STokenFunc, Result);
  while PosStart > 0 do
  begin
    PosEnd := PosEx(STokenEnd, Result, PosStart + Length(STokenFunc));
    if PosEnd > PosStart then
    begin
      Func := Copy(Result, PosStart + Length(STokenFunc), PosEnd - PosStart - Length(STokenFunc));
      FuncValue := AScripter.ExecuteSubroutine(Func, [ObjectToVar(AData)]);
      if not VarIsNull(FuncValue) and not VarIsEmpty(FuncValue) then
        Result := Copy(Result, 1, PosStart - 1) + VarToStr(FuncValue) + Copy(Result, PosEnd + Length(STokenEnd));
      PosStart := Pos(STokenFunc, Result);
    end
    else
      PosStart := PosEx(STokenEnd, Result, PosStart + Length(STokenFunc));
  end;
end;

class procedure TLooperJob.UpdateDict(AData: TDataSet; ADict: IDictionaryStringString);
var
  I: Integer;
begin
  ADict.Clear;
  for I := 0 to AData.FieldCount - 1 do
    if AData.Fields[I].FieldName <> SFieldAPIStatus then
      ADict.AddOrSetValue(STokenBegin + UpperCase(AData.Fields[I].FieldName) + STokenEnd, AData.Fields[I].AsString);
  ADict.AddOrSetValue(STokenRECNO, AData.RecNo.ToString);
end;

class function TLooperJob.GetRestCall(AData: TDataSet; ADict: IDictionaryStringString; AScripter: TatPascalScripter; const
  APrimaryKeyValue, ABaseURL, AResource, AParameters: string): TRestCall;
begin
  UpdateDict(AData, ADict);
  Result.BaseURL := ABaseURL;
  Result.Resource := AResource;
  Result.Params := DictTrans(AParameters, AData, ADict, AScripter);
  Result.PrimaryKeyValue := APrimaryKeyValue;
end;

class function TLooperJob.AllParams(): string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to ParamCount do
    Result := Result + ParamStr(I) + ' ';
end;

//procedure TLooperJob.ErrorRequestAPI(const Host: string; const ARestCall: TRestCall; const AErrorMessage: string);
procedure TLooperJob.ErrorRequestAPI(const ARestCall: TRestCall; const AErrorMessage: string);
begin
  TMSLogger.Error(Format(SFmtError, [AErrorMessage]));
  Writeln(Format(SFmtError, [AErrorMessage]));
  Inc(FAPIErrorCount);
end;

function TLooperJob.Run: Boolean;
var
  t1 : TStopwatch;
  Connection: TFDConnection;
  Query: TFDQuery;
  Que: TRestAPIQueueeThread;
  QueList: TList<TRestAPIQueueeThread>;
  Dict: IDictionaryStringString;
  Count: Integer;
  Progress: Integer;
  i: Integer;
begin
  Result := False;
  FAPIErrorCount := 0;
  TMSLogger.Info(Format(SLogJobStart, [AllParams]));
  writeln('run');
  try
    t1 := TStopwatch.StartNew;
    Connection := TFDConnection.Create(nil);
    try
      Connection.DriverName := 'ADS';
      Connection.Params.Add('Alias=TisWin3DD');
      Connection.Params.Add('Alias=TisWin3DD');
      Connection.Params.Add('Password=lezaM123$51');
      Connection.Params.Add('User_Name=User1');
      Connection.LoginPrompt := False;
      Query := TFDQuery.Create(Connection);
      Query.Connection := Connection;
      Query.FetchOptions.CursorKind := ckForwardOnly;
      Query.FetchOptions.Unidirectional := True;
      Query.FetchOptions.RowsetSize := 1000;
      Query.SQL.Text := FSQL;
      Query.Open();

      QueList := TList<TRestAPIQueueeThread>.Create;

      for i := 0 to Length(FURLs) - 1 do
      begin
        Que := TRestAPIQueueeThread.Create;
        QueList.Add(Que);
        try
          //Que.OnCompleteRequest := CompleteRequestAPI;
          Writeln('que');
          Que.Host := FURLs[i];
          Que.OnErrorRequest := ErrorRequestAPI;
          Dict := TCollections.CreateDictionary<string, string>();
          Count := 0;
          while not Query.Eof do
          begin
            //Que.Add(TLooperJob.GetRestCall(Query, Dict, FScripter, '', FBaseURL, FResource, FParameters));
            Que.Add(TLooperJob.GetRestCall(Query, Dict, FScripter, '', FURLs[i]+FURLPath, FResource, FParameters));
            Query.Next;
            Inc(Count);
          end;
          Writeln('que filled, count=', Count);

          Progress := 0;
          while Que.ListCount > 0 do
          begin
            CheckSynchronize(100);
            if ((Count - Que.ListCount) * 100 div Count) <> Progress then
            begin
              Progress := ((Count - Que.ListCount) * 100 div Count);
              Writeln(Progress, '%');
            end;
          end;

          Writeln('que completed');

          Que.Terminate;
          Que.WaitFor;

          TMSLogger.Info(Format(SLogJobEndProcessedElapsed, [Count, t1.Elapsed.ToString()]));
          Result := FAPIErrorCount = 0;

        finally
          Que.Free;
        end;
      end;

    finally
      Connection.Free;
    end;
  except
    on E: Exception do
    begin
      TMSLogger.Exception(E.Message);
      raise;
    end;
  end;
end;

initialization
  TMSLogger.Outputs := [loTimeStamp, loLogLevel, loValue];
  TMSLogger.OutputFormats.TimeStampFormat := SLogTimeStampFormat;
  TMSLogger.OutputFormats.LogLevelFormat := SLogLogLevelFormat;
  TMSLogger.OutputFormats.ValueFormat := SLogValueFormat;

  if not DirectoryExists(SLogDir) then
    MkDir(SLogDir);

  GTextHandler := TMSLogger.RegisterOutputHandlerClass(TTMSLoggerTextOutputHandler,
      [SLogDir + TPath.GetFileNameWithoutExtension( ParamStr(0)) + SLogExt]) as TTMSLoggerTextOutputHandler;
end.
