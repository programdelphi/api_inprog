unit uAPIInProg;
// For pop up menu item hints
// https://stackoverflow.com/questions/470696/display-a-tooltip-hint-on-a-disabled-menu-item-of-a-popup-menu
// for TMenuItemHint definition
// https://stackoverflow.com/questions/48509834/delphi-menu-hint-bug
//

interface

Uses
  VCL.TMSLogging, TMSLoggingUtils, TMSLoggingTextOutputHandler, System.SysUtils, System.Classes, VCL.Forms, Data.DB,
  System.Generics.Collections, cxButtons, VCL.Menus, VCL.ExtCtrls, Winapi.Messages, Winapi.Windows, FireDAC.Comp.Client,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, uRestAPIQueueeThread, uRecords, System.ImageList, Vcl.ImgList, Vcl.Controls,
  cxImageList, cxGraphics, dxScreenTip, cxClasses, dxCustomHint, cxHint, cxGrid, cxGridLevel, cxGridDBTableView,
  Vcl.StdCtrls, Vcl.Buttons, System.Notification, Vcl.Graphics, System.IniFiles, Vcl.Grids, cxGridPopupMenu,
  cxGridCustomPopupMenu;

type
  EAPIInProgError = class(Exception);

  TMenuItemHint = class (THintWindow)
  private
    activeMenuItem: TMenuItem;
    showTimer: TTimer;
    hideTimer: TTimer;
    procedure HideTime(Sender: TObject);
    procedure ShowTime(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override ;
    procedure DoActivateHint(menuItem: TMenuItem);
    destructor Destroy; override;
  end;

  TSizeablePanel = class(TPanel)
  private
    FDragging: Boolean;
    FLastPos: TPoint;
  protected
    procedure Paint; override;
    procedure MouseDown(Button: TMouseButton; Shift:
      TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
  public
    property PopupMenu;
    property Visible;
  end;

  TCustomGridAccess = class(TCustomGrid)
  public
    property PopupMenu;
  end;

  TcxCustomGridAccess = class(TcxCustomGrid)
  public
    property PopupMenu;
  end;

  TButtonRestAPIJobExecType = (brjeBeforeOriginalOnClickEvent, brjeAfterOriginalOnClickEvent, brjeNotRunOriginalOnClickEvent);

  TButtonRestAPIJobRec = record
    OriginalOnClick: TNotifyEvent;
    RestAPIJobAssigned: Boolean;
    RestAPIJobExecType: TButtonRestAPIJobExecType;
    RestAPIJobID: TGUID;
  end;

  TFormRestAPIJobRec = record
    APIJobID: TGUID;
    FromMenuItem: Boolean;
    Button: TCustomButton;
    MenuItem: TMenuItem;
  end;

  TdmAPIInProg = class(TDataModule)
    mtAPIJobs: TFDMemTable;
    mtAPIJobsFormClass: TStringField;
    mtAPIJobsCaption: TStringField;
    mtAPIJobsID: TGuidField;
    mtAPIJobsGroupID: TStringField;
    mtAPIJobsType: TStringField;
    mtAPIJobsResponseType: TStringField;
    mtAPIJobsActive: TBooleanField;
    mtAPIJobsOrder: TSmallintField;
    mtAPIJobsTimeout: TIntegerField;
    mtAPIJobsLocationHost: TStringField;
    mtAPIJobsRequestResource: TStringField;
    mtAPIJobsRequestParams: TStringField;
    mtAPIJobsSQL: TStringField;
    dsAPIJobs: TDataSource;
    mtAPIJobs1: TFDMemTable;
    ilAPIJobs: TcxImageList;
    HintStyleControllerAPIJobs: TcxHintStyleController;
    PopupMenu1: TPopupMenu;
    NotificationCenter1: TNotificationCenter;
    mtFormGridJobAssignments: TFDMemTable;
    mtFormGridJobAssignmentsFormClass: TStringField;
    mtFormGridJobAssignmentsGridName: TStringField;
    mtFormGridJobAssignmentsJobGroupID: TStringField;
    mtFormGridJobAssignmentsAllJobs: TBooleanField;
    mtFormButtonJobAssignments: TFDMemTable;
    mtFormButtonJobAssignmentsFormClass: TStringField;
    mtFormButtonJobAssignmentsJobID: TGuidField;
    mtFormButtonJobAssignmentsButtonName: TStringField;
    mtFormButtonJobAssignmentsJobExecutionType: TStringField;
    mtAPIJobsSendsWindowsNotification: TBooleanField;
    mtFormButtonJobAssignmentsNewButtonCaption: TStringField;
    mtFormMenuJobAssignments: TFDMemTable;
    mtFormMenuJobAssignmentsFormClass: TStringField;
    mtFormMenuJobAssignmentsMenuName: TStringField;
    mtFormMenuJobAssignmentsJobGroupID: TStringField;
    mtFormMenuJobAssignmentsAllJobs: TBooleanField;
    mtFormButtonJobAssignmentsChangeCaption: TBooleanField;
    mtAPIJobResponseFields: TFDMemTable;
    mtAPIJobResponseFieldsFormClass: TStringField;
    mtAPIJobResponseFieldsResponseFieldName: TStringField;
    mtAPIJobResponseFieldsShowInResponseGrid: TBooleanField;
    mtAPIJobResponseFieldsJobID: TGuidField;
    mtAPIJobResponseFieldsOrderNo: TSmallintField;
    mtButtonJobAssignmentResponseFields: TFDMemTable;
    mtButtonJobAssignmentResponseFieldsJobID: TGuidField;
    mtButtonJobAssignmentResponseFieldsButtonName: TStringField;
    mtButtonJobAssignmentResponseFieldsResponseFieldName: TStringField;
    mtButtonJobAssignmentResponseFieldsShowInResponseGrid: TBooleanField;
    mtButtonJobAssignmentResponseFieldsOrderNo: TSmallintField;
    dsFormButtonJobAssignments: TDataSource;
    mtAPIJobsSuccessNotificationTitle: TStringField;
    mtAPIJobsSuccessNotificationHeader: TStringField;
    mtAPIJobsSuccessNotificationMessage: TStringField;
    mtAPIJobsErrorNotificationTitle: TStringField;
    mtAPIJobsErrorNotificationHeader: TStringField;
    mtAPIJobsErrorNotificationMessage: TStringField;
    mtAPIJobsResponseGridWidth: TIntegerField;
    mtAPIJobsResponseGridHeight: TIntegerField;
    mtFormButtonJobAssignmentsSuccessNotificationTitle: TStringField;
    mtFormButtonJobAssignmentsSuccessNotificationHeader: TStringField;
    mtFormButtonJobAssignmentsSuccessNotificationMessage: TStringField;
    mtFormButtonJobAssignmentsErrorNotificationTitle: TStringField;
    mtFormButtonJobAssignmentsErrorNotificationHeader: TStringField;
    mtFormButtonJobAssignmentsErrorNotificationMessage: TStringField;
    mtFormButtonJobAssignmentsResponseGridWidth: TIntegerField;
    mtFormButtonJobAssignmentsResponseGridHeight: TIntegerField;
    mtAPIJobResponseFieldsResponseGridColumnWidth: TIntegerField;
    mtButtonJobAssignmentResponseFieldsResponseGridColumnWidth: TIntegerField;
    mtFormButtonJobAssignmentsCustomizeResponseGrid: TBooleanField;
    mtFormButtonJobAssignmentsCustomizeNotification: TBooleanField;
    dsAPIJobResponseFields: TDataSource;
    mtFormButtonJobAssignmentsSendsWindowsNotification: TBooleanField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure miSaveLayoutClick(Sender: TObject);
  private
    { Private declarations }
    mtAPIJobResponseFieldsModified: Boolean;
    // Menu item hints
    procedure PopupListWndProc(var AMsg: TMessage);
    procedure btnAPIJobMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure btnAPIJobMouseLeave(Sender: TObject);
    procedure btnUpdateAPICallDropDownMenuPopupEx(Sender: TObject; var APopupMenu: TComponent; var AHandled: Boolean);    { Private declarations }
    procedure grdAPIStatusExit(Sender: TObject);
    procedure miAPIJobClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormResize(Sender: TObject);
    procedure btnUpdateAPICallDropDownClick(Sender: TObject);
    procedure RepositionResultPanel(Form: TForm; SenderButton: TCustomButton; pnlAPIStatus: TSizeablePanel; FormResized: Boolean = False);
    procedure APIJobButtonDrowDownClick(Sender: TObject);
  public
    { Public declarations }
    IniF: TInifile;

    fOldWndProc: TFarProc;
    miHint: TMenuItemHint;

    procedure btnAPIJobClick(Sender: TObject);
    class function StringTranslate(const AURLItem: string; Form: TForm; const ASQL: string = ''; const AURLEncode: Boolean = True): string; static;
    class function ResponseNotificationTranslate(const AMsg: string; const ARestCall: TRestCall; Response: TDictionary<string, string> = nil): string; static;
    procedure dbtvAPIStatusKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure LoadAPIJobs;
    procedure LoadAssignments;
    procedure SaveAPIJobs;
    procedure SaveButtonJobAssignments;
    class function GetRestCall(ADict: IDictionaryStringString; const AAPIJobID: TGUID; ASourceForm: TForm;
      const AFromMenuItem: Boolean; const AButton: TCustomButton; const AMenuItem: TMenuItem;
      const ABaseURL, AResource, AParameters, AResponseType: string): TRestCall;
    procedure CompleteRequestAPI(const ARestCall: TRestCall);
    procedure ErrorRequestAPI(const ARestCall: TRestCall; const AErrorMessage: string);
    procedure SendNotification(const Name, Title, Header, MessageDetail: string);
    procedure APIJobUpdateGUI(form: TForm);
  end;

const
  STokenRECNO = '##RECNO##';
  STokenBegin = '##';
  STokenEnd = '##';
  STokenFunc = '##!';
  SParamSeparator = '&';
  SLogDir = '.\logs\';
  SLogExt = '.log';
  SLogTimeStampFormat = '{%"yyyy-mm-dd hh:nn:ss.zz"dt}'#9;
  SLogLogLevelFormat = '{%s}'#9;
  SLogValueFormat = '{%s}';
  SLAPIStart = 'starting "API Calls"';
  SFmtSuccess = 'Success %s';
  SFmtError = 'Error %s';

  SFieldAPIStatus = 'Status';

  //SResponseFieldNameFmt = 'response_%s';
  SResultGridViewLayoutFileNamePrefix = 'api_inprog.results.gridview_layout';
  SResultGridViewLayoutPrefix = 'dbtvAPIStatus';
  SSettingsResultGridSizeSection_Suffix = 'Response Grid Size';
  SSettingsResultridWidthKey = 'Width';
  SSettingsResultridHeihtKey = 'Height';

  REQUEST_SETTINGS_FILENAME = 'api_inprog.requestsettings.json';
  RESPONSE_FIELDS_FILENAME = 'api_inprog.responsefields.json';
  BUTTON_JOB_ASSIGNMENTS_FILENAME = 'api_inprog.button_job_assignments.json';
  BUTTON_JOB_ASSIGNMENTS_RESPONSE_FIELDS_FILENAME = 'api_inprog.button_job_assignments_responsefields.json';
  GRID_JOB_ASSIGNMENTS_FILENAME = 'api_inprog.grid_job_assignments.json';
  MENU_JOB_ASSIGNMENTS_FILENAME = 'api_inprog.menu_job_assignments.json';

var
  dmAPIInProg: TdmAPIInProg;

  DictFormRestAPIDomain : TDictionary<TForm, TDictionary<string, TDictionary<string, TField>>>;
  DictFormRestAPIDomainItem: TPair<TForm, TDictionary<string, TDictionary<string, TField>>>;
  DictRestAPIDomain : TDictionary<string, TDictionary<string, TField>>;
  DictRestAPIDomainItem: TPair<string, TDictionary<string, TField>>;
  DictRestAPIField : TDictionary<string, TField>;
  DictRestAPIFieldItem : TPair<string, TField>;

  DictFormRestAPIConnection : TDictionary<TForm, TFDConnection>;

  DictResponseQue: TDictionary<TForm, TDictionary<TGUID, TDictionary<String, String>>>;

  //DictButtonRestAPIJob : TDictionary<TCustomButton, TGUID>;
  DictButtonRestAPIJob : TDictionary<TCustomButton, TButtonRestAPIJobRec>;

  DictMenuItemAPIJobID : TDictionary<TMenuItem, TGUID>;

  DictFormAPIJobThreadList: TDictionary<TForm, TRestAPIQueueeThread>;

  DictFormCompletedRestAPIJob : TDictionary<TForm, TFormRestAPIJobRec>;

  Dict: IDictionaryStringString;

  GTextHandler : TTMSLoggerTextOutputHandler;
  OrgFormCloseQuery: procedure(Sender: TObject; var CanClose: Boolean) of object;

  DictFormRestAPIFormResize : TDictionary<TForm, TNotifyEvent>;

procedure AddLog(ALevel: TTMSLoggerLogLevel; const AMessage: string);

procedure RegisterAPIDataset(Form: TForm; DomainName: string; Dataset: TDataset; IncludedFields: TStringList = nil;
                             ExcludedFields: TStringList = nil);

procedure RegisterAPIConnection(Form: TForm; Conn: TFDConnection);

procedure Show_API_SetupForm(Form: TForm);

procedure ApplicationCloseCheck(Form: TForm);

procedure FormResizeCheck(Form: TForm);

procedure PrepareRequestResultControls(Form: TForm; var grdAPIStatus: TcxGrid);

procedure UpdateButtonCodeAndGUI(button: TCustomButton; APIJobID: TGUID; APIJobExecType:
  TButtonRestAPIJobExecType = brjeNotRunOriginalOnClickEvent; ChangeCaption:Boolean = False; NewCaption: String = '');

procedure CreateAPIJobContextMenuItem(form: TForm; GroupID: string = '');

procedure CreateGridAPIJobContextMenuItem(grid: TCustomGrid; GroupID: string = '');

procedure CreatecxGridAPIJobContextMenuItem(grid: TcxCustomGrid; GroupID: string = '');

procedure CreatecxGridPopupMenuAPIJobContextMenuItem(gridpopupmenu: TcxGridPopupMenu; GroupID: string = '';
  HitTypes: TcxGridViewHitTypes = [gvhtCell]);

procedure CreateAPIJobContextMenuItemsForMenu(form: TForm; menu: TPopupMenu; GroupID: string = '');


implementation

uses
  System.IOUtils, System.JSON, uAPIInProgSettings, Spring, Spring.Collections, System.Variants, System.Types, uDataManager,
  System.UITypes, cxGridCustomView, Vcl.Dialogs, uTableHelper, cxControls;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure AddLog(ALevel: TTMSLoggerLogLevel; const AMessage: string);
var
  strLogLevel, LMessage {, FullHostName}: string;
  //LoggingFileSW: TStreamWriter;
begin
  LMessage := StringReplace(AMessage, sLineBreak, ' ', [rfReplaceAll]);

  case ALevel of
    TTMSLoggerLogLevel.Trace:
    begin
      TMSLogger.Trace(LMessage);
      strLogLevel := 'Trace';
    end;
    TTMSLoggerLogLevel.Debug:
    begin
      TMSLogger.Debug(LMessage);
      strLogLevel := 'Debug';
    end;
    TTMSLoggerLogLevel.Info:
    begin
      TMSLogger.Info(LMessage);
      strLogLevel := 'Info';
    end;
    TTMSLoggerLogLevel.Warning:
    begin
      TMSLogger.Warning(LMessage);
      strLogLevel := 'Warning';
    end;
    TTMSLoggerLogLevel.Error:
    begin
      TMSLogger.Error(LMessage);
      strLogLevel := 'Error';
    end;
    TTMSLoggerLogLevel.Exception:
    begin
      TMSLogger.Exception(LMessage);
      strLogLevel := 'Exception';
    end;
    TTMSLoggerLogLevel.All:
    begin
      TMSLogger.Custom(LMessage);
      strLogLevel := 'All';
    end;
    TTMSLoggerLogLevel.Custom:
    begin
      TMSLogger.Custom(LMessage);
      strLogLevel := 'Custom';
    end;
  end;

//  if chkSeparateLoggingFiles.Checked then
//  begin
//    LoggingFileSW := FLoggingFileSWList.Items[Host];
//    LoggingFileSW.WriteLine(FormatDateTime('yyyy-mm-dd hh:nn:ss.zz', Now())+ #9 + strLogLevel + #9  +LMessage);
//  end;

end;

procedure RegisterAPIDataset(Form: TForm; DomainName: string; Dataset: TDataset; IncludedFields: TStringList = nil;
                             ExcludedFields: TStringList = nil);
var
  DictRestAPIDomainField : TDictionary<string, TField>;
  fld: TField;
  i: Integer;
begin
  try
    if Form = nil then
      raise Exception.Create('To register a dataset for API Calls, form object must be provided!');
    if Dataset = nil then
      raise Exception.Create('To register a dataset for API Calls, dataset object must be provided!');
    if Dataset.FieldCount = 0 then
      raise Exception.Create('To register a dataset for API Calls, dataset object must have at least a field!');

    if DictFormRestAPIDomain = nil then
      DictFormRestAPIDomain := TDictionary<TForm, TDictionary<string, TDictionary<string, TField>>>.Create;

    if not DictFormRestAPIDomain.TryGetValue(Form, DictRestAPIDomain) then
    begin
      DictRestAPIDomain := TDictionary<string, TDictionary<string, TField>>.Create;
      DictFormRestAPIDomain.Add(Form,DictRestAPIDomain);
    end
    else
      if DictRestAPIDomain.ContainsKey(DomainName) then
        raise Exception.Create(Format('Domain %s is registered before!', [DomainName]));

    DictRestAPIDomainField := TDictionary<string, TField>.Create;
    DictRestAPIDomain.Add(DomainName,DictRestAPIDomainField);

    if (IncludedFields = nil) or (IncludedFields.Count = 0) then
      for i := 0 to Dataset.FieldCount - 1  do
      begin
        if (ExcludedFields = nil) or (ExcludedFields.Count = 0) or (ExcludedFields.IndexOf(Dataset.fields[i].FieldName) = -1)then
          DictRestAPIDomainField.Add(Dataset.fields[i].FieldName, Dataset.fields[i]);
      end
    else
    for i := 0 to IncludedFields.Count - 1 do
    begin
      fld := Dataset.FindField(IncludedFields[i]);
      if fld = nil then
        raise Exception.Create(Format('Due to %s is not in the %s, domain %s can not be registered for API Callss!',
          [IncludedFields[i], Dataset.Name, DomainName]));

      if (ExcludedFields = nil) or (ExcludedFields.Count = 0) or (ExcludedFields.IndexOf(fld.FieldName) = -1)then
        DictRestAPIDomainField.Add(fld.FieldName, fld);
    end;

  except
    on e: Exception do
     MessageDlg(e.Message, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
  end;
end;

procedure RegisterAPIConnection(Form: TForm; Conn: TFDConnection);
begin
  try
    if Form = nil then
      raise Exception.Create('To register a connection for API Calls, form object must be provided!');
    if Conn = nil then
      raise Exception.Create('To register a connection for API Calls, connection object must be provided!');

    DictFormRestAPIConnection.AddOrSetValue(Form, Conn);

  except
    on e: Exception do
     MessageDlg(e.Message, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
  end;
end;

procedure Show_API_SetupForm(Form: TForm);
begin
  if Form = nil then
    raise Exception.Create('The form which setup will be sown is not specified. Setu form could not be shown!');

  frmAPIInProgSettings := TfrmAPIInProgSettings.Create(Form);
  try
    frmAPIInProgSettings.RequestForm := Form;
    frmAPIInProgSettings.ShowModal;
  finally
    frmAPIInProgSettings.Free;
  end;
end;

procedure ApplicationCloseCheck(Form: TForm);
var
  FormOCQ, APIFormOCQ: procedure(Sender: TObject; var CanClose: Boolean) of object;
begin
  FormOCQ := Form.OnCloseQuery;
  APIFormOCQ := dmAPIInProg.FormCloseQuery;
  if (Form = Application.MainForm) and (@FormOCQ <> @APIFormOCQ) then
  begin
    OrgFormCloseQuery := Form.OnCloseQuery;
    Form.OnCloseQuery := dmAPIInProg.FormCloseQuery;
  end;
end;

procedure FormResizeCheck(Form: TForm);
var
  FormResize, APIFormResize: procedure(Sender: TObject) of object;
begin
  FormResize := Form.OnResize;
  APIFormResize := dmAPIInProg.FormResize;
  if (@FormResize <> @APIFormResize) then
  begin
    // store original resize method in dictionary
    DictFormRestAPIFormResize.AddOrSetValue(Form, Form.OnResize);
    // form resize procedure dictionary must be used
    Form.OnResize := dmAPIInProg.FormResize;
  end;
end;

procedure PrepareRequestResultControls(Form: TForm; var grdAPIStatus: TcxGrid);
var
  mtAPIStatus: TFDMemTable;
  dsAPIStatus: TDataSource;
  grdAPIStatusL: TcxGridLevel;
  dbtvAPIStatus: TcxGridDBTableView;
  pnlAPIStatus: TSizeablePanel;
  pumAPIStatus: TPopupMenu;
  miSaveLayout: TMenuItem;
begin
  if not Assigned(dmAPIInProg ) then
    dmAPIInProg := TdmAPIInProg.Create(Application);

  mtAPIStatus := TFDMemTable(Form.FindComponent('mtAPIStatus'));
  if mtAPIStatus = nil then
  begin
    mtAPIStatus := TFDMemTable.Create(Form);
    mtAPIStatus.Name := 'mtAPIStatus';
    dsAPIStatus := TDataSource.Create(Form);
    dsAPIStatus.Name := 'dsAPIStatus';
    dsAPIStatus.DataSet := mtAPIStatus;
  end;
  if not mtAPIStatus.Active and (mtAPIStatus.FieldDefs.Count = 0) then
  begin
    mtAPIStatus.FieldDefs.Add('ID', TFieldType.ftGuid, 38);
    mtAPIStatus.FieldDefs.Add(SFieldAPIStatus, TFieldType.ftString, 200);
    mtAPIStatus.CreateDataSet;
  end;
  if not mtAPIStatus.Active then
    mtAPIStatus.Active := True;

  pnlAPIStatus := TSizeablePanel(Form.FindComponent('pnlAPIStatus'));
  if pnlAPIStatus = nil then
  begin
    pnlAPIStatus := TSizeablePanel.Create(Form);
    pnlAPIStatus.Parent := Form;
    pnlAPIStatus.Name := 'pnlAPIStatus';
    pnlAPIStatus.Caption := '';
    pnlAPIStatus.Align := alNone;
    pnlAPIStatus.Width := 1030;
    pnlAPIStatus.Height := 63;

    pumAPIStatus := TPopupMenu.Create(Form);
    pumAPIStatus.Name := 'pumRestAPIStatus';
    miSaveLayout := TMenuItem.Create(Form);
    miSaveLayout.Caption := 'Save Layout';
    miSaveLayout.OnClick := dmAPIInProg.miSaveLayoutClick;
    pumAPIStatus.Items.add(miSaveLayout);

    pnlAPIStatus.PopupMenu := pumAPIStatus;
  end;
  grdAPIStatus := TcxGrid(Form.FindComponent('grdAPIStatus'));
  if grdAPIStatus = nil then
  begin
    grdAPIStatus := TcxGrid.Create(Form);
    grdAPIStatus.Align := alClient;
    grdAPIStatus.Name := 'grdAPIStatus';

    grdAPIStatus.OnExit := dmAPIInProg.grdAPIStatusExit;
    grdAPIStatus.Parent := pnlAPIStatus;
    //grdAPIStatus.Width := 1030;
    //grdAPIStatus.Height := 63;
    grdAPIStatusL := grdAPIStatus.Levels.Add;
    grdAPIStatusL.Name := 'grdAPIStatusL';
    dbtvAPIStatus := grdAPIStatus.CreateView(TcxGridDBTableView) as TcxGridDBTableView;
    dbtvAPIStatus.Name := 'dbtvAPIStatus';
    grdAPIStatusL.GridView := dbtvAPIStatus;
    dbtvAPIStatus.DataController.DataSource := dsAPIStatus;
    dbtvAPIStatus.OptionsBehavior.BestFitMaxRecordCount := 50;
    dbtvAPIStatus.OptionsCustomize.ColumnsQuickCustomization := True;
    dbtvAPIStatus.OptionsData.CancelOnExit := False;
    dbtvAPIStatus.OptionsData.Deleting := False;
    dbtvAPIStatus.OptionsData.DeletingConfirmation := False;
    dbtvAPIStatus.OptionsData.Editing := False;
    dbtvAPIStatus.OptionsData.Inserting := False;
    //dbtvAPIStatus.OptionsView.ColumnAutoWidth := True;
    dbtvAPIStatus.OptionsView.GroupByBox := False;
    dbtvAPIStatus.OnKeyDown := dmAPIInProg.dbtvAPIStatusKeyDown;
    dbtvAPIStatus.ClearItems;
    dbtvAPIStatus.DataController.CreateAllItems;
    dbtvAPIStatus.Columns[0].Width := 195;
    dbtvAPIStatus.Columns[1].Width := 63;
    // API Status
    dbtvAPIStatus.DataController.KeyFieldNames := 'ID';
  end;
  pnlAPIStatus.Visible := False;
end;

procedure UpdateButtonCodeAndGUI(button: TCustomButton; APIJobID: TGUID; APIJobExecType:
  TButtonRestAPIJobExecType = brjeNotRunOriginalOnClickEvent; ChangeCaption:Boolean = False; NewCaption: String = '');
var
  Form: TForm;
  grdAPIStatus: TcxGrid;
  ButtonRestAPIJob: TButtonRestAPIJobRec;
  ButtonOnClick, RestAPIJoBOnClick: TNotifyEvent;
begin
  try
    if button = nil then
      raise Exception.Create('For button code and gui update, a button must be supplied!');

    if APIJobID.IsEmpty then
      raise Exception.Create('For button code and gui update, a job API ID must be supplied!');

    if not Assigned(dmAPIInProg ) then
      dmAPIInProg := TdmAPIInProg.Create(Application);

    if not dmAPIInProg.mtAPIJobs.Active then
      dmAPIInProg.LoadAPIJobs;

    if not dmAPIInProg.mtAPIJobs.Active then
      raise Exception.Create('Rest API jobs are not defined. For button code and gui update, a valid job API ID must be supplied!');

    dmAPIInProg.mtAPIJobs.IndexFieldNames := 'ID';
    if not dmAPIInProg.mtAPIJobs.Locate('ID', APIJobID.ToString, []) then
      raise Exception.Create(Format('There is not an API Job defined with ID %s',[APIJobID.ToString]));

    RestAPIJoBOnClick := dmAPIInProg.btnAPIJobClick;
    if (button is TButton) or (button is TBitBtn) then
    begin
      ButtonOnClick := TButton(button).OnClick;
      if @ButtonOnClick = @RestAPIJoBOnClick then
        exit;
      if ChangeCaption and (NewCaption <> '') then
        TButton(button).Caption := NewCaption;
      ButtonRestAPIJob.OriginalOnClick := TButton(button).OnClick;
      TButton(button).OnClick := dmAPIInProg.btnAPIJobClick;
      button.Images := dmAPIInProg.ilAPIJobs;
      button.ImageIndex := -1;
    end else if button is TcxButton then
    begin
      ButtonOnClick := TcxButton(button).OnClick;
      if @ButtonOnClick = @RestAPIJoBOnClick then
        exit;
      if ChangeCaption and (NewCaption <> '') then
        TcxButton(button).Caption := NewCaption;
      ButtonRestAPIJob.OriginalOnClick := TcxButton(button).OnClick;
      TcxButton(button).OnClick := dmAPIInProg.btnAPIJobClick;
      TcxButton(button).OptionsImage.Images := dmAPIInProg.ilAPIJobs;
      TcxButton(button).OptionsImage.ImageIndex := -1;
    end;
    button.Hint := '';

    //DictButtonRestAPIJob.AddOrSetValue(button, APIJobID);
    ButtonRestAPIJob.RestAPIJobAssigned := True;
    ButtonRestAPIJob.RestAPIJobExecType := APIJobExecType;
    ButtonRestAPIJob.RestAPIJobID := APIJobID;
    DictButtonRestAPIJob.AddOrSetValue(button, ButtonRestAPIJob);

    Form := TForm(GetParentForm(button));
    ApplicationCloseCheck(Form);
    FormResizeCheck(Form);
    PrepareRequestResultControls(Form, grdAPIStatus);
  except
    on e: Exception do
     MessageDlg(e.Message, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
  end;
end;

{ class of TMenuItemHint}

constructor TMenuItemHint.Create(AOwner: TComponent);
begin
  inherited;

  showTimer := TTimer.Create(self);
  showTimer.Interval := Application.HintPause;

  hideTimer := TTimer.Create(self);
  hideTimer.Interval := Application.HintHidePause;
end;

destructor TMenuItemHint.Destroy;
begin
  hideTimer.OnTimer := nil;
  showTimer.OnTimer := nil;
  self.ReleaseHandle;
  inherited;
end;

procedure TMenuItemHint.DoActivateHint(menuItem: TMenuItem);
begin
  hideTime(self);

  if (menuItem = nil) or (menuItem.Hint = '') then
  begin
    activeMenuItem := nil;
    Exit;
  end;

  activeMenuItem := menuItem;

  showTimer.OnTimer := ShowTime;
  hideTimer.OnTimer := HideTime;
end;

procedure TMenuItemHint.HideTime(Sender: TObject);
begin
  self.ReleaseHandle;
  hideTimer.OnTimer := nil;
end;

procedure TMenuItemHint.ShowTime(Sender: TObject);
var
  r : TRect;
  wdth : integer;
  hght : integer;
begin
  if activeMenuItem <> nil then
  begin

    wdth := Canvas.TextWidth(activeMenuItem.Hint);
    hght := Canvas.TextHeight(activeMenuItem.Hint);

    r.Left := Mouse.CursorPos.X + 16;
    r.Top := Mouse.CursorPos.Y + 16;
    r.Right := r.Left + wdth + 6;
    r.Bottom := r.Top + hght + 4;

    ActivateHint(r,activeMenuItem.Hint);
  end;

  showTimer.OnTimer := nil;
end;

procedure TdmAPIInProg.DataModuleCreate(Sender: TObject);
var
  NewWndProc: TFarProc;
begin
  miHint := TMenuItemHint.Create(self);

  // Menu item hints
  NewWndProc := MakeObjectInstance(dmAPIInProg.PopupListWndProc);
  fOldWndProc := TFarProc(SetWindowLong(VCL.Menus.PopupList.Window, GWL_WNDPROC, integer(NewWndProc)));

  LoadAPIJobs;

  LoadAssignments;

  IniF := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'api_inprog.ini');
end;

procedure TdmAPIInProg.DataModuleDestroy(Sender: TObject);
var
  //menu item hints
  NewWndProc: TFarProc;
  js: TJSONObject;
begin
  IniF.Free;
  //fOldWndProc := TFarProc(lblMenuHint.Tag);
  NewWndProc := TFarProc(SetWindowLong(VCL.Menus.PopupList.Window, GWL_WNDPROC,
    integer(fOldWndProc)));
  FreeObjectInstance(NewWndProc);
  PopupMenu1.Free;

  if mtAPIJobResponseFieldsModified then
  begin
    // Save dataset of response fields
    js := MT2JS(mtAPIJobResponseFields);
    try
      TFile.WriteAllText(RESPONSE_FIELDS_FILENAME, js.ToString);
    finally
      js.Free;
    end;
  end;

end;

procedure TdmAPIInProg.grdAPIStatusExit(Sender: TObject);
var
  grdAPIStatus: TcxGrid;
begin
  if (TcxGrid(Sender).Parent <> nil) and (TcxGrid(Sender).Parent is TPanel)  then
    TcxGrid(Sender).Parent.Visible := False;
end;

procedure TdmAPIInProg.dbtvAPIStatusKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  grdAPIStatus: TcxGrid;
begin
  if Key = VK_Escape then
  begin
    grdAPIStatus := TcxGrid(TcxGridSite(Sender).Container);
    if grdAPIStatus = nil then
      Exit;
    if (grdAPIStatus.Parent <> nil) and (grdAPIStatus.Parent.Visible) then
      grdAPIStatus.Parent.Visible := False;
  end;
end;

class function TdmAPIInProg.StringTranslate(const AURLItem: string; Form: TForm; const ASQL: string = ''; const AURLEncode: Boolean = True): string;
var
  expr, {Key,} Value: string;
  idx, PosStart, PosEnd, PosDot: Integer;
  Domain, FieldName {, Func}: string;
  //FuncValue: Variant;
  //ParamsArr: TArray<string>;
  DictParams: TDictionary<string, string>;
  DictParamsItem: TPair<string, string>;
  ADictDomain: TDictionary<string, TDictionary<string, TField>>;
  ADictFields: TDictionary<string, TField>;
  Field: TField;
  Query : TFDQuery;
  Conn : TFDConnection;
begin
  if AURLItem = '' then
    raise Exception.Create('No parameter is specified for the parameter translation!');
  if Form = nil then
    raise Exception.Create('No form is specified for the parameter translation!');
  Result := AURLItem;
  DictParams := TDictionary<string, string>.Create;
  idx := 0;
  while (idx < AURLItem.Length -1) do
  begin
    expr := '';
    // Get variable parameter and translate it
    PosStart := AURLItem.IndexOf(STokenBegin, idx);
    if PosStart > -1 then
    begin
      if PosStart < AURLItem.Length - 1 then
        PosEnd := AURLItem.IndexOf(STokenEnd, PosStart + 1)
      else
        PosEnd := 0;
      expr := AURLItem.Substring(PosStart, PosEnd - PosStart + Length(STokenEnd));

      idx := PosEnd + Length(STokenEnd);
      if (expr <> '') and not DictParams.ContainsKey(expr) then
      begin
        PosDot := Expr.IndexOf('.');
        if PosDot = -1 then
          raise Exception.Create('Parameter does not contain domain!');
        Domain := Expr.Substring(STokenBegin.Length, PosDot - STokenBegin.Length);
        FieldName := Expr.Substring(PosDot + 1, Expr.Length - PosDot - STokenEnd.Length - 1 );
        if Domain.ToUpperInvariant <> 'SQL' then
        begin
          if not DictFormRestAPIDomain.TryGetValue(form, ADictDomain) then
            raise Exception.Create('No domain is registered for the form!');
          if not ADictDomain.ContainsKey(Domain) then
            raise Exception.Create('No dataset is registered for the domain!');
          if not ADictDomain.TryGetValue(Domain, ADictFields) then
            raise Exception.Create('No field is registered for the domain!');
           if not ADictFields.TryGetValue(FieldName, Field) then
            raise Exception.Create('No field is registered for the field!');
          DictParams.AddOrSetValue(expr, VarToStr(Field.Value));
        end else
        begin
          if ASQL = '' then
            raise Exception.Create('No SQL is specified for the SQL fields!');

          if Query = nil then
          begin
            Query := TFDQuery.Create(Form);
            if not DictFormRestAPIConnection.TryGetValue(Form, Conn) then
            raise Exception.Create('No Connection is registered for REST API calls related to form. SQL field value can not be determined!');

            Query.Connection := Conn;
            Query.SQL.Text := ASQL;
          end;
          if not Query.Active then
            Query.Open();

          if Query.RecordCount = 0 then
            raise Exception.Create('No record is retrieved for the specified SQL. SQL field value can not be determined!');

          Field := Query.FindField(FieldName);
          if Field = nil then
            raise Exception.Create('Requested Field with SQL domain is not in the SQL result set!');
          DictParams.AddOrSetValue(expr, VarToStr(Field.Value));
        end;
      end;
    end
    else
      idx := AURLItem.Length;
  end;

  for DictParamsItem in DictParams do
  begin
    if AURLEncode then
      Value := EncodeUrl(DictParamsItem.Value)
    else
      Value := DictParamsItem.Value;
    Result := StringReplace(Result, DictParamsItem.Key, Value, [rfIgnoreCase, rfReplaceAll]);
  end;

  {PosStart := Pos(STokenFunc, Result);
  while PosStart > 0 do
  begin
    PosEnd := PosEx(STokenEnd, Result, PosStart + Length(STokenFunc));
    if PosEnd > PosStart then
    begin
      Func := Copy(Result, PosStart + Length(STokenFunc), PosEnd - PosStart - Length(STokenFunc));
      FuncValue := AScripter.ExecuteSubroutine(Func, [ObjectToVar(AData)]);
      if not VarIsNull(FuncValue) and not VarIsEmpty(FuncValue) then
        Result := Copy(Result, 1, PosStart - 1) + VarToStr(FuncValue) + Copy(Result, PosEnd + Length(STokenEnd));
      PosStart := Pos(STokenFunc, Result);
    end
    else
      PosStart := PosEx(STokenEnd, Result, PosStart + Length(STokenFunc));
  end;}
end;

class function TdmAPIInProg.ResponseNotificationTranslate(const AMsg: string; const ARestCall: TRestCall; Response: TDictionary<string, string> = nil): string;
var
  expr, Value: string;
  idx, PosStart, PosEnd, PosDot: Integer;
  Domain, FieldName: string;
  DictParams: TDictionary<string, string>;
  DictParamsItem: TPair<string, string>;
begin
  Result := AMsg;
  if AMsg = '' then
  begin
    ShowMessage('No notification message is specified for response notification translation!');
    exit;
  end;

  DictParams := TDictionary<string, string>.Create;
  idx := 0;
  while (idx < AMsg.Length -1) do
  begin
    expr := '';
    // Get variable parameter and translate it
    PosStart := AMsg.IndexOf(STokenBegin, idx);
    if PosStart > -1 then
    begin
      if PosStart < AMsg.Length - 1 then
      begin
        PosEnd := AMsg.IndexOf(STokenEnd, PosStart + 1);
        expr := AMsg.Substring(PosStart, PosEnd - PosStart + Length(STokenEnd));
        idx := PosEnd + Length(STokenEnd);
      end
      else
      begin
        idx := AMsg.Length;
      end;

      if (expr <> '') and not DictParams.ContainsKey(expr) then
      begin
        PosDot := Expr.IndexOf('.');
        if PosDot = -1 then
        begin
          FieldName := Expr.Substring(STokenBegin.Length, Expr.Length - STokenBegin.Length - STokenEnd.Length).Trim;
          if FieldName.ToLowerInvariant = 'location' then
            Value := ARestCall.BaseURL
          else if FieldName.ToLowerInvariant = 'resource' then
            Value := ARestCall.Resource
          else if FieldName.ToLowerInvariant = 'jobid' then
            Value := ARestCall.APIJobID.ToString
          else if FieldName.ToLowerInvariant = 'formname' then
            Value := ARestCall.SourceForm.Name
          else if (FieldName.ToLowerInvariant = 'form') or (FieldName.ToLowerInvariant = 'formcaption') then
            Value := ARestCall.SourceForm.Caption
          else if (FieldName.ToLowerInvariant = 'params') or (FieldName.ToLowerInvariant = 'parameters') then
            //Value := StringTranslate(ARestCall.Params, ARestCall.SourceForm)
            Value := ARestCall.Params
          else if FieldName.ToLowerInvariant = 'response' then
            Value := ARestCall.ResponseText
          else
          begin
            ShowMessage(Format('"%s" is not a standard keyword!', [FieldName]));
            continue;
          end;
        end else
        begin
          Domain := Expr.Substring(STokenBegin.Length, PosDot - STokenBegin.Length).Trim;
          FieldName := Expr.Substring(PosDot + 1, Expr.Length - PosDot - STokenEnd.Length - 1 ).Trim;

          if Domain.ToUpperInvariant <> 'RESPONSE' then
          begin
            //ShowMessage('Parameter domain is different from Response!');
            continue;
          end;

          if Response = nil then
          begin
            ShowMessage('Parameter can not be a response field!');
            continue;
          end;

          if not response.TryGetValue(FieldName, Value) then
            if FieldName.ToLowerInvariant = SFieldAPIStatus then
              Value := ''
            else
            begin
              ShowMessage(Format('"%s" is not in the response fields!', [FieldName]));
              continue;
            end;
        end;
        DictParams.AddOrSetValue(expr, Value);
      end;
    end
    else
      idx := AMsg.Length;
  end;

  for DictParamsItem in DictParams do
    Result := StringReplace(Result, DictParamsItem.Key, DictParamsItem.Value, [rfIgnoreCase, rfReplaceAll]);

  if Result.IndexOf(STokenBegin, 0) > -1 then
    Result := StringTranslate(Result, ARestCall.SourceForm, '', False);
end;

procedure TdmAPIInProg.PopupListWndProc(var AMsg: TMessage);
  function FindItemForCommand(APopupMenu: TPopupMenu;
    const AMenuMsg: TWMMenuSelect): TMenuItem;
  var
    SubMenu: HMENU;
  begin
    Assert(APopupMenu <> nil);
    // menuitem
    Result := APopupMenu.FindItem(AMenuMsg.IDItem, fkCommand);
    if Result = nil then begin
      // submenu
      SubMenu := GetSubMenu(AMenuMsg.Menu, AMenuMsg.IDItem);
      if SubMenu <> 0 then
        Result := APopupMenu.FindItem(SubMenu, fkHandle);
    end;
  end;

var
  Msg: TWMMenuSelect;
  menuItem: TMenuItem;
  MenuIndex: integer;
  OldWndProc: TFarProc;
begin
  if dmAPIInProg = nil then
    exit;
  OldWndProc := dmAPIInProg.fOldWndProc;
  AMsg.Result := CallWindowProc(OldWndProc, VCL.Menus.PopupList.Window,
    AMsg.Msg, AMsg.WParam, AMsg.LParam);
  if AMsg.Msg = WM_MENUSELECT then begin
    menuItem := nil;
    Msg := TWMMenuSelect(AMsg);
    if (Msg.MenuFlag <> $FFFF) or (Msg.IDItem <> 0) then begin
      for MenuIndex := 0 to PopupList.Count - 1 do begin
        menuItem := FindItemForCommand(PopupList.Items[MenuIndex], Msg);
        if menuItem <> nil then
          break;
      end;
    end;
    dmAPIInProg.miHint.DoActivateHint(menuItem);
  end;
end;

procedure TdmAPIInProg.SendNotification(const Name, Title, Header, MessageDetail: string);
var
  Notification : TNotification;
begin
  Notification := NotificationCenter1.CreateNotification;
  Notification.Name := Name;
  Notification.Title := Title;
  Notification.AlertBody := Header +#13#10 + MessageDetail;
  NotificationCenter1.PresentNotification(Notification);
end;

procedure TdmAPIInProg.RepositionResultPanel(Form: TForm; SenderButton: TCustomButton; pnlAPIStatus: TSizeablePanel; FormResized: Boolean = False);
var
  pnlPos: TPoint;
begin
  if pnlAPIStatus.Width > form.ClientWidth - 10 then
    pnlAPIStatus.Width := form.ClientWidth - 10;

  if pnlAPIStatus.Height > form.ClientHeight - 10 then
    pnlAPIStatus.Height := form.ClientHeight - 10;

  if SenderButton <> nil then
  begin
    pnlPos := Form.ScreenToClient(TCustomButton(SenderButton).ClientToScreen(Point(1, TCustomButton(SenderButton).Height)));
    if pnlPos.Y + pnlAPIStatus.Height > Form.ClientHeight then
      pnlPos.Y := pnlPos.Y - TCustomButton(SenderButton).Height - pnlAPIStatus.Height;

    if pnlPos.X + pnlAPIStatus.Width > Form.ClientWidth then
        pnlPos.X := pnlPos.X + TCustomButton(SenderButton).Width - pnlAPIStatus.Width;
  end
  else
  begin
    if not FormResized then
    begin
     if not Winapi.Windows.GetCursorPos(pnlPos) then
      exit;
      pnlPos := Form.ScreenToClient(pnlPos);

      pnlPos.X := pnlPos.X - Round(pnlAPIStatus.Width / 2);
      pnlPos.Y := pnlPos.Y - pnlAPIStatus.Height;
    end else
    begin
      //if formresized and some part of panel at the left is not visible make that part visible;
      //if formresized and some part of panel at the top is not visible make that part visible;

      pnlPos.X :=  pnlAPIStatus.Left;
      pnlPos.Y := pnlAPIStatus.Top;
    end;

    if pnlPos.X < 0 then
      pnlPos.X := 0;
    if pnlPos.Y < 0 then
      pnlPos.Y := 0;

    if pnlAPIStatus.Height > Form.ClientHeight - 8 then
        pnlPos.Y := 0
    else if pnlPos.Y + pnlAPIStatus.Height > Form.ClientHeight - 8 then
      pnlPos.Y := Form.ClientHeight - pnlAPIStatus.Height - 8;

    if pnlAPIStatus.Width > Form.ClientWidth - 10 then
        pnlPos.X := 0
    else
    if pnlPos.X + pnlAPIStatus.Width > Form.ClientWidth - 10 then
      pnlPos.X := Form.ClientWidth - pnlAPIStatus.Width - 10;
  end;

  pnlAPIStatus.Left := pnlPos.X;
  pnlAPIStatus.Top := pnlPos.Y;

  if SenderButton <> nil then
    pnlAPIStatus.Hint := SenderButton.Name
  else
    pnlAPIStatus.Hint := '';
end;

class function TdmAPIInProg.GetRestCall(ADict: IDictionaryStringString; const AAPIJobID: TGUID; ASourceForm: TForm;
      const AFromMenuItem: Boolean; const AButton: TCustomButton; const AMenuItem: TMenuItem;
      const ABaseURL, AResource, AParameters, AResponseType: string): TRestCall;
begin
  ///UpdateDict(AData, ADict);
  Result.APIJobID := AAPIJobID;
  Result.SourceForm := ASourceForm;
  Result.FromMenuItem := AFromMenuItem;
  Result.Button := AButton;
  Result.MenuItem := AMenuItem;
  Result.BaseURL := StringTranslate(ABaseURL, ASourceForm);
  Result.Resource := StringTranslate(AResource, ASourceForm);
  Result.Params := StringTranslate(AParameters, ASourceForm);
  Result.ResponseType := AResponseType;
  //Result.PrimaryKeyValue := APrimaryKeyValue;}
end;

procedure TdmAPIInProg.btnAPIJobMouseLeave(Sender: TObject);
begin
  HintStyleControllerAPIJobs.HideHint;
end;

procedure TdmAPIInProg.btnAPIJobMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  btnAPIJob: TCustomButton;
  //cxbtnAPIJob: TcxButton;
  btnAPIJobImageIdx: Integer;
begin
  if not (Sender is TCustomButton) then
    raise Exception.Create('API Job move is not called by a TCustomButton!');

  btnAPIJob := TCustomButton(Sender);
  if (Sender is TButton) or (Sender is TBitBtn) then
  begin
    btnAPIJobImageIdx := btnAPIJob.ImageIndex;
  end else if Sender is TcxButton then
  begin
    btnAPIJobImageIdx := TcxButton(Sender).OptionsImage.ImageIndex;
  end;

  if (btnAPIJobImageIdx = 2) and (btnAPIJob.Hint <> '') then
    with btnAPIJob.ClientToScreen(Point(X, Y)) do
    HintStyleControllerAPIJobs.ShowHint(X, Y, '', btnAPIJob.Hint);
end;

procedure TdmAPIInProg.btnUpdateAPICallDropDownClick(Sender: TObject);
begin
  APIJobButtonDrowDownClick(Sender);
end;

procedure TdmAPIInProg.btnUpdateAPICallDropDownMenuPopupEx(Sender: TObject; var APopupMenu: TComponent;
  var AHandled: Boolean);
begin
  APIJobButtonDrowDownClick(Sender);
end;

procedure TdmAPIInProg.btnAPIJobClick(Sender: TObject);
var
  //APIJobID: TGUID;
  Form: TForm;
  SetupForm: TfrmAPIInProgSettings;
  ButtonOnSetupForm: Boolean;
  //HostQRecNo: Integer;
  RestAPIQueueeThread: TRestAPIQueueeThread;
  ButtonRestAPIJob: TButtonRestAPIJobRec;

  procedure AddRestCallToHostQueThread;
  begin
//    FRestAPIQueueeThreadList.Items[mtHostQueueshost.AsString].Add(TLooperJob.GetRestCall(mtData, Dict, Scripter,
//      GetPrimaryKeyValue(mtData, PrimaryKey), GetHostBasUrl(mtHostQueueshost.AsString), edtResource.Text, edtParameters.Text));
    if not ButtonOnSetupForm then
      DictFormAPIJobThreadList.Items[Form].Add(TdmAPIInProg.GetRestCall(Dict, dmAPIInProg.mtAPIJobsID.AsGuid, Form, False,
        TCustomButton(Sender), nil, dmAPIInProg.mtAPIJobsLocationHost.AsString, dmAPIInProg.mtAPIJobsRequestResource.AsString,
        dmAPIInProg.mtAPIJobsRequestParams.AsString, dmAPIInProg.mtAPIJobsResponseType.AsString))
    else
      DictFormAPIJobThreadList.Items[Form].Add(TdmAPIInProg.GetRestCall(Dict, SetupForm.mtAPIJobsID.AsGuid, Form, False,
        TCustomButton(Sender), nil, SetupForm.mtAPIJobsLocationHost.AsString, SetupForm.mtAPIJobsRequestResource.AsString,
        SetupForm.mtAPIJobsRequestParams.AsString, SetupForm.mtAPIJobsResponseType.AsString))
  end;

begin
  if not (Sender is TCustomButton) then
    raise Exception.Create('API Job click is not called by a TCustomButton!');

  Form := TForm(GetParentForm(TCustomButton(Sender)));
  if Form is TfrmAPIInProgSettings then
  begin
    ButtonOnSetupForm := True;
    SetupForm := TfrmAPIInProgSettings(Form);
    Form := TfrmAPIInProgSettings(Form).RequestForm;
  end
  else
    ButtonOnSetupForm := False;

  if not Assigned(dmAPIInProg ) then
    dmAPIInProg := TdmAPIInProg.Create(Application);

  if not ButtonOnSetupForm then
  begin
    //if not DictButtonRestAPIJob.TryGetValue(TCustomButton(Sender), APIJobID) then
    if not DictButtonRestAPIJob.TryGetValue(TCustomButton(Sender), ButtonRestAPIJob) then
      raise Exception.Create('There is not an API Job defined which is related to the button!');

    if (not ButtonRestAPIJob.RestAPIJobAssigned) or (ButtonRestAPIJob.RestAPIJobID.IsEmpty) then
      raise Exception.Create('There is not an API Job defined which is related to the button!');

    if not dmAPIInProg.mtAPIJobs.Active then
      dmAPIInProg.LoadAPIJobs;

    dmAPIInProg.mtAPIJobs.IndexFieldNames := 'ID';
    if not dmAPIInProg.mtAPIJobs.Locate('ID', ButtonRestAPIJob.RestAPIJobID.ToString, []) then
      raise Exception.Create(Format('There is not an API Job defined with ID %s',[ButtonRestAPIJob.RestAPIJobID.ToString]));
  end;

  try
    TCustomButton(Sender).Enabled := False;
    if (Sender is TButton) or (Sender is TBitBtn) then
    begin
      TButton(Sender).ImageIndex := 0;
      TButton(Sender).OnMouseLeave := nil;
      TButton(Sender).OnMouseMove := nil;
      TButton(Sender).OnDropDownClick := nil;
      TButton(Sender).Style := bsPushButton;
    end else if Sender is TcxButton then
    begin
      TcxButton(Sender).OptionsImage.ImageIndex := 0;
      TcxButton(Sender).OnMouseLeave := nil;
      TcxButton(Sender).OnMouseMove := nil;
      TcxButton(Sender).OnDropDownMenuPopupEx := nil;
      TcxButton(Sender).Kind := cxbkStandard;
    end;

    if not DictFormAPIJobThreadList.ContainsKey(Form) then
    begin
      RestAPIQueueeThread := TRestAPIQueueeThread.Create;
      DictFormAPIJobThreadList.Add(Form, RestAPIQueueeThread);
      //RestAPIQueueeThread.Host := HostList[i];
      RestAPIQueueeThread.Form := Form;
      RestAPIQueueeThread.OnCompleteRequest := dmAPIInProg.CompleteRequestAPI;
      RestAPIQueueeThread.OnErrorRequest := dmAPIInProg.ErrorRequestAPI;
      RestAPIQueueeThread.Retries := 1;
    end;


    //List := TCollections.CreateList<TRestCall>;
  //  if Host <> '' then
  //    TMSLogger.Info(SLAPIStart)
  //  else
      TMSLogger.Info(SLAPIStart);

//    if not ButtonOnSetupForm then
//    begin
//      grdAPIStatus := TcxGrid(Form.FindComponent('grdAPIStatus'));
//      if grdAPIStatus <> nil then
//      begin
//        grdPos := Form.ScreenToClient(TCustomButton(Sender).ClientToScreen(Point(1, TCustomButton(Sender).Height)));
//
//        if grdPos.Y + grdAPIStatus.Height > Form.ClientHeight then
//        begin
//          grdPos.Y := grdPos.Y - TCustomButton(Sender).Height - grdAPIStatus.Height;
//        end;
//        if grdPos.X + grdAPIStatus.Width > Form.ClientWidth then
//        begin
//          grdPos.X := grdPos.X - grdAPIStatus.Width;
//        end;
//
//        grdAPIStatus.Left := grdPos.X;
//        grdAPIStatus.Top := grdPos.Y;
//        grdAPIStatus.Visible := False;
//      end;
//    end;

    dmAPIInProg.HintStyleControllerAPIJobs.HideHint;

    if not ButtonOnSetupForm then
    begin
      if ButtonRestAPIJob.RestAPIJobExecType = brjeAfterOriginalOnClickEvent then
        ButtonRestAPIJob.OriginalOnClick(Sender);
    end;

    try
      {if Host = '' then
      begin
        mtHostQueues.First;
        while not mtHostQueues.Eof do
        begin
          AddRestCallToHostQueThread(Key);
          mtHostQueues.Next;
        end;
      end else
      begin
        if not mtHostQueues.Locate('host',Host,[]) then
          raise Exception.Create(SEInternalLocate)
        else}
          AddRestCallToHostQueThread;
      //end;
    finally
//      mtHostQueues.RecNo := HostQRecNo;
//      mtHostQueues.EnableControls;
//      dbtvHostQueues.EndUpdate;
      if not ButtonOnSetupForm then
      begin
        if ButtonRestAPIJob.RestAPIJobExecType = brjeBeforeOriginalOnClickEvent then
          ButtonRestAPIJob.OriginalOnClick(Sender);
      end;
    end;
  except
    on e: Exception do
    begin
      TCustomButton(Sender).Enabled := True;
      if (Sender is TButton) or (Sender is TBitBtn) then
        TCustomButton(Sender).ImageIndex := -1
      else if Sender is TcxButton then
        TcxButton(Sender).OptionsImage.ImageIndex := -1;
      Raise;
    end;
  end;
end;

procedure TdmAPIInProg.CompleteRequestAPI(const ARestCall: TRestCall);
var
  //DictResponseJob: TDictionary<TGUID, TDictionary<String, String>>;
  //ResponseJobItem: TPair<TGUID, TDictionary<String, String>>;
  response: TDictionary<string, string>;
  ResponseItem: TPair<string, string>;
  NewFields: ISet<string>;
  Form: TForm;
  mtAPIStatus: TFDMemTable;
  pnlAPIStatus: TSizeablePanel;
  dbtvAPIStatus: TcxGridDBTableView;
  SavedAPIStatusRecNo: Integer;
  fld: TField;
  grdAPIStatus: TcxGrid;
  i, pnlAPIStatus_Width, pnlAPIStatus_Height: Integer;
  NotificationTitle, NotificationHeader, NotificationMsg, NotificationMsgResp: string;
  colGridDBColumn: TcxGridDBColumn;
  RespFldName, RespValue: string;
  colIndex: Integer;
  FrmLastCompletedResAPIJobRec: TFormRestAPIJobRec;
  SendsWindowsNotification: Boolean;

  procedure AddNewFields(AFields: ISet<string>);
  var
    Field: string;
  begin
    for Field in AFields do
    begin
      //mtAPIStatus.AddNewField(Format(SResponseFieldNameFmt, [LowerCase(Field)]), ftWideString, 200);
      //mtAPIStatus.FieldDefs.Add(Format(SResponseFieldNameFmt, [LowerCase(Field)]), ftWideString, 200);
      mtAPIStatus.FieldDefs.Add(Field, ftWideString, 200);
    end;
    mtAPIStatus.CreateDataSet;

    for Field in AFields do
    begin
      colGridDBColumn := dbtvAPIStatus.CreateColumn;
      colGridDBColumn.Name := dbtvAPIStatus.Name + Field;
      colGridDBColumn.Caption := Field;
      //colGridDBColumn.DataBinding.FieldName := Format(SResponseFieldNameFmt, [LowerCase(Field)]);
      colGridDBColumn.DataBinding.FieldName := Field;
      colGridDBColumn.Width := 100;
    end;
  end;

  procedure AddIfNotExistAPIJobResponseFieldsRecord(RespFldName: string);
  begin
    if not mtAPIJobResponseFields.Locate('FormClass;JobID;ResponseFieldName', VarArrayOf([ARestCall.SourceForm.ClassName, ARestCall.APIJobID.ToString, RespFldName])) then
    begin
      mtAPIJobResponseFields.Insert;
      mtAPIJobResponseFieldsFormClass.AsString := ARestCall.SourceForm.ClassName;
      mtAPIJobResponseFieldsJobID.AsGuid := ARestCall.APIJobID;
      mtAPIJobResponseFieldsResponseFieldName.AsString := RespFldName;
      mtAPIJobResponseFieldsShowInResponseGrid.AsBoolean := True;
      mtAPIJobResponseFieldsOrderNo.AsInteger := mtAPIJobResponseFields.RecordCount + 1;
      mtAPIJobResponseFields.Post;
      if mtAPIJobResponseFields.Aggregates[0].InUse and (mtAPIJobResponseFieldsOrderNo.AsInteger <> mtAPIJobResponseFields.Aggregates[0].Value) then
      begin
        mtAPIJobResponseFields.Edit;
        mtAPIJobResponseFieldsOrderNo.AsInteger := mtAPIJobResponseFields.Aggregates[0].Value + 1;
        mtAPIJobResponseFields.Post;
      end;
      mtAPIJobResponseFieldsModified := True;
    end;
  end;

begin

  AddLog(TTMSLoggerLogLevel.Trace, Format(SFmtSuccess, [ARestCall.ResponseText]));

  // Clear previous drow down of calling button and set last completed job of tHe form
  if not DictFormCompletedRestAPIJob.TryGetValue(ARestCall.SourceForm, FrmLastCompletedResAPIJobRec) then
  begin
    FrmLastCompletedResAPIJobRec.APIJobID := TGUID.Empty;
    FrmLastCompletedResAPIJobRec.FromMenuItem := False;
    FrmLastCompletedResAPIJobRec.Button := nil;
    FrmLastCompletedResAPIJobRec.MenuItem := nil;
  end;

  if not FrmLastCompletedResAPIJobRec.APIJobID.IsEmpty then
  begin
    if not FrmLastCompletedResAPIJobRec.FromMenuItem then
    begin
      if(FrmLastCompletedResAPIJobRec.Button <> nil) and
         not (csDestroying in FrmLastCompletedResAPIJobRec.Button.ComponentState) then
      begin
        if (FrmLastCompletedResAPIJobRec.Button is TButton) or (FrmLastCompletedResAPIJobRec.Button is TBitBtn) then
        begin
          TButton(FrmLastCompletedResAPIJobRec.Button).ImageIndex := -1;
          TButton(FrmLastCompletedResAPIJobRec.Button).OnDropDownClick := nil;
          TButton(FrmLastCompletedResAPIJobRec.Button).Style := bsPushButton;
        end else if FrmLastCompletedResAPIJobRec.Button is TcxButton then
        begin
          TcxButton(FrmLastCompletedResAPIJobRec.Button).OptionsImage.ImageIndex := -1;
          TcxButton(FrmLastCompletedResAPIJobRec.Button).OnDropDownMenuPopupEx := nil;
          TcxButton(FrmLastCompletedResAPIJobRec.Button).Kind := cxbkStandard;
        end;
      end
    end
    else
    if (FrmLastCompletedResAPIJobRec.MenuItem <> nil) and
       not (csDestroying in FrmLastCompletedResAPIJobRec.MenuItem.ComponentState) then
    begin
      TMenuItem(FrmLastCompletedResAPIJobRec.MenuItem).ImageIndex := -1;
    end;
  end;

  FrmLastCompletedResAPIJobRec.APIJobID := ARestCall.APIJobID;
  FrmLastCompletedResAPIJobRec.FromMenuItem := ARestCall.FromMenuItem;
  FrmLastCompletedResAPIJobRec.Button := ARestCall.Button;
  FrmLastCompletedResAPIJobRec.MenuItem := ARestCall.MenuItem;
  DictFormCompletedRestAPIJob.AddOrSetValue(ARestCall.SourceForm, FrmLastCompletedResAPIJobRec);

  if ARestCall.APIJobID.IsEmpty then
  begin
    AddLog(TTMSLoggerLogLevel.Error, Format(SFmtError, ['API Job ID stored on rest call is empty.']));
    if ARestCall.ResponseType = 'Blocking' then
    begin
      NotificationMsg := Format('Rest API call is successfully completed. '+sLineBreak+'Response: '+sLineBreak+'%s',
          [ARestCall.ResponseText]);
      ShowMessage(NotificationMsg);
    end;
    Exit;
  end;

  if not mtAPIJobs.Locate('FormClass;ID', vararrayof([ARestCall.SourceForm.ClassName, ARestCall.APIJobID.ToString]), []) then
  begin
    AddLog(TTMSLoggerLogLevel.Error, Format(SFmtError, ['API Job ID stored on rest call could not be found in te list of stored jobs!']));
    if ARestCall.ResponseType = 'Blocking' then
    begin
      NotificationMsg := Format('Rest API call is successfully completed. '+sLineBreak+'Response: '+sLineBreak+'%s',
          [ARestCall.ResponseText]);
      ShowMessage(NotificationMsg);
    end;
    Exit;
  end;

  NotificationMsg := Format('Rest API call of %s with id %s is successfully completed. '+sLineBreak+'Response: '+sLineBreak+'%s',
      [mtAPIJobsCaption.AsString, ARestCall.APIJobID.ToString, ARestCall.ResponseText]);

  if ARestCall.ResponseType = 'Blocking' then
    ShowMessage(NotificationMsg);


  Form := ARestCall.SourceForm;
  if Form = nil then
    Exit;

  if (not ARestCall.FromMenuItem) and (ARestCall.Button <> nil) then
    Form := TForm(GetParentForm(ARestCall.Button));

  if not ARestCall.FromMenuItem then
  begin
    if ARestCall.Button <> nil then
    begin
      if (ARestCall.Button is TButton) or (ARestCall.Button is TBitBtn) then
      begin
        TButton(ARestCall.Button).ImageIndex := 1;
        TButton(ARestCall.Button).Style := bsSplitButton;
        TButton(ARestCall.Button).OnDropDownClick := btnUpdateAPICallDropDownClick;
      end else if ARestCall.Button is TcxButton then
      begin
        TcxButton(ARestCall.Button).OptionsImage.ImageIndex := 1;
        TcxButton(ARestCall.Button).Kind := cxbkDropDownButton;
        TcxButton(ARestCall.Button).OnDropDownMenuPopupEx := btnUpdateAPICallDropDownMenuPopupEx;
      end;
      ARestCall.Button.Enabled := True;
    end;
  end
  else
  begin
    if ARestCall.MenuItem <> nil then
    begin
      ARestCall.MenuItem.ImageIndex := 1;
      ARestCall.MenuItem.Enabled := True;
    end;
  end;

  response := ARestCall.FlatKV;
  if (response <> nil) and (response.Count = 0) then
    AddLog(TTMSLoggerLogLevel.Error, Format(SFmtError, ['Count of response fields is zero.']));

  if (ARestCall.Button <> nil) and
     mtFormButtonJobAssignments.Locate('FormClass;ButtonName',
       VarArrayOf([ARestCall.SourceForm.ClassName, ARestCall.Button.Name]), []) then
    SendsWindowsNotification := mtFormButtonJobAssignmentsSendsWindowsNotification.AsBoolean
  else
    SendsWindowsNotification := mtAPIJobsSendsWindowsNotification.AsBoolean;

  if SendsWindowsNotification then
  begin
    if (ARestCall.Button <> nil) and
       mtFormButtonJobAssignments.Locate('FormClass;ButtonName',
         VarArrayOf([ARestCall.SourceForm.ClassName, ARestCall.Button.Name]), []) and
       mtFormButtonJobAssignmentsCustomizeNotification.AsBoolean then
    begin
      NotificationTitle := mtFormButtonJobAssignmentsSuccessNotificationTitle.AsString;
      NotificationHeader := mtFormButtonJobAssignmentsSuccessNotificationHeader.AsString;
      NotificationMsg := mtFormButtonJobAssignmentsSuccessNotificationMessage.AsString;
    end else
    begin
      NotificationTitle := mtAPIJobsSuccessNotificationTitle.AsString;
      NotificationHeader := mtAPIJobsSuccessNotificationHeader.AsString;
      NotificationMsg := mtAPIJobsSuccessNotificationMessage.AsString;
    end;
//    if (NotificationMsgResp.Length > 2) and (NotificationMsgResp.Substring(NotificationMsgResp.Length - 2) = ', ') then
//      NotificationMsgResp := NotificationMsgResp.Remove(NotificationMsgResp.Length - 2);
//    NotificationMsg := Format('Rest API call of %s is successfully completed. Response: '+sLineBreak+'%s',
//        [mtAPIJobsCaption.AsString, NotificationMsgResp]);

//    SendNotification(ARestCall.APIJobID.ToString+'_'+FormatDateTime('yyyymmdd hhnnss', Now()), NotificationMsg);
    if NotificationTitle.Trim <> '' then
      NotificationTitle := TdmAPIInProg.ResponseNotificationTranslate(NotificationTitle, ARestCall, response);

    if NotificationHeader.Trim <> '' then
      NotificationHeader := TdmAPIInProg.ResponseNotificationTranslate(NotificationHeader, ARestCall, response);

    if NotificationMsg.Trim <> '' then
      NotificationMsg := TdmAPIInProg.ResponseNotificationTranslate(NotificationMsg, ARestCall, response);

    if NotificationTitle = '' then
      ShowMessage('Notification title is not specified for the notification of Rest API Call''s success response!');
    if NotificationMsg = '' then
      ShowMessage('Notification message is not specified for the notification of Rest API Call''s success response!');
    SendNotification(ARestCall.APIJobID.ToString+'_'+FormatDateTime('yyyymmdd hhnnss', Now()), NotificationTitle,
                     NotificationHeader, NotificationMsg);
  end;

  pnlAPIStatus := TSizeablePanel(Form.FindComponent('pnlAPIStatus'));
  if pnlAPIStatus = nil then
    Exit;
  pnlAPIStatus.Visible := False;

  grdAPIStatus := TcxGrid(Form.FindComponent('grdAPIStatus'));

  if grdAPIStatus = nil then
    Exit;

//  if (response <> nil) then
//  begin
//    if not DictResponseQue.ContainsKey(ARestCall.SourceForm) then
//    begin
//      DictResponseJob := TDictionary<TGUID, TDictionary<String, String>>.Create;
//      DictResponseQue.add(ARestCall.SourceForm, DictResponseJob);
//    end else
//    DictResponseJob := DictResponseQue[ARestCall.SourceForm];
//
//    DictResponseJob.AddOrSetValue(ARestCall.APIJobID, response);
//  end;

  mtAPIStatus := TFDMemTable(Form.FindComponent('mtAPIStatus'));
  if mtAPIStatus = nil then
    Exit;

  dbtvAPIStatus := TcxGridDBTableView(grdAPIStatus.Levels[0].GridView);
  if dbtvAPIStatus = nil then
    Exit;

//  if not DictResponseQue.TryGetValue(ARestCall.SourceForm, DictResponseJob) then
//    Exit;

  mtAPIStatus.Close;
  i := 2;
  while mtAPIStatus.FieldDefs.Count > 2 do
  begin
    mtAPIStatus.FieldDefs[i].Free;
    //mtAPIStatus.FieldDefs.Delete(i);
  end;
  i := 1;
  while i < dbtvAPIStatus.ColumnCount do
  begin
    if (dbtvAPIStatus.Columns[i].DataBinding.FieldName <> 'ID') and
       (dbtvAPIStatus.Columns[i].DataBinding.FieldName <> SFieldAPIStatus) then
      dbtvAPIStatus.Columns[i].Free
    else
     i := i + 1;
  end;

  grdAPIStatus.Levels[0].Caption := ARestCall.Resource;

  SavedAPIStatusRecNo := mtAPIStatus.RecNo;
  dbtvAPIStatus.BeginUpdate();
  mtAPIStatus.DisableControls;
  try
    // identify new fields in response
    NewFields := TCollections.CreateSet<string>();
    //for R in FResponseQue do
    //for ResponseJobItem in DictResponseJob do
    //begin
      //Response := ResponseJobItem.Value;
      for ResponseItem in Response do
      begin
        if not SameText(ResponseItem.Key, SFieldAPIStatus) then
          NewFields.Add(ResponseItem.Key);
      end;
    //end;

    {NewFields.RemoveAll(
      function(const F: string):Boolean
      begin
        Result := mtAPIStatus.FindField(Format(SResponseFieldNameFmt, [F])) <> nil;
      end);}

    // add them all if any
    if not NewFields.IsEmpty then
      AddNewFields(NewFields)
    else
      mtAPIStatus.CreateDataSet;

    //for R in FResponseQue do
    //for ResponseJobItem in DictResponseJob do
    //begin
      if (ARestCall.APIJobID.ToString <> '') then
      begin

//      dbtvHostQueues.BeginUpdate;
//      mtHostQueues.DisableControls;
//      SavedHostQueueRecNo := mtHostQueues.RecNo;
//      try
//        if mtHostQueues.Locate('host', ResponseQueListItem.Key, []) then
//        begin
//          if R.Value.ContainsKey(SFieldAPIStatus) then
//            LastMessage := R.Value[SFieldAPIStatus]
//          else
//            LastMessage := '';
//
//          mtHostQueues.Edit;
//          if LastMessage.ToLowerInvariant = 'success' then
//            mtHostQueuesSuccessAmount.Value := mtHostQueuesSuccessAmount.AsInteger + 1
//          else
//            mtHostQueuesErrorAmount.Value := mtHostQueuesErrorAmount.AsInteger + 1;
//          mtHostQueuesLastMessage.Value := LastMessage;
//          mtHostQueues.Post;
//        end
//        else
//        begin
//          AddLog(ResponseQueListItem.Key, TTMSLoggerLogLevel.Error, Format(SFmtError, ['Location qeue record related to response message could not be located.']));
//          Exit;
//        end;
//
//        FullHostName := GetFullHostName(ResponseQueListItem.Key);
//        if (mtAPIStatus.RecordCount = 0) or
//           (((mtAPIStatus.FieldByName('LocationHost').AsString <> mtHostQueueshost.AsString) or
//             (mtAPIStatus.FieldByName(PrimaryKey).AsVariant <> R.Key)) and
//           not mtAPIStatus.Locate('LocationHost;'+PrimaryKey, vararrayof([ResponseQueListItem.Key, R.Key]), [])) then
//        begin
//          mtAPIStatus.Append;
//          mtAPIStatus.FieldByName('LocationHost').AsVariant := ResponseQueListItem.Key;
//          mtAPIStatus.FieldByName('LocationName').AsVariant := FullHostName;
//          mtAPIStatus.FieldByName(PrimaryKey).AsVariant := R.Key;
//        end
//        else
//          mtAPIStatus.Edit;
//
        if (mtAPIStatus.RecordCount = 0) or
             ((mtAPIStatus.FieldByName('ID').AsString <> ARestCall.APIJobID.ToString) and
           not mtAPIStatus.Locate('ID', ARestCall.APIJobID.ToString, [])) then
        begin
          mtAPIStatus.Append;
          mtAPIStatus.FieldByName('ID').AsGuid := ARestCall.APIJobID;
        end
        else
          mtAPIStatus.Edit;

        //Response := ResponseJobItem.Value;
        if Response.ContainsKey(SFieldAPIStatus) then
          mtAPIStatus.FieldByName(SFieldAPIStatus).AsString := Response[SFieldAPIStatus];

        for ResponseItem in Response do
        begin
          //fld := mtAPIStatus.FindField(Format(SResponseFieldNameFmt, [LowerCase(F.Key)]));
          fld := mtAPIStatus.FindField(ResponseItem.Key);
          if Assigned(fld) then
            fld.AsString := ResponseItem.Value;
        end;

        mtAPIStatus.Post;
//        finally
//          mtHostQueues.RecNo := SavedHostQueueRecNo;
//          mtHostQueues.EnableControls;
//          dbtvHostQueues.EndUpdate;
//        end;
      end
      else
      begin
        AddLog(TTMSLoggerLogLevel.Error, Format(SFmtError, ['Job ID related to response message was not specified!']));
        //DictResponseJob.Clear;
        Exit;
      end;
    //end;

  finally

    mtAPIStatus.RecNo := SavedAPIStatusRecNo;

    mtAPIStatus.EnableControls;
    dbtvAPIStatus.EndUpdate;
  end;

  // rearrange result grid
  try
//    pnlAPIStatus_Width := StrToIntDef(dmAPIInProg.IniF.ReadString(ARestCall.Resource + ' ' + SSettingsResultGridSizeSection_Suffix, SSettingsResultridWidthKey, '1030'), 1030);
//    pnlAPIStatus_Height := StrToIntDef(dmAPIInProg.IniF.ReadString(ARestCall.Resource + ' ' + SSettingsResultGridSizeSection_Suffix, SSettingsResultridHeihtKey, '63'), 63);
    if (ARestCall.Button <> nil) and
       mtFormButtonJobAssignments.Locate('FormClass;ButtonName',
         VarArrayOf([ARestCall.SourceForm.ClassName, ARestCall.Button.Name]), []) and
       mtFormButtonJobAssignmentsCustomizeResponseGrid.AsBoolean and
       (mtFormButtonJobAssignmentsResponseGridWidth.AsInteger <> 0) and
       (mtFormButtonJobAssignmentsResponseGridHeight.AsInteger <> 0) then
    begin
      pnlAPIStatus_Width := mtFormButtonJobAssignmentsResponseGridWidth.AsInteger;
      pnlAPIStatus_Height := mtFormButtonJobAssignmentsResponseGridHeight.AsInteger;
    end else if (mtAPIJobsResponseGridWidth.AsInteger <> 0) and (mtAPIJobsResponseGridHeight.AsInteger <> 0) then
    begin
      pnlAPIStatus_Width := mtAPIJobsResponseGridWidth.AsInteger;
      pnlAPIStatus_Height := mtAPIJobsResponseGridHeight.AsInteger;
    end;

    if pnlAPIStatus_Width = 0 then
      pnlAPIStatus_Width := 1030;
    if pnlAPIStatus_Height = 0 then
      pnlAPIStatus_Height := 63;

    if pnlAPIStatus_Width > form.ClientWidth - 10 then
      pnlAPIStatus_Width := form.ClientWidth - 10;

    if pnlAPIStatus_Height > form.ClientHeight - 10 then
      pnlAPIStatus_Height := form.ClientHeight - 10;

    pnlAPIStatus.Width := pnlAPIStatus_Width;
    pnlAPIStatus.Height := pnlAPIStatus_Height;

//    if TFile.Exists(SResultGridViewLayoutFileNamePrefix+'_'+ARestCall.Resource+'.ini') then
//    begin
//      dbtvAPIStatus.RestoreFromIniFile(SResultGridViewLayoutFileNamePrefix+'_'+ARestCall.Resource+'.ini', False, False);
//      i := 0;
//      while i < dbtvAPIStatus.ColumnCount do
//      begin
//       if dbtvAPIStatus.Columns[i].DataBinding.FieldName = '' then
//         dbtvAPIStatus.Columns[i].Free
//       else
//         i := i +1;
//      end;
//    end;

     if (ARestCall.Button <> nil) and
       mtFormButtonJobAssignments.Locate('FormClass;ButtonName',
         VarArrayOf([ARestCall.SourceForm.ClassName, ARestCall.Button.Name]), []) and
       mtFormButtonJobAssignmentsCustomizeResponseGrid.AsBoolean and
       (mtButtonJobAssignmentResponseFields.RecordCount > 0) then
    begin
      colIndex := 0;
      mtButtonJobAssignmentResponseFields.First;
      while not mtButtonJobAssignmentResponseFields.Eof do
      begin
        //if (mtButtonJobAssignmentResponseFieldsResponseFieldName.AsString = 'ID') or
        //   SameText(mtButtonJobAssignmentResponseFieldsResponseFieldName.AsString, SFieldAPIStatus) then
          RespFldName := mtButtonJobAssignmentResponseFieldsResponseFieldName.AsString;
        //else
        //  RespFldName := Format(SResponseFieldNameFmt, [LowerCase(mtButtonJobAssignmentResponseFieldsResponseFieldName.AsString)]);

        colGridDBColumn := dbtvAPIStatus.GetColumnByFieldName(RespFldName);
        if colGridDBColumn <> nil then
        begin
          colGridDBColumn.Visible := mtButtonJobAssignmentResponseFieldsShowInResponseGrid.AsBoolean;
          colIndex := colIndex + 1;
          //colGridDBColumn.Index := colIndex;
          colGridDBColumn.Index := mtButtonJobAssignmentResponseFieldsOrderNo.AsInteger;
          if mtButtonJobAssignmentResponseFieldsResponseGridColumnWidth.AsInteger > 0 then
            colGridDBColumn.Width := mtButtonJobAssignmentResponseFieldsResponseGridColumnWidth.AsInteger;
        end;
        mtButtonJobAssignmentResponseFields.Next;
      end;
    end
    else if mtAPIJobResponseFields.RecordCount > 0 then
    begin
      try
        mtAPIJobResponseFields.IndexName := mtAPIJobResponseFields.Indexes[1].Name;
        // IX_FC_Job_OrderNoDef - FormClass;JobID;OrderNo
        colIndex := 0;
        mtAPIJobResponseFields.First;
        while not mtAPIJobResponseFields.Eof do
        begin
          //if (mtAPIJobResponseFieldsResponseFieldName.AsString = 'ID') or
          //   SameText(mtAPIJobResponseFieldsResponseFieldName.AsString, SFieldAPIStatus) then
            RespFldName := mtAPIJobResponseFieldsResponseFieldName.AsString;
          //else
          //  RespFldName := Format(SResponseFieldNameFmt, [LowerCase(mtAPIJobResponseFieldsResponseFieldName.AsString)]);

          colGridDBColumn := dbtvAPIStatus.GetColumnByFieldName(RespFldName);
          if colGridDBColumn <> nil then
          begin
            colGridDBColumn.Visible := mtAPIJobResponseFieldsShowInResponseGrid.AsBoolean;
            colIndex := colIndex + 1;
            //colGridDBColumn.Index := colIndex;
            colGridDBColumn.Index := mtAPIJobResponseFieldsOrderNo.AsInteger;
            if mtAPIJobResponseFieldsResponseGridColumnWidth.AsInteger > 0  then
              colGridDBColumn.Width := mtAPIJobResponseFieldsResponseGridColumnWidth.AsInteger;
          end;
          mtAPIJobResponseFields.Next;
        end;
      finally
        mtAPIJobResponseFields.IndexName := mtAPIJobResponseFields.Indexes[0].Name;
        // IX_FC_Job_ResponseFieldName - FormClass;JobID;ResponseFieldName
      end;
    end;

   // Updating stored response fields
    try
      //mtAPIJobResponseFields.Filter :=
      //  'FormClass = '+QuotedStr(Form.ClassName) + ' AND JobID = '+ QuotedStr(ARestCall.APIJobID.ToString);
      //mtAPIJobResponseFields.Filtered := True;

      // Checking existence of extra fields or fields which are n longer in the reponse
      mtAPIJobResponseFields.First;
      while not mtAPIJobResponseFields.Eof do
      begin
        if (mtAPIJobResponseFieldsResponseFieldName.AsString = 'ID') or
           (mtAPIJobResponseFieldsResponseFieldName.AsString = SFieldAPIStatus) then
        begin
          mtAPIJobResponseFields.Next;
          continue;
        end;

        //if SameText(mtAPIJobResponseFieldsResponseFieldName.AsString, SFieldAPIStatus) then
          RespFldName := mtAPIJobResponseFieldsResponseFieldName.AsString;
        //else
        //  RespFldName := Format(SResponseFieldNameFmt, [LowerCase(mtAPIJobResponseFieldsResponseFieldName.AsString)]);

        fld := mtAPIStatus.FindField(RespFldName);
        if fld = nil then
        begin
          mtAPIJobResponseFields.Delete;
          mtAPIJobResponseFieldsModified := True;
        end
        else
          mtAPIJobResponseFields.Next;
      end;

      // Checking new fields in te response
      AddIfNotExistAPIJobResponseFieldsRecord('ID');
      AddIfNotExistAPIJobResponseFieldsRecord(SFieldAPIStatus);
      //for ResponseJobItem in DictResponseJob do
      //begin
        if (ARestCall.APIJobID.ToString <> '') then
        begin
          //Response := ResponseJobItem.Value;
          for ResponseItem in Response do
          begin
            //if SameText(F.Key, SFieldAPIStatus) then
            RespFldName := ResponseItem.Key;
            AddIfNotExistAPIJobResponseFieldsRecord(RespFldName);

          end;
        end
      //end;

    finally
      //mtAPIJobResponseFields.Filter := '';
      //mtAPIJobResponseFields.Filtered := False;
    end;

  finally
    //DictResponseJob.Clear;
  end;

end;

procedure TdmAPIInProg.ErrorRequestAPI(const ARestCall: TRestCall; const AErrorMessage: string);
var
  //DictResponseJob: TDictionary<TGUID, TDictionary<String, String>>;
  //DictResponseFields: TDictionary<string, string>;
  RestCallCaption, NotificationTitle, NotificationHeader, NotificationMsg: string;
  SendsWindowsNotification: Boolean;
begin
  AddLog(TTMSLoggerLogLevel.Error, Format(SFmtError, [AErrorMessage]));

  if ARestCall.FromMenuItem then
    RestCallCaption := TMenuItem(ARestCall.MenuItem).Caption
  else
    RestCallCaption := TButton(ARestCall.Button).Caption; // custombutton caption is private and can not be accessible

  NotificationMsg := Format('Rest API call of %s with ID %s is successfully completed with an error. '+sLineBreak+'Error: '+sLineBreak+'%s',
      [RestCallCaption, ARestCall.APIJobID.ToString, AErrorMessage]);

  if (ARestCall.Button <> nil) and
     mtFormButtonJobAssignments.Locate('FormClass;ButtonName',
       VarArrayOf([ARestCall.SourceForm.ClassName, ARestCall.Button.Name]), []) then
    SendsWindowsNotification := mtFormButtonJobAssignmentsSendsWindowsNotification.AsBoolean
  else
    SendsWindowsNotification := mtAPIJobsSendsWindowsNotification.AsBoolean;

  if SendsWindowsNotification then
  begin
    if (ARestCall.Button <> nil) and
       mtFormButtonJobAssignments.Locate('FormClass;ButtonName',
         VarArrayOf([ARestCall.SourceForm.ClassName, ARestCall.Button.Name]), []) and
       mtFormButtonJobAssignmentsCustomizeNotification.AsBoolean then
    begin
      NotificationTitle := mtFormButtonJobAssignmentsErrorNotificationTitle.AsString;
      NotificationHeader := mtFormButtonJobAssignmentsErrorNotificationHeader.AsString;
      NotificationMsg := mtFormButtonJobAssignmentsErrorNotificationMessage.AsString;
    end else
    begin
      NotificationTitle := mtAPIJobsErrorNotificationTitle.AsString;
      NotificationHeader := mtAPIJobsErrorNotificationHeader.AsString;
      NotificationMsg := mtAPIJobsErrorNotificationMessage.AsString;
    end;

    if NotificationHeader <> '' then
      NotificationHeader := AErrorMessage + ' ' + NotificationHeader
    else;
      NotificationHeader := AErrorMessage;

    if NotificationTitle.Trim <> '' then
      NotificationTitle := TdmAPIInProg.ResponseNotificationTranslate(NotificationTitle, ARestCall);

    if NotificationHeader.Trim <> '' then
      NotificationHeader := TdmAPIInProg.ResponseNotificationTranslate(NotificationHeader, ARestCall);

    if NotificationMsg.Trim <> '' then
      NotificationMsg := TdmAPIInProg.ResponseNotificationTranslate(NotificationMsg, ARestCall);

    if NotificationTitle = '' then
      ShowMessage('Notification title is not specified for the notification of Rest API Call''s error response!');

    SendNotification(ARestCall.APIJobID.ToString+'_'+FormatDateTime('yyyymmdd hhnnss', Now()),
      NotificationTitle, NotificationHeader, NotificationMsg);
  end;

  if ARestCall.ResponseType = 'Blocking' then
    ShowMessage(NotificationMsg);

  if not ARestCall.FromMenuItem then
    if ARestCall.Button <> nil then
    begin
      ARestCall.Button.Hint := AErrorMessage;
      if (ARestCall.Button is TButton) or (ARestCall.Button is TBitBtn) then
      begin
        TButton(ARestCall.Button).ImageIndex := 2;
        if not ARestCall.SourceForm.ShowHint then
          ARestCall.SourceForm.ShowHint := True;
        if not Application.ShowHint then
          Application.ShowHint := True;
      end else if ARestCall.Button is TcxButton then
      begin
        TcxButton(ARestCall.Button).OptionsImage.ImageIndex := 2;
        TcxButton(ARestCall.Button).OnMouseLeave := dmAPIInProg.btnAPIJobMouseLeave;
        TcxButton(ARestCall.Button).OnMouseMove := dmAPIInProg.btnAPIJobMouseMove;
      end;
      ARestCall.Button.Enabled := True;
    end
    else
  else
    if ARestCall.MenuItem <> nil then
    begin
      ARestCall.MenuItem.ImageIndex := 2;
      ARestCall.MenuItem.Enabled := True;
      if not ARestCall.SourceForm.ShowHint then
        ARestCall.SourceForm.ShowHint := True;
      if not Application.ShowHint then
        Application.ShowHint := True;
      ARestCall.MenuItem.Hint := AErrorMessage;
    end;

  //if ARestCall.APIJobID.ToString <> '' then
  //begin
//    DictResponseFields := TDictionary<string, string>.Create;
//    DictResponseFields.AddOrSetValue(SFieldAPIStatus, AErrorMessage);
//    if not DictResponseQue.ContainsKey(ARestCall.SourceForm) then
//    begin
//      DictResponseJob := TDictionary<TGUID, TDictionary<String, String>>.Create;
//      DictResponseQue.add(ARestCall.SourceForm, DictResponseJob);
//    end else
//    DictResponseJob := DictResponseQue[ARestCall.SourceForm];
//    //FResponseQue.AddOrSetValue(ARestCall.PrimaryKeyValue, CreateDictionaryStringString(SFieldAPIStatus, AErrorMessage));
//    DictResponseJob.AddOrSetValue(ARestCall.APIJobID, DictResponseFields);
  //end;

end;

procedure TdmAPIInProg.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  TotalQueListCount: Integer;
  item: TPair<TForm, TRestAPIQueueeThread>;
begin
  if Sender <> Application.MainForm then
  begin
    if Assigned(OrgFormCloseQuery) then
      OrgFormCloseQuery(Sender, CanClose);
    Exit;
  end;

  TotalQueListCount := 0;
  for item in DictFormAPIJobThreadList do
    TotalQueListCount := TotalQueListCount + item.Value.ListCount;

  CanClose := TotalQueListCount = 0;

  if CanClose then
  begin
    for item in DictFormAPIJobThreadList do
    begin
      if Assigned(item.Value) then
      begin
        item.Value.ClearList;
        item.Value.Terminate;
      end;
    end;
    for item in DictFormAPIJobThreadList do
    begin
      if Assigned(item.Value) then
        item.Value.WaitFor;
    end;
    if Assigned(OrgFormCloseQuery) then
      OrgFormCloseQuery(Sender, CanClose);
  end;
end;

procedure TdmAPIInProg.LoadAPIJobs;
var
  js: TJSONObject;
begin
  if TFile.Exists(REQUEST_SETTINGS_FILENAME) then
  begin
    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(REQUEST_SETTINGS_FILENAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtAPIJobs);
      finally
        js.Free;
      end;
    except
      on e1: Exception do
        MessageDlg('An error occured while loading API Job Response Fields Dataset. Error: '+ e1.Message,
                   TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
    end;
  end;

  if not mtAPIJobs.Active then
    mtAPIJobs.Active := True;

  mtAPIJobs1.CloneCursor(mtAPIJobs, True, False);
  mtAPIJobs1.IndexFieldNames := 'FormClass;GroupID;Order';

  if TFile.Exists(RESPONSE_FIELDS_FILENAME) then
  begin
    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(RESPONSE_FIELDS_FILENAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtAPIJobResponseFields);
      finally
        js.Free;
      end;
    except
      on e2: Exception do
        MessageDlg('An error occured while loading API Job Response Fields Dataset. Error: '+ e2.Message,
                   TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
    end;
  end;

  if not mtAPIJobResponseFields.Active then
    mtAPIJobResponseFields.Active := True;
end;

procedure TdmAPIInProg.LoadAssignments;
var
  SavedName: string;
  js: TJSONObject;
begin
  if TFile.Exists(BUTTON_JOB_ASSIGNMENTS_FILENAME) then
  begin
    if mtFormButtonJobAssignments.RecordCount > 0 then
      SavedName := mtFormButtonJobAssignmentsButtonName.AsString
    else
      SavedName := '';

    mtFormButtonJobAssignments.Filtered := False;

    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(BUTTON_JOB_ASSIGNMENTS_FILENAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtFormButtonJobAssignments);
      finally
        js.Free;
      end;
    except
      on e1: Exception do
        MessageDlg('An error occured while loading dataset of Button API Job Assignments. Error: '+ e1.Message,
                   TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
    end;

    mtFormButtonJobAssignments.Filtered := mtFormButtonJobAssignments.Filter <> '';

    if (SavedName <> '') and mtFormButtonJobAssignments.Active then
      mtFormButtonJobAssignments.Locate('ButtonName', SavedName, []);
  end;

  if not mtFormButtonJobAssignments.Active  then
    mtFormButtonJobAssignments.Active := True;

  if TFile.Exists(BUTTON_JOB_ASSIGNMENTS_RESPONSE_FIELDS_FILENAME) then
  begin
    mtButtonJobAssignmentResponseFields.Filtered := False;

    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(BUTTON_JOB_ASSIGNMENTS_RESPONSE_FIELDS_FILENAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtButtonJobAssignmentResponseFields);
      finally
        js.Free;
      end;
    except
      on e2: Exception do
        MessageDlg('An error occured while loading dataset of Button API Job Assignment Response Fields. Error: '+ e2.Message,
                   TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
    end;
  end;

  if not mtButtonJobAssignmentResponseFields.Active  then
    mtButtonJobAssignmentResponseFields.Active := True;

  if TFile.Exists(GRID_JOB_ASSIGNMENTS_FILENAME) then
  begin
    if mtFormGridJobAssignments.RecordCount > 0 then
      SavedName := mtFormGridJobAssignmentsGridName.AsString
    else
      SavedName := '';

    mtFormGridJobAssignments.Filtered := False;

    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(GRID_JOB_ASSIGNMENTS_FILENAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtFormGridJobAssignments);
      finally
        js.Free;
      end;
    except
      on e3: Exception do
        MessageDlg('An error occured while loading dataset of Grid API Job Assignments. Error: '+ e3.Message,
                   TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
    end;

    mtFormGridJobAssignments.Filtered := mtFormGridJobAssignments.Filter <> '';

    if (SavedName <> '') and mtFormGridJobAssignments.Active then
      mtFormGridJobAssignments.Locate('GridName', SavedName, []);
  end;

  if not mtFormGridJobAssignments.Active  then
    mtFormGridJobAssignments.Active := True;

  if TFile.Exists(MENU_JOB_ASSIGNMENTS_FILENAME) then
  begin
    if mtFormMenuJobAssignments.RecordCount > 0 then
      SavedName := mtFormMenuJobAssignmentsMenuName.AsString
    else
      SavedName := '';

    mtFormMenuJobAssignments.Filtered := False;

    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(MENU_JOB_ASSIGNMENTS_FILENAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtFormMenuJobAssignments);
      finally
        js.Free;
      end;
    except
      on e4: Exception do
        MessageDlg('An error occured while loading dataset of Menu API Job Assignments. Error: '+ e4.Message,
                   TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
    end;

    mtFormMenuJobAssignments.Filtered := mtFormMenuJobAssignments.Filter <> '';

    if (SavedName <> '') and mtFormMenuJobAssignments.Active then
      mtFormMenuJobAssignments.Locate('MenuName', SavedName, []);
  end;

  if not mtFormMenuJobAssignments.Active  then
    mtFormMenuJobAssignments.Active := True;
end;

procedure TdmAPIInProg.SaveAPIJobs;
var
  js: TJSONObject;
begin
  js := MT2JS(mtAPIJobs);
  try
    TFile.WriteAllText(REQUEST_SETTINGS_FILENAME, js.ToString);
  finally
    js.Free;
  end;

  if mtAPIJobs.UpdatesPending then
    mtAPIJobs.CommitUpdates;

  js := MT2JS(mtAPIJobResponseFields);
  try
    TFile.WriteAllText(RESPONSE_FIELDS_FILENAME, js.ToString);
  finally
    js.Free;
  end;

  if mtAPIJobResponseFields.UpdatesPending then
    mtAPIJobResponseFields.CommitUpdates;
end;

procedure TdmAPIInProg.SaveButtonJobAssignments;
var
  js: TJSONObject;
begin
  if mtFormButtonJobAssignments.State in dsEditModes then
    mtFormButtonJobAssignments.Post;

  js := MT2JS(mtFormButtonJobAssignments);
  try
    TFile.WriteAllText(BUTTON_JOB_ASSIGNMENTS_FILENAME, js.ToString);
  finally
    js.Free;
  end;

  if mtFormButtonJobAssignments.UpdatesPending then
    mtFormButtonJobAssignments.CommitUpdates;

  js := MT2JS(mtButtonJobAssignmentResponseFields);
  try
    TFile.WriteAllText(BUTTON_JOB_ASSIGNMENTS_RESPONSE_FIELDS_FILENAME, js.ToString);
  finally
    js.Free;
  end;

  if mtButtonJobAssignmentResponseFields.UpdatesPending then
    mtButtonJobAssignmentResponseFields.CommitUpdates;
end;

procedure TdmAPIInProg.miAPIJobClick(Sender: TObject);
var
  APIJobID: TGUID;
  Form: TForm;
  //HostQRecNo: Integer;
  RestAPIQueueeThread: TRestAPIQueueeThread;
  pnlAPIStatus: TSizeablePanel;
  grdAPIStatus: TcxGrid;

  procedure AddRestCallToHostQueThread;
  begin
//    FRestAPIQueueeThreadList.Items[mtHostQueueshost.AsString].Add(TLooperJob.GetRestCall(mtData, Dict, Scripter,
//      GetPrimaryKeyValue(mtData, PrimaryKey), GetHostBasUrl(mtHostQueueshost.AsString), edtResource.Text, edtParameters.Text));
    DictFormAPIJobThreadList.Items[Form].Add(TdmAPIInProg.GetRestCall(Dict, dmAPIInProg.mtAPIJobsID.AsGuid, Form, True,
      nil, TMenuItem(Sender), dmAPIInProg.mtAPIJobsLocationHost.AsString, dmAPIInProg.mtAPIJobsRequestResource.AsString,
      dmAPIInProg.mtAPIJobsRequestParams.AsString, dmAPIInProg.mtAPIJobsResponseType.AsString));
  end;

begin
  if not (Sender is TMenuItem) then
    raise Exception.Create('API Job click is not called by a Menu item!');

   if not DictMenuItemAPIJobID.TryGetValue(TMenuItem(Sender), APIJobID) then
     raise Exception.Create('There is not an API Job defined which is related to the menu item!');

  if not Assigned(dmAPIInProg ) then
    dmAPIInProg := TdmAPIInProg.Create(Application);

  if not dmAPIInProg.mtAPIJobs.Active then
    dmAPIInProg.LoadAPIJobs;

  dmAPIInProg.mtAPIJobs.IndexFieldNames := 'ID';
  if not dmAPIInProg.mtAPIJobs.Locate('ID', APIJobID.ToString, []) then
    raise Exception.Create(Format('There is not an API Job defined with ID %s',[APIJobID.ToString]));

  if TMenuItem(Sender).GetParentMenu.Owner is TForm then
    Form := TForm(TMenuItem(Sender).GetParentMenu.Owner)
  else
    raise Exception.Create(Format('Form can not be determined for an API Job defined with ID %s and assigned to the menu item',[APIJobID.ToString]));

  if TMenuItem(Sender).ImageIndex = 1 then
  begin
    try
      TMenuItem(Sender).Enabled := False;
      pnlAPIStatus := TSizeablePanel(Form.FindComponent('pnlAPIStatus'));
      if pnlAPIStatus <> nil then
      begin
        if not pnlAPIStatus.Visible then
        begin
          RepositionResultPanel(Form, nil, pnlAPIStatus);

        end;
        pnlAPIStatus.Visible := not pnlAPIStatus.Visible;

        if pnlAPIStatus.Visible then
        begin
          // Response panel is shown then reset te menuiem to initial state.
          TMenuItem(Sender).ImageIndex := -1;
          // Focus into the grid
          grdAPIStatus := TcxGrid(Form.FindComponent('grdAPIStatus'));
          if grdAPIStatus <> nil then
            grdAPIStatus.SetFocus;
        end;
      end;

    finally
      TMenuItem(Sender).Enabled := True;
    end;
    exit;
  end;

  if not DictFormAPIJobThreadList.ContainsKey(Form) then
  begin
    RestAPIQueueeThread := TRestAPIQueueeThread.Create;
    DictFormAPIJobThreadList.Add(Form, RestAPIQueueeThread);
    //RestAPIQueueeThread.Host := HostList[i];
    RestAPIQueueeThread.Form := Form;
    RestAPIQueueeThread.OnCompleteRequest := dmAPIInProg.CompleteRequestAPI;
    RestAPIQueueeThread.OnErrorRequest := dmAPIInProg.ErrorRequestAPI;
    RestAPIQueueeThread.Retries := 1;
  end;


  //List := TCollections.CreateList<TRestCall>;
//  if Host <> '' then
//    TMSLogger.Info(SLAPIStart)
//  else
    TMSLogger.Info(SLAPIStart);

  TMenuItem(Sender).Enabled := False;
  TMenuItem(Sender).ImageIndex := 0;
  TMenuItem(Sender).Hint := '';

  {

  dmAPIInProg.HintStyleControllerAPIJobs.HideHint;}

  try
    try
      {if Host = '' then
      begin
        mtHostQueues.First;
        while not mtHostQueues.Eof do
        begin
          AddRestCallToHostQueThread(Key);
          mtHostQueues.Next;
        end;
      end else
      begin
        if not mtHostQueues.Locate('host',Host,[]) then
          raise Exception.Create(SEInternalLocate)
        else}
          AddRestCallToHostQueThread;
      //end;
    finally
  //      mtHostQueues.RecNo := HostQRecNo;
  //      mtHostQueues.EnableControls;
  //      dbtvHostQueues.EndUpdate;
    end;
  except
    on e: Exception do
    begin
      TMenuItem(Sender).Enabled := True;
      TMenuItem(Sender).ImageIndex := -1;
      Raise;
    end;
  end;
end;

procedure TdmAPIInProg.miSaveLayoutClick(Sender: TObject);
var
  pnlAPIStatus: TSizeablePanel;
  grdAPIStatus: TcxGrid;
  Form, SourceForm: TForm;
  dbtvAPIStatus: TcxGridDBTableView;
  SenderButton: TCustomButton;
  mtAPIStatus: TFDMemTable;
  i: Integer;
begin
  if TMenuItem(Sender).GetParentMenu.Owner is TForm then
    Form := TForm(TMenuItem(Sender).GetParentMenu.Owner)
  else
    raise Exception.Create('Form can not be determined!');

  grdAPIStatus := TcxGrid(Form.FindComponent('grdAPIStatus'));

  pnlAPIStatus := TSizeablePanel(grdAPIStatus.Parent);

  dbtvAPIStatus := TcxGridDBTableView(grdAPIStatus.Levels[0].GridView);
  if dbtvAPIStatus = nil then
    Exit;

  //dbtvAPIStatus.StoreToIniFile(SResultGridViewLayoutFileNamePrefix+'_'+grdAPIStatus.Levels[0].Caption+'.ini');
//  IniF.WriteString(grdAPIStatus.Levels[0].Caption + ' ' + SSettingsResultGridSizeSection_Suffix, SSettingsResultridWidthKey, IntToStr(grdAPIStatus.Parent.Width));
//  IniF.WriteString(grdAPIStatus.Levels[0].Caption + ' ' + SSettingsResultGridSizeSection_Suffix, SSettingsResultridHeihtKey, IntToStr(grdAPIStatus.Parent.Height));

  if mtAPIJobResponseFields.RecordCount = 0 then
    Exit;
  mtAPIStatus := TFDMemTable(Form.FindComponent('mtAPIStatus'));
  if not mtAPIJobs.Locate('ID', mtAPIStatus.FieldByName('ID').AsString, []) then
    Exit;

  SenderButton := nil;
  if (pnlAPIStatus.Hint <> '') then
    SenderButton := TCustomButton(Form.FindComponent(pnlAPIStatus.Hint));

  if Form is TfrmAPIInProgSettings then
    SourceForm := TfrmAPIInProgSettings(Form).RequestForm
  else
    SourceForm := Form;

  if (SenderButton <> nil) and
     mtFormButtonJobAssignments.Locate('FormClass;ButtonName', VarArrayOf([SourceForm.ClassName, SenderButton.Name]), []) and
     mtFormButtonJobAssignmentsCustomizeResponseGrid.AsBoolean then
  begin
    mtFormButtonJobAssignments.Edit;
    mtFormButtonJobAssignmentsResponseGridWidth.AsInteger := pnlAPIStatus.Width;
    mtFormButtonJobAssignmentsResponseGridHeight.AsInteger := pnlAPIStatus.Height;
    mtFormButtonJobAssignments.Post;
    for i := 0 to dbtvAPIStatus.ColumnCount - 1 do
    begin
      if mtButtonJobAssignmentResponseFields.Locate('ResponseFieldName', dbtvAPIStatus.Columns[i].DataBinding.FieldName) then
      begin
        mtButtonJobAssignmentResponseFields.Edit;
        mtButtonJobAssignmentResponseFieldsOrderNo.AsInteger := dbtvAPIStatus.Columns[i].Index;
        mtButtonJobAssignmentResponseFieldsShowInResponseGrid.AsBoolean := dbtvAPIStatus.Columns[i].Visible;
        mtButtonJobAssignmentResponseFieldsResponseGridColumnWidth.AsInteger := dbtvAPIStatus.Columns[i].Width;
        mtButtonJobAssignmentResponseFields.Post;
      end;
    end;

    SaveButtonJobAssignments;
  end else
  begin
    mtAPIJobs.Edit;
    mtAPIJobsResponseGridWidth.AsInteger := pnlAPIStatus.Width;
    mtAPIJobsResponseGridHeight.AsInteger := pnlAPIStatus.Height;
    mtAPIJobs.Post;
    for i := 0 to dbtvAPIStatus.ColumnCount - 1 do
    begin
      if mtAPIJobResponseFields.Locate('ResponseFieldName', dbtvAPIStatus.Columns[i].DataBinding.FieldName) then
      begin
        mtAPIJobResponseFields.Edit;
        mtAPIJobResponseFieldsOrderNo.AsInteger := dbtvAPIStatus.Columns[i].Index;
        mtAPIJobResponseFieldsShowInResponseGrid.AsBoolean := dbtvAPIStatus.Columns[i].Visible;
        mtAPIJobResponseFieldsResponseGridColumnWidth.AsInteger := dbtvAPIStatus.Columns[i].Width;
        mtAPIJobResponseFields.Post;
      end;
    end;
    SaveAPIJobs;
  end;
end;

procedure TdmAPIInProg.FormResize(Sender: TObject);
var
  OrgFormResize: TNotifyEvent;
  pnlAPIStatus: TSizeablePanel;
  SenderButton: TCustomButton;
begin
  if DictFormRestAPIFormResize.TryGetValue(TForm(Sender), OrgFormResize) then
    if Assigned(OrgFormResize) then
      OrgFormResize(Sender);
  pnlAPIStatus := TSizeablePanel(TForm(Sender).FindComponent('pnlAPIStatus'));
  if (pnlAPIStatus = nil) or (not pnlAPIStatus.Visible) then
    Exit;
  SenderButton := nil;
  if (pnlAPIStatus.Hint <> '') then
    SenderButton := TCustomButton(TForm(Sender).FindComponent(pnlAPIStatus.Hint));

  if SenderButton <> nil then
    RepositionResultPanel(TForm(Sender), SenderButton, pnlAPIStatus, True)
  else
    RepositionResultPanel(TForm(Sender), nil, pnlAPIStatus, True);
end;

procedure CreateAPIJobContextMenuItem(form: TForm; GroupID: string = '');
var
  grdAPIStatus: TcxGrid;
  mi : TMenuItem;
  SavedGroupID: string;
  NoPopupMenu: Boolean;
  APISubmenu: TMenuItem;
begin
  try
    if form = nil then
      raise Exception.Create('For creating context menu items of API jobs, related form must be supplied!');

    if not Assigned(dmAPIInProg ) then
      dmAPIInProg := TdmAPIInProg.Create(Application);

    if not dmAPIInProg.mtAPIJobs.Active then
      dmAPIInProg.LoadAPIJobs;

    dmAPIInProg.mtAPIJobs1.Filter := 'FormClass = ' + QuotedStr(form.ClassName);
    if GroupId <> '' then
      dmAPIInProg.mtAPIJobs1.Filter := dmAPIInProg.mtAPIJobs1.Filter + ' AND GroupID = ' + QuotedStr(GroupID);

    dmAPIInProg.mtAPIJobs1.Filtered := True;
    if dmAPIInProg.mtAPIJobs1.RecordCount = 0 then
    begin
      if GroupId = '' then
        MessageDlg('No Rest API job is defined for the form!', TMsgDlgType.mtWarning, [TMsgDlgBtn.mbOK], 0)
      else
        MessageDlg(Format('No Rest API job is defined for the form with the ''%s'' Group ID!', [GroupId]), TMsgDlgType.mtWarning, [TMsgDlgBtn.mbOK], 0);
      Exit;
    end;

    NoPopupMenu := True;
    if form.PopupMenu = nil then
    begin
      form.PopupMenu := TPopupMenu.Create(form);
      form.PopupMenu.Name := 'pumRestAPIJobs';
      form.PopupMenu.Images := dmAPIInProg.ilAPIJobs;
    end
    else if form.PopupMenu.Name <> 'pumRestAPIJobs' then
    begin
      NoPopupMenu := False;
      APISubmenu := TMenuItem(Application.FindComponent('miRestAPIJobs'));
      if APISubmenu = nil then
      begin
        mi := TMenuItem.Create(form);
        mi.Caption := '-';
        form.PopupMenu.Items.add(mi);
        mi := TMenuItem.Create(form);
        mi.Name := 'miRestAPIJobs';
        mi.Caption := 'Rest API Jobs';
        mi.SubMenuImages := dmAPIInProg.ilAPIJobs;
        form.PopupMenu.Items.add(mi);
        APISubmenu := mi;
      end;
    end;

    dmAPIInProg.mtAPIJobs1.First;
    SavedGroupID := dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString;
    while not dmAPIInProg.mtAPIJobs1.Eof do
    begin
      if not DictMenuItemAPIJobID.ContainsValue(dmAPIInProg.mtAPIJobs1.FieldByName('ID').AsGUID) then
      begin
        if dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString <> SavedGroupID then
        begin
          mi := TMenuItem.Create(form);
          mi.Caption := '-';
          if NoPopupMenu then
            form.PopupMenu.Items.add(mi)
          else
            APISubmenu.Add(mi);
          SavedGroupID := dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString;
        end;

        mi := TMenuItem.Create(form);
        mi.Caption := dmAPIInProg.mtAPIJobs1.FieldByName('Caption').AsString;
        mi.OnClick := dmAPIInProg.miAPIJobClick;
        DictMenuItemAPIJobID.AddOrSetValue(mi, dmAPIInProg.mtAPIJobs1.FieldByName('ID').AsGUID);

        if NoPopupMenu then
          form.PopupMenu.Items.add(mi)
        else
          APISubmenu.Add(mi);
      end;
      dmAPIInProg.mtAPIJobs1.Next;
    end;

    ApplicationCloseCheck(Form);
    FormResizeCheck(Form);
    PrepareRequestResultControls(Form, grdAPIStatus);

  except
    on e: Exception do
     MessageDlg(e.Message, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
  end;
end;

procedure CreateGridAPIJobContextMenuItem(grid: TCustomGrid; GroupID: string = '');
var
  grdAPIStatus: TcxGrid;
  mi : TMenuItem;
  SavedGroupID: string;
  NoPopupMenu: Boolean;
  APISubmenu: TMenuItem;
  form: TForm;
begin
  try
    if grid = nil then
      raise Exception.Create('For creating context menu items of API jobs for grids, related grid must be supplied!');

    if not Assigned(dmAPIInProg ) then
      dmAPIInProg := TdmAPIInProg.Create(Application);

    if not dmAPIInProg.mtAPIJobs.Active then
      dmAPIInProg.LoadAPIJobs;

    form := TForm(GetParentForm(grid));

    dmAPIInProg.mtAPIJobs1.Filter := 'FormClass = ' + QuotedStr(form.ClassName);
    if GroupId <> '' then
      dmAPIInProg.mtAPIJobs1.Filter := dmAPIInProg.mtAPIJobs1.Filter + ' AND GroupID = ' + QuotedStr(GroupID);

    dmAPIInProg.mtAPIJobs1.Filtered := True;
    if dmAPIInProg.mtAPIJobs1.RecordCount = 0 then
    begin
      if GroupId = '' then
        MessageDlg('No Rest API job is defined for the form!', TMsgDlgType.mtWarning, [TMsgDlgBtn.mbOK], 0)
      else
        MessageDlg(Format('No Rest API job is defined for the form with the ''%s'' Group ID!', [GroupId]), TMsgDlgType.mtWarning, [TMsgDlgBtn.mbOK], 0);
      Exit;
    end;

    NoPopupMenu := True;
    if TCustomGridAccess(grid).PopupMenu = nil then
    begin
      TCustomGridAccess(grid).PopupMenu := TPopupMenu.Create(form);
      TCustomGridAccess(grid).PopupMenu.Name := grid.Name + '_pumRestAPIJobs';
      TCustomGridAccess(grid).PopupMenu.Images := dmAPIInProg.ilAPIJobs;
    end
    else if TCustomGridAccess(grid).PopupMenu.Name <> grid.Name + '_pumRestAPIJobs' then
    begin
      NoPopupMenu := False;
      APISubmenu := TMenuItem(form.FindComponent(grid.Name + '_miRestAPIJobs'));
      if APISubmenu = nil then
      begin
        mi := TMenuItem.Create(form);
        mi.Caption := '-';
        TCustomGridAccess(grid).PopupMenu.Items.add(mi);
        mi := TMenuItem.Create(form);
        mi.Name := grid.Name + '_miRestAPIJobs';
        mi.Caption := 'Rest API Jobs';
        mi.SubMenuImages := dmAPIInProg.ilAPIJobs;
        TCustomGridAccess(grid).PopupMenu.Items.add(mi);
        APISubmenu := mi;
      end;
    end;

    dmAPIInProg.mtAPIJobs1.First;
    SavedGroupID := dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString;
    while not dmAPIInProg.mtAPIJobs1.Eof do
    begin
      //if not DictMenuItemAPIJobID.ContainsValue(dmAPIInProg.mtAPIJobs1.FieldByName('ID').AsGUID) then
      //begin
        if dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString <> SavedGroupID then
        begin
          mi := TMenuItem.Create(form);
          mi.Caption := '-';
          if NoPopupMenu then
            TCustomGridAccess(grid).PopupMenu.Items.add(mi)
          else
            APISubmenu.Add(mi);
          SavedGroupID := dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString;
        end;

        mi := TMenuItem.Create(form);
        mi.Caption := dmAPIInProg.mtAPIJobs1.FieldByName('Caption').AsString;
        mi.OnClick := dmAPIInProg.miAPIJobClick;
        DictMenuItemAPIJobID.AddOrSetValue(mi, dmAPIInProg.mtAPIJobs1.FieldByName('ID').AsGUID);

        if NoPopupMenu then
          TCustomGridAccess(grid).PopupMenu.Items.add(mi)
        else
          APISubmenu.Add(mi);
      //end;
      dmAPIInProg.mtAPIJobs1.Next;
    end;

    ApplicationCloseCheck(Form);
    FormResizeCheck(Form);
    PrepareRequestResultControls(Form, grdAPIStatus);

  except
    on e: Exception do
     MessageDlg(e.Message, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
  end;
end;

procedure CreatecxGridAPIJobContextMenuItem(grid: TcxCustomGrid; GroupID: string = '');
var
  grdAPIStatus: TcxGrid;
  mi : TMenuItem;
  SavedGroupID: string;
  NoPopupMenu: Boolean;
  APISubmenu: TMenuItem;
  form: TForm;
begin
  try
    if grid = nil then
      raise Exception.Create('For creating context menu items of API jobs for grids, related grid must be supplied!');

    if not Assigned(dmAPIInProg ) then
      dmAPIInProg := TdmAPIInProg.Create(Application);

    if not dmAPIInProg.mtAPIJobs.Active then
      dmAPIInProg.LoadAPIJobs;

    form := TForm(GetParentForm(grid));

    dmAPIInProg.mtAPIJobs1.Filter := 'FormClass = ' + QuotedStr(form.ClassName);
    if GroupId <> '' then
      dmAPIInProg.mtAPIJobs1.Filter := dmAPIInProg.mtAPIJobs1.Filter + ' AND GroupID = ' + QuotedStr(GroupID);

    dmAPIInProg.mtAPIJobs1.Filtered := True;
    if dmAPIInProg.mtAPIJobs1.RecordCount = 0 then
    begin
      if GroupId = '' then
        MessageDlg('No Rest API job is defined for the form!', TMsgDlgType.mtWarning, [TMsgDlgBtn.mbOK], 0)
      else
        MessageDlg(Format('No Rest API job is defined for the form with the ''%s'' Group ID!', [GroupId]), TMsgDlgType.mtWarning, [TMsgDlgBtn.mbOK], 0);
      Exit;
    end;

    NoPopupMenu := True;
    if TcxCustomGridAccess(grid).PopupMenu = nil then
    begin
      TcxCustomGridAccess(grid).PopupMenu := TPopupMenu.Create(form);
      TcxCustomGridAccess(grid).PopupMenu.Name := grid.Name + '_pumRestAPIJobs';
      TPopupMenu(TcxCustomGridAccess(grid).PopupMenu).Images := dmAPIInProg.ilAPIJobs;
    end
    else if TcxCustomGridAccess(grid).PopupMenu.Name <> grid.Name + '_pumRestAPIJobs' then
    begin
      NoPopupMenu := False;
      APISubmenu := TMenuItem(form.FindComponent(grid.Name + '_miRestAPIJobs'));
      if APISubmenu = nil then
      begin
        mi := TMenuItem.Create(form);
        mi.Caption := '-';
        TPopupMenu(TcxCustomGridAccess(grid).PopupMenu).Items.add(mi);
        mi := TMenuItem.Create(form);
        mi.Name := grid.Name + '_miRestAPIJobs';
        mi.Caption := 'Rest API Jobs';
        mi.SubMenuImages := dmAPIInProg.ilAPIJobs;
        TPopupMenu(TcxCustomGridAccess(grid).PopupMenu).Items.add(mi);
        APISubmenu := mi;
      end;
    end;

    dmAPIInProg.mtAPIJobs1.First;
    SavedGroupID := dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString;
    while not dmAPIInProg.mtAPIJobs1.Eof do
    begin
      //if not DictMenuItemAPIJobID.ContainsValue(dmAPIInProg.mtAPIJobs1.FieldByName('ID').AsGUID) then
      //begin
        if dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString <> SavedGroupID then
        begin
          mi := TMenuItem.Create(form);
          mi.Caption := '-';
          if NoPopupMenu then
            TPopupMenu(TcxCustomGridAccess(grid).PopupMenu).Items.add(mi)
          else
            APISubmenu.Add(mi);
          SavedGroupID := dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString;
        end;

        mi := TMenuItem.Create(form);
        mi.Caption := dmAPIInProg.mtAPIJobs1.FieldByName('Caption').AsString;
        mi.OnClick := dmAPIInProg.miAPIJobClick;
        DictMenuItemAPIJobID.AddOrSetValue(mi, dmAPIInProg.mtAPIJobs1.FieldByName('ID').AsGUID);

        if NoPopupMenu then
          TPopupMenu(TcxCustomGridAccess(grid).PopupMenu).Items.add(mi)
        else
          APISubmenu.Add(mi);
      //end;
      dmAPIInProg.mtAPIJobs1.Next;
    end;

    ApplicationCloseCheck(Form);
    FormResizeCheck(Form);
    PrepareRequestResultControls(Form, grdAPIStatus);

  except
    on e: Exception do
     MessageDlg(e.Message, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
  end;
end;

procedure CreatecxGridPopupMenuAPIJobContextMenuItem(gridpopupmenu: TcxGridPopupMenu; GroupID: string = ''; HitTypes: TcxGridViewHitTypes = [gvhtCell]);
var
  grdAPIStatus: TcxGrid;
  mi : TMenuItem;
  SavedGroupID: string;
  NoPopupMenu: Boolean;
  APISubmenu: TMenuItem;
  form: TForm;
  PopupMenuInfo: TcxPopupMenuInfo;
  i: integer;
begin
  try
    if gridpopupmenu = nil then
      raise Exception.Create('For creating context menu items of API jobs for cxgridpopupmenus, related cxgridpopupmenu must be supplied!');

    if gridpopupmenu.grid = nil then
      raise Exception.Create('For creating context menu items of API jobs for cxgridpopupmenus, related grid of specified cxgridpopupmenu must be supplied!');

    if not Assigned(dmAPIInProg ) then
      dmAPIInProg := TdmAPIInProg.Create(Application);

    if not dmAPIInProg.mtAPIJobs.Active then
      dmAPIInProg.LoadAPIJobs;

    form := TForm(GetParentForm(gridpopupmenu.grid));

    dmAPIInProg.mtAPIJobs1.Filter := 'FormClass = ' + QuotedStr(form.ClassName);
    if GroupId <> '' then
      dmAPIInProg.mtAPIJobs1.Filter := dmAPIInProg.mtAPIJobs1.Filter + ' AND GroupID = ' + QuotedStr(GroupID);

    dmAPIInProg.mtAPIJobs1.Filtered := True;
    if dmAPIInProg.mtAPIJobs1.RecordCount = 0 then
    begin
      if GroupId = '' then
        MessageDlg('No Rest API job is defined for the form!', TMsgDlgType.mtWarning, [TMsgDlgBtn.mbOK], 0)
      else
        MessageDlg(Format('No Rest API job is defined for the form with the ''%s'' Group ID!', [GroupId]), TMsgDlgType.mtWarning, [TMsgDlgBtn.mbOK], 0);
      Exit;
    end;

    for i := 0 to gridpopupmenu.PopupMenus.Count - 1 do
    begin
      PopupMenuInfo := gridpopupmenu.PopupMenus[i];
      if HitTypes = PopupMenuInfo.HitTypes then
      begin
        NoPopupMenu := True;
        if PopupMenuInfo.PopupMenu = nil then
        begin
          PopupMenuInfo.PopupMenu := TPopupMenu.Create(form);
          PopupMenuInfo.PopupMenu.Name := gridpopupmenu.Name + '_'+IntToStr(i)+'_pumRestAPIJobs';
          TPopupMenu(PopupMenuInfo.PopupMenu).Images := dmAPIInProg.ilAPIJobs;
        end
        else if PopupMenuInfo.PopupMenu.Name <> gridpopupmenu.Name + '_'+IntToStr(i)+'_pumRestAPIJobs' then
        begin
          NoPopupMenu := False;
          APISubmenu := TMenuItem(form.FindComponent(gridpopupmenu.Name + '_'+IntToStr(i)+'_miRestAPIJobs'));
          if APISubmenu = nil then
          begin
            mi := TMenuItem.Create(form);
            mi.Caption := '-';
            TPopupMenu(PopupMenuInfo.PopupMenu).Items.add(mi);
            mi := TMenuItem.Create(form);
            mi.Name := gridpopupmenu.Name + '_'+IntToStr(i) + '_miRestAPIJobs';
            mi.Caption := 'Rest API Jobs';
            mi.SubMenuImages := dmAPIInProg.ilAPIJobs;
            TPopupMenu(PopupMenuInfo.PopupMenu).Items.add(mi);
            APISubmenu := mi;
          end;
        end;

        dmAPIInProg.mtAPIJobs1.First;
        SavedGroupID := dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString;
        while not dmAPIInProg.mtAPIJobs1.Eof do
        begin
          //if not DictMenuItemAPIJobID.ContainsValue(dmAPIInProg.mtAPIJobs1.FieldByName('ID').AsGUID) then
          //begin
            if dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString <> SavedGroupID then
            begin
              mi := TMenuItem.Create(form);
              mi.Caption := '-';
              if NoPopupMenu then
                TPopupMenu(PopupMenuInfo.PopupMenu).Items.add(mi)
              else
                APISubmenu.Add(mi);
              SavedGroupID := dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString;
            end;

            mi := TMenuItem.Create(form);
            mi.Caption := dmAPIInProg.mtAPIJobs1.FieldByName('Caption').AsString;
            mi.OnClick := dmAPIInProg.miAPIJobClick;
            DictMenuItemAPIJobID.AddOrSetValue(mi, dmAPIInProg.mtAPIJobs1.FieldByName('ID').AsGUID);

            if NoPopupMenu then
              TPopupMenu(PopupMenuInfo.PopupMenu).Items.add(mi)
            else
              APISubmenu.Add(mi);
          //end;
          dmAPIInProg.mtAPIJobs1.Next;
        end;
      end;
    end;

    ApplicationCloseCheck(Form);
    FormResizeCheck(Form);
    PrepareRequestResultControls(Form, grdAPIStatus);

  except
    on e: Exception do
     MessageDlg(e.Message, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
  end;
end;

procedure TdmAPIInProg.APIJobUpdateGUI(form: TForm);
var
  button: TCustomButton;
  Grid: TComponent;
  menu: TPopupMenu;
  JobExecType: TButtonRestAPIJobExecType;
begin
  mtFormButtonJobAssignments.Filter := 'FormClass = ' + QuotedStr(form.ClassName);
  mtFormButtonJobAssignments.Filtered := True;
  mtFormButtonJobAssignments.First;
  while not mtFormButtonJobAssignments.Eof do
  begin
    button := TCustomButton(Form.FindComponent(mtFormButtonJobAssignmentsButtonName.AsString));
    if button = nil then
      MessageDlg(Format('Button named as %s couldn''t be found in the specified form for assigning job to button!',
                        [mtFormButtonJobAssignmentsButtonName.AsString]), mtError, [mbOK], 0)
    else
    begin
      if mtFormButtonJobAssignmentsJobExecutionType.AsString = 'Before OnClick' then
        JobExecType := brjeBeforeOriginalOnClickEvent
      else if mtFormButtonJobAssignmentsJobExecutionType.AsString = 'After OnClick' then
        JobExecType := brjeAfterOriginalOnClickEvent
      else if mtFormButtonJobAssignmentsJobExecutionType.AsString = 'Not Run OnClick' then
        JobExecType := brjeNotRunOriginalOnClickEvent
      else
      begin
        MessageDlg(Format('Button named as %s couldn''t be found in the specified form for assigning job to button!',
                          [mtFormButtonJobAssignmentsButtonName.AsString]), mtError, [mbOK], 0);
        mtFormButtonJobAssignments.Next;
        continue;
      end;
      UpdateButtonCodeAndGUI(button, mtFormButtonJobAssignmentsJobID.AsGuid, JobExecType,
                             mtFormButtonJobAssignmentsChangeCaption.AsBoolean,
                             mtFormButtonJobAssignmentsNewButtonCaption.AsString);
    end;
    mtFormButtonJobAssignments.Next;
  end;

  mtFormGridJobAssignments.Filter := 'FormClass = ' + QuotedStr(form.ClassName);
  mtFormGridJobAssignments.Filtered := True;
  mtFormGridJobAssignments.First;
  while not mtFormGridJobAssignments.Eof do
  begin
    Grid := Form.FindComponent(mtFormGridJobAssignmentsGridName.AsString);
    if Grid = nil then
      MessageDlg(Format('Grid named as %s couldn''t be found in the specified form for assigning job group(s) to grid!',
                        [mtFormGridJobAssignmentsGridName.AsString]), mtError, [mbOK], 0)
    else
    begin
      if Grid is TCustomGrid then
        CreateGridAPIJobContextMenuItem(TCustomGrid(Grid), mtFormGridJobAssignmentsJobGroupID.AsString)
      else if Grid is TcxCustomGrid then
        CreatecxGridAPIJobContextMenuItem(TcxCustomGrid(Grid), mtFormGridJobAssignmentsJobGroupID.AsString)
      else
        MessageDlg(Format('Grid named as %s is not standard rid or cxrid for assigning job group(s) to grid!',
                        [mtFormGridJobAssignmentsGridName.AsString]), mtError, [mbOK], 0)
    end;
    mtFormGridJobAssignments.Next;
  end;

  mtFormMenuJobAssignments.Filter := 'FormClass = ' + QuotedStr(form.ClassName);
  mtFormMenuJobAssignments.Filtered := True;
  mtFormMenuJobAssignments.First;
  while not mtFormMenuJobAssignments.Eof do
  begin
    menu := TPopupMenu(Form.FindComponent(mtFormMenuJobAssignmentsMenuName.AsString));
    if menu = nil then
      MessageDlg(Format('Menu named as %s couldn''t be found in the specified form for assigning job group(s) to menu!',
                        [mtFormMenuJobAssignmentsMenuName.AsString]), mtError, [mbOK], 0)
    else
      CreateAPIJobContextMenuItemsForMenu(form, menu, mtFormMenuJobAssignmentsJobGroupID.AsString);

    mtFormMenuJobAssignments.Next;
  end;
end;

procedure TdmAPIInProg.APIJobButtonDrowDownClick(Sender: TObject);
var
  Form: TForm;
  pnlAPIStatus: TSizeablePanel;
  grdAPIStatus: TcxGrid;
begin
  try
    TCustomButton(Sender).Enabled := False;
    Form := TForm(GetParentForm(TCustomButton(Sender)));
    pnlAPIStatus := TSizeablePanel(Form.FindComponent('pnlAPIStatus'));
    if pnlAPIStatus <> nil then
    begin
      if not pnlAPIStatus.Visible then
        RepositionResultPanel(Form, TCustomButton(Sender), pnlAPIStatus);
      pnlAPIStatus.Visible := not pnlAPIStatus.Visible;
      if pnlAPIStatus.Visible then
      begin
        grdAPIStatus := TcxGrid(Form.FindComponent('grdAPIStatus'));
        if grdAPIStatus <> nil then
          grdAPIStatus.SetFocus;
      end;
    end;
  finally
    TCustomButton(Sender).Enabled := True;
  end;
end;

procedure CreateAPIJobContextMenuItemsForMenu(form: TForm; menu: TPopupMenu; GroupID: string = '');
var
  grdAPIStatus: TcxGrid;
  mi : TMenuItem;
  SavedGroupID: string;
  APISubmenu: TMenuItem;
begin
  try
    if form = nil then
      raise Exception.Create('To create context menu items of API jobs for a menu, related form must be supplied!');

    if menu = nil then
      raise Exception.Create('To create context menu items of API jobs for a menu, a pop up menu must be supplied!');

    if not Assigned(dmAPIInProg ) then
      dmAPIInProg := TdmAPIInProg.Create(Application);

    if not dmAPIInProg.mtAPIJobs.Active then
      dmAPIInProg.LoadAPIJobs;

    dmAPIInProg.mtAPIJobs1.Filter := 'FormClass = ' + QuotedStr(form.ClassName);
    if GroupId <> '' then
      dmAPIInProg.mtAPIJobs1.Filter := dmAPIInProg.mtAPIJobs1.Filter + ' AND GroupID = ' + QuotedStr(GroupID);

    dmAPIInProg.mtAPIJobs1.Filtered := True;
    if dmAPIInProg.mtAPIJobs1.RecordCount = 0 then
    begin
      if GroupId = '' then
        MessageDlg('No Rest API job is defined for the form!', TMsgDlgType.mtWarning, [TMsgDlgBtn.mbOK], 0)
      else
        MessageDlg(Format('No Rest API job is defined for the form with the ''%s'' Group ID!', [GroupId]), TMsgDlgType.mtWarning, [TMsgDlgBtn.mbOK], 0);
      Exit;
    end;

    APISubmenu := TMenuItem(Application.FindComponent(menu.Name+'_miRestAPIJobs'));
    if APISubmenu = nil then
    begin
      mi := TMenuItem.Create(form);
      mi.Caption := '-';
      menu.Items.add(mi);
      mi := TMenuItem.Create(form);
      mi.Name := menu.Name+'_miRestAPIJobs';
      mi.Caption := 'Rest API Jobs';
      mi.SubMenuImages := dmAPIInProg.ilAPIJobs;
      menu.Items.add(mi);
      APISubmenu := mi;
    end;

    dmAPIInProg.mtAPIJobs1.First;
    SavedGroupID := dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString;
    while not dmAPIInProg.mtAPIJobs1.Eof do
    begin
      //if not DictMenuItemAPIJobID.ContainsValue(dmAPIInProg.mtAPIJobs1.FieldByName('ID').AsGUID) then
      //begin
        if dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString <> SavedGroupID then
        begin
          mi := TMenuItem.Create(form);
          mi.Caption := '-';
          APISubmenu.Add(mi);
          SavedGroupID := dmAPIInProg.mtAPIJobs1.FieldByName('GroupID').AsString;
        end;

        mi := TMenuItem.Create(form);
        mi.Caption := dmAPIInProg.mtAPIJobs1.FieldByName('Caption').AsString;
        mi.OnClick := dmAPIInProg.miAPIJobClick;
        DictMenuItemAPIJobID.AddOrSetValue(mi, dmAPIInProg.mtAPIJobs1.FieldByName('ID').AsGUID);

        APISubmenu.Add(mi);
      //end;
      dmAPIInProg.mtAPIJobs1.Next;
    end;

    ApplicationCloseCheck(Form);
    FormResizeCheck(Form);
    PrepareRequestResultControls(Form, grdAPIStatus);

  except
    on e: Exception do
     MessageDlg(e.Message, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
  end;
end;

{ TSizeablePanel }

procedure TSizeablePanel.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbLeft) and ((Width - x ) < 10) and
    ((Height - y ) < 10) then
  begin
    FDragging := TRue;
    FLastPos := Point(x, y);
    MouseCapture := true;
    Screen.cursor := crSizeNWSE;
  end
  else
    inherited;
end;

procedure TSizeablePanel.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  r: TRect;
begin
  if FDragging then
  begin
    r := BoundsRect;
    SetBounds( r.left, r.top, r.right - r.left + X - FlastPos.X,
    r.bottom - r.top + Y - Flastpos.Y );
    FLastPos := Point( x, y );
  end
  else
  begin
    inherited;
    if ((Width - x ) < 10) and ((Height - y ) < 10) then
      Cursor := crSizeNWSE
    else
      Cursor := crDefault;
  end;
end;

procedure TSizeablePanel.MouseUp(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if FDragging then
  begin
    FDragging := False;
    MouseCapture := false;
    Screen.Cursor := crDefault;
  end
  else
    inherited;
end;

procedure TSizeablePanel.Paint;
var
  x, y: Integer;
begin
  inherited;
  Canvas.Font.Name := 'Marlett';
  Canvas.Font.Size := 10;
  Canvas.Brush.Style := bsClear;
  x := clientwidth - canvas.textwidth('o');
  y := clientheight - canvas.textheight('o');
  canvas.textout( x, y, 'o' );
end;

initialization

  DictFormRestAPIDomain := TDictionary<TForm, TDictionary<string, TDictionary<string, TField>>>.Create;
  DictFormRestAPIConnection := TDictionary<TForm, TFDConnection>.Create;
  //DictButtonRestAPIJob := TDictionary<TCustomButton, TGUID>.Create;
  DictButtonRestAPIJob := TDictionary<TCustomButton, TButtonRestAPIJobRec>.Create;

  DictMenuItemAPIJobID := TDictionary<TMenuItem, TGUID>.Create;
  DictFormAPIJobThreadList := TDictionary<TForm, TRestAPIQueueeThread>.Create;
  DictFormCompletedRestAPIJob := TDictionary<TForm, TFormRestAPIJobRec>.Create;

  DictFormRestAPIFormResize := TDictionary<TForm, TNotifyEvent>.Create;

  Dict := CreateDictionaryStringString;

  DictResponseQue := TDictionary<TForm, TDictionary<TGUID, TDictionary<String, String>>>.Create;

  TMSLogger.Outputs := [loTimeStamp, loLogLevel, loValue];
  TMSLogger.OutputFormats.TimeStampFormat := SLogTimeStampFormat;
  TMSLogger.OutputFormats.LogLevelFormat := SLogLogLevelFormat;
  TMSLogger.OutputFormats.ValueFormat := SLogValueFormat;
  if not DirectoryExists(SLogDir) then
    MkDir(SLogDir);
  GTextHandler := TMSLogger.RegisterOutputHandlerClass(TTMSLoggerTextOutputHandler,
      [SLogDir + TPath.GetFileNameWithoutExtension(ParamStr(0)) + SLogExt]) as TTMSLoggerTextOutputHandler;

finalization
  if DictFormRestAPIDomain <> nil then
  begin
    for DictFormRestAPIDomainItem in DictFormRestAPIDomain do
    begin
      if DictFormRestAPIDomainItem.Value <> nil then
      begin
        for DictRestAPIDomainItem in DictFormRestAPIDomainItem.Value do
        begin
           if DictRestAPIDomainItem.Value <> nil then
             DictRestAPIDomainItem.Value.Free; // field dictionary is freed.
        end;
        DictFormRestAPIDomainItem.Value.free; // domain dictionary is freed
      end;
    end;
    DictFormRestAPIDomain.Free; // form dictionary is freed.
  end;
  if DictButtonRestAPIJob <> nil then
    DictButtonRestAPIJob.Free;
  if DictMenuItemAPIJobID <> nil then
    DictMenuItemAPIJobID.Free;

  if DictFormAPIJobThreadList <> nil then
    DictFormAPIJobThreadList.Free;

  if DictFormCompletedRestAPIJob <> nil then
    DictFormCompletedRestAPIJob.Free;

  if DictFormRestAPIConnection <> nil then
    DictFormRestAPIConnection.Free;

  if DictFormRestAPIFormResize <> nil then
  begin
    DictFormRestAPIFormResize.Clear;
    DictFormRestAPIFormResize.Free;
  end;

  if Dict <> nil then
    Dict := nil;

  if DictResponseQue <> nil then
    DictResponseQue := nil;

end.

