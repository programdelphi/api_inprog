unit uRequestTypes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, dxDateRanges, dxScrollbarAnnotations, cxDBData, cxGridLevel, dxLayoutContainer, cxGridInplaceEditForm,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, Vcl.StdCtrls, Vcl.ExtCtrls,
  Spring, Spring.Collections;

type
  TfrmRequestTypes = class(TForm)
    mtRequestTypes: TFDMemTable;
    dsRequestTypes: TDataSource;
    Panel1: TPanel;
    Button1: TButton;
    grdRequestTypes: TcxGrid;
    dbtvRequestTypes: TcxGridDBTableView;
    grdRequestTypesLevel1: TcxGridLevel;
    mtRequestTypesType: TStringField;
    dbtvRequestTypesType: TcxGridDBColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure Edit;
    class function List: IList<string>;
  end;

var
  frmRequestTypes: TfrmRequestTypes;

const
  REQUEST_TYPES_FILENAME = 'api_inprog.requesttypes.json';

implementation

{$R *.dfm}

uses
  System.JSON, System.IOUtils, uRecords;

class procedure TfrmRequestTypes.Edit;
var
  form: TfrmRequestTypes;
begin
  form := TfrmRequestTypes.Create(nil);
  try
    form.ShowModal;
  finally
    form.Free;
  end;
end;

procedure TfrmRequestTypes.FormClose(Sender: TObject; var Action: TCloseAction);
var
  B: TBookmark;
  U: ISet<string>;
begin
  Action := caFree;
  mtRequestTypes.DisableControls;
  B := mtRequestTypes.Bookmark;
  try
    mtRequestTypes.First;
    U := TCollections.CreateSet<string>;
    while not mtRequestTypes.Eof do
    begin
      if U.Contains(mtRequestTypesType.AsString) then
      begin
        Action := caNone;
        MessageDlg(Format('Type "%s" is not unique, please correct it.', [mtRequestTypesType.AsString]), mtError, [mbOK], 0);
        Exit;
      end;
      U.Add(mtRequestTypesType.AsString);
      mtRequestTypes.Next;
    end;
  finally
    mtRequestTypes.Bookmark := B;
    mtRequestTypes.EnableControls;
  end;
end;

procedure TfrmRequestTypes.FormCreate(Sender: TObject);
var
  js: TJSONObject;
begin
  if TFile.Exists(REQUEST_TYPES_FILENAME) then
    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(REQUEST_TYPES_FILENAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtRequestTypes);
      finally
        js.Free;
      end;
    except
      on e: Exception do
        MessageDlg('An error occured while loading "Request Types" dataset. Error: '+ e.Message,
                   TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
    end;
  mtRequestTypes.Active := True;
end;

procedure TfrmRequestTypes.FormDestroy(Sender: TObject);
var
  js: TJSONObject;
begin
  js := MT2JS(mtRequestTypes);
  try
    TFile.WriteAllText(REQUEST_TYPES_FILENAME, js.ToString);
  finally
    js.Free;
  end;
end;

class function TfrmRequestTypes.List: IList<string>;
var
  form: TfrmRequestTypes;
begin
  Result := TCollections.CreateList<string>;
  form := TfrmRequestTypes.Create(nil);
  try
    form.mtRequestTypes.First;
    while not form.mtRequestTypes.Eof do
    begin
      Result.Add(form.mtRequestTypesType.AsString);
      form.mtRequestTypes.Next;
    end;
  finally
    form.Free;
  end;
end;

end.
