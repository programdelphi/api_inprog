object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 436
  ClientWidth = 728
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 395
    Width = 728
    Height = 41
    Align = alBottom
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    object btnFrm2: TButton
      AlignWithMargins = True
      Left = 7
      Top = 4
      Width = 88
      Height = 33
      Margins.Left = 6
      Align = alLeft
      Caption = 'Show Form2'
      TabOrder = 0
      OnClick = btnFrm2Click
    end
    object btnSetup: TButton
      AlignWithMargins = True
      Left = 104
      Top = 4
      Width = 80
      Height = 33
      Margins.Left = 6
      Align = alLeft
      Caption = 'API Setup'
      TabOrder = 1
      OnClick = btnSetupClick
    end
    object btnDemoAPI1: TButton
      AlignWithMargins = True
      Left = 624
      Top = 4
      Width = 97
      Height = 33
      Margins.Right = 6
      Align = alRight
      Caption = 'Demo API 1'
      TabOrder = 2
      OnClick = btnDemoAPI1Click
    end
    object btnDemoAPI2: TButton
      AlignWithMargins = True
      Left = 518
      Top = 4
      Width = 97
      Height = 33
      Margins.Right = 6
      Align = alRight
      Caption = 'Demo API 2'
      TabOrder = 3
      OnClick = btnDemoAPI2Click
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 257
    Height = 395
    Align = alLeft
    DataSource = DataSource1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBGrid2: TDBGrid
    Left = 257
    Top = 0
    Width = 296
    Height = 395
    Align = alLeft
    DataSource = DataSource2
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object cxGrid1: TcxGrid
    Left = 553
    Top = 0
    Width = 250
    Height = 395
    Align = alLeft
    PopupMenu = PopupMenu1
    TabOrder = 3
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object FDMemTable1: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 128
    Top = 32
  end
  object DataSource1: TDataSource
    DataSet = FDMemTable1
    Left = 40
    Top = 32
  end
  object FDMemTable2: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 336
    Top = 56
  end
  object DataSource2: TDataSource
    DataSet = FDMemTable2
    Left = 496
    Top = 48
  end
  object FDConnection1: TFDConnection
    Left = 608
    Top = 88
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <
      item
        HitTypes = [gvhtCell]
        Index = 0
      end>
    Left = 360
    Top = 224
  end
  object PopupMenu1: TPopupMenu
    Left = 160
    Top = 248
    object SampleMenuItem1: TMenuItem
      Caption = 'Sample Menu Item '
    end
  end
end
