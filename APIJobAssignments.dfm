object frmAPIJobAssignments: TfrmAPIJobAssignments
  Left = 0
  Top = 0
  Caption = 'UI API Job Assignments'
  ClientHeight = 689
  ClientWidth = 966
  Color = clWindow
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object gbButtonAPIJobAssignments: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    Caption = 'Button API Job Assignmentts'
    TabOrder = 0
    Height = 292
    Width = 966
    object gbResponseSettings: TcxGroupBox
      Left = 515
      Top = 18
      Align = alRight
      Caption = 'Response Grid Settings'
      TabOrder = 0
      Height = 272
      Width = 449
      object gbResponseFields: TcxGroupBox
        Left = 2
        Top = 67
        Align = alClient
        Caption = 'Response Fields'
        TabOrder = 0
        Height = 203
        Width = 445
        object grdResponseFields: TcxGrid
          AlignWithMargins = True
          Left = 5
          Top = 19
          Width = 435
          Height = 142
          Margins.Top = 1
          Align = alClient
          TabOrder = 0
          object grdResponseFieldsDBTV: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.Images = cxImageList1
            Navigator.Buttons.Insert.Visible = False
            Navigator.Buttons.Delete.Visible = False
            Navigator.Visible = True
            ScrollbarAnnotations.CustomAnnotations = <>
            DataController.DataSource = dsButtonJobAssignmentResponseFields
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Images = cxImageList1
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 40
            object grdResponseFieldsDBTVResponseFieldName: TcxGridDBColumn
              DataBinding.FieldName = 'ResponseFieldName'
              Options.Editing = False
              Width = 188
            end
            object grdResponseFieldsDBTVOrderNo: TcxGridDBColumn
              DataBinding.FieldName = 'OrderNo'
              Width = 67
            end
            object grdResponseFieldsDBTVShowInResponseGrid: TcxGridDBColumn
              DataBinding.FieldName = 'ShowInResponseGrid'
              Width = 81
            end
            object grdResponseFieldsDBTVResponseGridColumnWidth: TcxGridDBColumn
              DataBinding.FieldName = 'ResponseGridColumnWidth'
              Width = 91
            end
          end
          object grdResponseFieldsL: TcxGridLevel
            GridView = grdResponseFieldsDBTV
          end
        end
        object gbButtonsOfButtonJobAssignmentResponseFields: TcxGroupBox
          Left = 2
          Top = 164
          Align = alBottom
          PanelStyle.Active = True
          TabOrder = 1
          Height = 37
          Width = 441
          object btnAddAllResponseFieldsForButtonJobAssignment: TcxButton
            Left = 6
            Top = 6
            Width = 125
            Height = 25
            Caption = 'Add All Response Fields'
            TabOrder = 0
            OnClick = btnAddAllResponseFieldsForButtonJobAssignmentClick
          end
          object btnDeleteAllResponseFieldsOfButtonJobAssignment: TcxButton
            Left = 137
            Top = 6
            Width = 178
            Height = 25
            Caption = 'Delete All Response Fields Records'
            TabOrder = 1
            OnClick = btnDeleteAllResponseFieldsOfButtonJobAssignmentClick
          end
        end
      end
      object gbResponseGridSize: TcxGroupBox
        Left = 2
        Top = 18
        Align = alTop
        PanelStyle.Active = True
        Style.BorderStyle = ebsNone
        Style.Edges = [bLeft, bTop, bRight, bBottom]
        TabOrder = 1
        Height = 49
        Width = 445
        object Label1: TLabel
          Left = 18
          Top = 25
          Width = 120
          Height = 13
          Caption = 'Width of Response Grid :'
          FocusControl = cxDBSpinEdit1
        end
        object Label2: TLabel
          Left = 234
          Top = 25
          Width = 123
          Height = 13
          Caption = 'Height of Response Grid :'
          FocusControl = cxDBSpinEdit2
        end
        object cxDBSpinEdit1: TcxDBSpinEdit
          Left = 143
          Top = 22
          DataBinding.DataField = 'ResponseGridWidth'
          DataBinding.DataSource = dsFormButtonJobAssignments
          TabOrder = 1
          Width = 51
        end
        object cxDBSpinEdit2: TcxDBSpinEdit
          Left = 362
          Top = 22
          DataBinding.DataField = 'ResponseGridHeight'
          DataBinding.DataSource = dsFormButtonJobAssignments
          TabOrder = 2
          Width = 51
        end
        object dcbCustomizeResponseGrid: TcxDBCheckBox
          Left = 10
          Top = 1
          BiDiMode = bdRightToLeft
          Caption = ': Customize Response Grid'
          DataBinding.DataField = 'CustomizeResponseGrid'
          DataBinding.DataSource = dsFormButtonJobAssignments
          ParentBiDiMode = False
          Properties.ClearKey = 46
          Properties.ImmediatePost = True
          Style.TransparentBorder = False
          TabOrder = 0
        end
      end
    end
    object pnlButtonAssignments: TPanel
      Left = 2
      Top = 18
      Width = 513
      Height = 272
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object cxGroupBox1: TcxGroupBox
        Left = 0
        Top = 235
        Align = alBottom
        PanelStyle.Active = True
        TabOrder = 0
        Height = 37
        Width = 513
        object btnNotificationSettings: TcxButton
          Left = 8
          Top = 6
          Width = 162
          Height = 25
          Caption = 'Notification Message Settings'
          TabOrder = 0
          OnClick = btnNotificationSettingsClick
        end
      end
      object grdButtonAPIJobAssinments: TcxGrid
        Left = 0
        Top = 0
        Width = 513
        Height = 235
        Align = alClient
        TabOrder = 1
        ExplicitLeft = 1
        object grdButtonAPIJobAssinmentsDBTV: TcxGridDBTableView
          Navigator.Buttons.OnButtonClick = grdButtonAPIJobAssinmentsDBTVNavigatorButtonsButtonClick
          Navigator.Buttons.CustomButtons = <
            item
              Hint = 'Add all "Response Fields"'
              ImageIndex = 0
            end
            item
              Hint = 'Delete all records of "Response Fields"'
              ImageIndex = 1
            end>
          Navigator.Buttons.Images = cxImageList1
          Navigator.Visible = True
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = dsFormButtonJobAssignments
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.FocusFirstCellOnNewRecord = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.NavigatorHints = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsView.GroupByBox = False
          object grdButtonAPIJobAssinmentsDBTVFormClass: TcxGridDBColumn
            DataBinding.FieldName = 'FormClass'
            Visible = False
            Options.Editing = False
          end
          object grdButtonAPIJobAssinmentsDBTVJobID: TcxGridDBColumn
            DataBinding.FieldName = 'JobID'
            Visible = False
            Options.Editing = False
          end
          object grdButtonAPIJobAssinmentsDBTVLCapitonWithName: TcxGridDBColumn
            Caption = 'Button'
            DataBinding.FieldName = 'LButtonCaptionWithName'
            Width = 153
          end
          object grdButtonAPIJobAssinmentsDBTVLJobCaption: TcxGridDBColumn
            DataBinding.FieldName = 'LJobCaption'
            Width = 142
          end
          object grdButtonAPIJobAssinmentsDBTVJobExecutionType: TcxGridDBColumn
            DataBinding.FieldName = 'JobExecutionType'
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.Items.Strings = (
              'Before OnClick'
              'After OnClick'
              'Not Run OnClick')
            Width = 106
          end
          object grdButtonAPIJobAssinmentsDBTVChangeCaption: TcxGridDBColumn
            DataBinding.FieldName = 'ChangeCaption'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ImmediatePost = True
            Width = 89
          end
          object grdButtonAPIJobAssinmentsDBTVNewButtonCaption: TcxGridDBColumn
            DataBinding.FieldName = 'NewButtonCaption'
            Width = 142
          end
          object grdButtonAPIJobAssinmentsDBTVSendsWindowsNotification: TcxGridDBColumn
            DataBinding.FieldName = 'SendsWindowsNotification'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Width = 141
          end
          object grdButtonAPIJobAssinmentsDBTVCustomizeNotification: TcxGridDBColumn
            DataBinding.FieldName = 'CustomizeNotification'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Width = 117
          end
          object grdButtonAPIJobAssinmentsDBTVSuccessNotificationTitle: TcxGridDBColumn
            DataBinding.FieldName = 'SuccessNotificationTitle'
            Width = 193
          end
          object grdButtonAPIJobAssinmentsDBTVSuccessNotificationHeader: TcxGridDBColumn
            DataBinding.FieldName = 'SuccessNotificationHeader'
            Width = 281
          end
          object grdButtonAPIJobAssinmentsDBTVSuccessNotificationMessage: TcxGridDBColumn
            DataBinding.FieldName = 'SuccessNotificationMessage'
            SortIndex = 0
            SortOrder = soAscending
            Width = 300
          end
          object grdButtonAPIJobAssinmentsDBTVErrorNotificationTitle: TcxGridDBColumn
            DataBinding.FieldName = 'ErrorNotificationTitle'
            Width = 197
          end
          object grdButtonAPIJobAssinmentsDBTVErrorNotificationHeader: TcxGridDBColumn
            DataBinding.FieldName = 'ErrorNotificationHeader'
            Width = 227
          end
          object grdButtonAPIJobAssinmentsDBTVErrorNotificationMessage: TcxGridDBColumn
            DataBinding.FieldName = 'ErrorNotificationMessage'
            Width = 300
          end
          object grdButtonAPIJobAssinmentsDBTVCustomizeResponseGrid: TcxGridDBColumn
            DataBinding.FieldName = 'CustomizeResponseGrid'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Width = 135
          end
          object grdButtonAPIJobAssinmentsDBTVResponseGridWidth: TcxGridDBColumn
            DataBinding.FieldName = 'ResponseGridWidth'
            Width = 122
          end
          object grdButtonAPIJobAssinmentsDBTVResponseGridHeight: TcxGridDBColumn
            DataBinding.FieldName = 'ResponseGridHeight'
            Width = 122
          end
        end
        object grdButtonAPIJobAssinmentsL: TcxGridLevel
          Caption = 'Button Rule Assignments'
          GridView = grdButtonAPIJobAssinmentsDBTV
        end
      end
    end
  end
  object gbGridAPIJobAssignmentts: TcxGroupBox
    Left = 0
    Top = 292
    Align = alBottom
    Caption = 'Grid API Job Assignmentts'
    TabOrder = 1
    Height = 180
    Width = 966
    object grdGridAPIJobAssinments: TcxGrid
      Left = 2
      Top = 18
      Width = 962
      Height = 160
      Align = alClient
      TabOrder = 0
      object grdGridAPIJobAssinmentsDBTV: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Visible = True
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dsFormGridJobAssignments
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsBehavior.FocusCellOnCycle = True
        OptionsView.GroupByBox = False
        object grdGridAPIJobAssinmentsDBTVFormClass: TcxGridDBColumn
          DataBinding.FieldName = 'FormClass'
          Visible = False
          Options.Editing = False
        end
        object grdGridAPIJobAssinmentsDBTVGridName: TcxGridDBColumn
          Caption = 'Grid Name'
          DataBinding.FieldName = 'GridName'
          Visible = False
          Options.Editing = False
        end
        object grdGridAPIJobAssinmentsDBTVLGridName: TcxGridDBColumn
          DataBinding.FieldName = 'LGridName'
          Width = 400
        end
        object grdGridAPIJobAssinmentsDBTVAllJobs: TcxGridDBColumn
          DataBinding.FieldName = 'AllJobs'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Width = 66
        end
        object grdGridAPIJobAssinmentsDBTVJobGroupID: TcxGridDBColumn
          DataBinding.FieldName = 'JobGroupID'
          Visible = False
          Options.Editing = False
          Width = 300
        end
        object grdGridAPIJobAssinmentsDBTVlJobGroupID: TcxGridDBColumn
          DataBinding.FieldName = 'lJobGroupID'
          Width = 475
        end
      end
      object grdGridAPIJobAssinmentsL: TcxGridLevel
        Caption = 'Button Rule Assignments'
        GridView = grdGridAPIJobAssinmentsDBTV
      end
    end
  end
  object gbButtons: TcxGroupBox
    Left = 0
    Top = 652
    Align = alBottom
    PanelStyle.Active = True
    TabOrder = 2
    Height = 37
    Width = 966
    object btnReload: TcxButton
      Left = 11
      Top = 6
      Width = 118
      Height = 25
      Caption = '&Reload Assigments'
      TabOrder = 0
      OnClick = btnReloadClick
    end
    object btnSave: TcxButton
      Left = 151
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Save'
      Enabled = False
      TabOrder = 1
      OnClick = btnSaveClick
    end
  end
  object gbMenuAPIJobAssignmentts: TcxGroupBox
    Left = 0
    Top = 472
    Align = alBottom
    Caption = 'Menu API Job Assignmentts'
    TabOrder = 3
    Height = 180
    Width = 966
    object grdMenuAPIJobAssinments: TcxGrid
      Left = 2
      Top = 18
      Width = 962
      Height = 160
      Align = alClient
      TabOrder = 0
      object grdMenuAPIJobAssinmentsDBTV: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Visible = True
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dsFormMenuJobAssignments
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.GroupByBox = False
        object grdMenuAPIJobAssinmentsDBTVFormClass: TcxGridDBColumn
          DataBinding.FieldName = 'FormClass'
          Visible = False
          Options.Editing = False
        end
        object grdMenuAPIJobAssinmentsDBTVMenuName: TcxGridDBColumn
          DataBinding.FieldName = 'MenuName'
          Visible = False
          Options.Editing = False
        end
        object grdMenuAPIJobAssinmentsDBTVLMenuName: TcxGridDBColumn
          DataBinding.FieldName = 'LMenuName'
          Width = 400
        end
        object grdMenuAPIJobAssinmentsDBTVAllJobs: TcxGridDBColumn
          DataBinding.FieldName = 'AllJobs'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Width = 66
        end
        object grdMenuAPIJobAssinmentsDBTVJobGroupID: TcxGridDBColumn
          DataBinding.FieldName = 'JobGroupID'
          Visible = False
          Options.Editing = False
          Width = 300
        end
        object grdMenuAPIJobAssinmentsDBTVlJobGroupID: TcxGridDBColumn
          DataBinding.FieldName = 'lJobGroupID'
          Width = 475
        end
      end
      object grdMenuAPIJobAssinmentsL: TcxGridLevel
        Caption = 'Button Rule Assignments'
        GridView = grdMenuAPIJobAssinmentsDBTV
      end
    end
  end
  object mtFormButtonJobAssignments: TFDMemTable
    AfterInsert = mtFormButtonJobAssignmentsAfterInsert
    AfterEdit = mtFormButtonJobAssignmentsAfterEdit
    BeforePost = mtFormButtonJobAssignmentsBeforePost
    AfterPost = mtFormButtonJobAssignmentsAfterPost
    AfterCancel = mtFormButtonJobAssignmentsAfterCancel
    BeforeDelete = mtFormButtonJobAssignmentsBeforeDelete
    AfterDelete = mtFormButtonJobAssignmentsAfterDelete
    OnNewRecord = mtFormButtonJobAssignmentsNewRecord
    CachedUpdates = True
    Indexes = <
      item
        Active = True
        Fields = 'FormClass;ButtonName'
        Options = [soNoCase, soUnique]
      end>
    ConstraintsEnabled = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 280
    Top = 81
    object mtFormButtonJobAssignmentsFormClass: TStringField
      DisplayLabel = 'Form Class'
      FieldName = 'FormClass'
      Size = 100
    end
    object mtFormButtonJobAssignmentsJobID: TGuidField
      DisplayLabel = 'Job ID'
      FieldName = 'JobID'
      OnValidate = mtFormButtonJobAssignmentsJobIDValidate
      Size = 38
    end
    object mtFormButtonJobAssignmentsLJobCaption: TStringField
      DisplayLabel = 'Job Caption'
      FieldKind = fkLookup
      FieldName = 'LJobCaption'
      LookupDataSet = frmAPIInProgSettings.mtAPIJobs
      LookupKeyFields = 'ID'
      LookupResultField = 'Caption'
      KeyFields = 'JobID'
      Size = 100
      Lookup = True
    end
    object mtFormButtonJobAssignmentsButtonName: TStringField
      DisplayLabel = 'Button Name'
      FieldName = 'ButtonName'
      Size = 100
    end
    object mtFormButtonJobAssignmentsLButtonName: TStringField
      DisplayLabel = 'Button Caption'
      FieldKind = fkLookup
      FieldName = 'LButtonCaptionWithName'
      LookupDataSet = mtFormButtons
      LookupKeyFields = 'Name'
      LookupResultField = 'CCaptionWithName'
      KeyFields = 'ButtonName'
      Size = 100
      Lookup = True
    end
    object mtFormButtonJobAssignmentsJobExecutionType: TStringField
      DisplayLabel = 'Job Execution Type'
      FieldName = 'JobExecutionType'
      Size = 100
    end
    object mtFormButtonJobAssignmentsChangeCaption: TBooleanField
      DisplayLabel = 'Change Caption'
      FieldName = 'ChangeCaption'
      OnValidate = mtFormButtonJobAssignmentsChangeCaptionValidate
    end
    object mtFormButtonJobAssignmentsNewButtonCaption: TStringField
      DisplayLabel = 'New Button Caption'
      FieldName = 'NewButtonCaption'
      Size = 100
    end
    object mtFormButtonJobAssignmentsSendsWindowsNotification: TBooleanField
      DefaultExpression = 'false'
      DisplayLabel = 'Sends Windows Notification'
      FieldName = 'SendsWindowsNotification'
    end
    object mtFormButtonJobAssignmentsCustomizeNotification: TBooleanField
      DefaultExpression = 'false'
      DisplayLabel = 'Customize Notification'
      FieldName = 'CustomizeNotification'
      OnValidate = mtFormButtonJobAssignmentsCustomizeNotificationValidate
    end
    object mtFormButtonJobAssignmentsSuccessNotificationTitle: TStringField
      DisplayLabel = 'Success Notification Title'
      FieldName = 'SuccessNotificationTitle'
      Size = 100
    end
    object mtFormButtonJobAssignmentsSuccessNotificationHeader: TStringField
      DisplayLabel = 'Success Notification Header'
      FieldName = 'SuccessNotificationHeader'
      Size = 200
    end
    object mtFormButtonJobAssignmentsSuccessNotificationMessage: TStringField
      DisplayLabel = 'Success Notification Message'
      FieldName = 'SuccessNotificationMessage'
      Size = 300
    end
    object mtFormButtonJobAssignmentsErrorNotificationTitle: TStringField
      DisplayLabel = 'Error Notification Title'
      FieldName = 'ErrorNotificationTitle'
      Size = 100
    end
    object mtFormButtonJobAssignmentsErrorNotificationHeader: TStringField
      DisplayLabel = 'Error Notification Header'
      FieldName = 'ErrorNotificationHeader'
      Size = 200
    end
    object mtFormButtonJobAssignmentsErrorNotificationMessage: TStringField
      DisplayLabel = 'Error Notification Message'
      FieldName = 'ErrorNotificationMessage'
      Size = 300
    end
    object mtFormButtonJobAssignmentsCustomizeResponseGrid: TBooleanField
      DefaultExpression = 'false'
      DisplayLabel = 'Customize Response Grid'
      FieldName = 'CustomizeResponseGrid'
      OnValidate = mtFormButtonJobAssignmentsCustomizeResponseGridValidate
    end
    object mtFormButtonJobAssignmentsResponseGridWidth: TIntegerField
      DisplayLabel = 'Width of Response Grid '
      FieldName = 'ResponseGridWidth'
    end
    object mtFormButtonJobAssignmentsResponseGridHeight: TIntegerField
      DisplayLabel = 'Height of Response Grid'
      FieldName = 'ResponseGridHeight'
    end
  end
  object dsFormButtonJobAssignments: TDataSource
    DataSet = mtFormButtonJobAssignments
    Left = 480
    Top = 80
  end
  object mtFormButtons: TFDMemTable
    OnCalcFields = mtFormButtonsCalcFields
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 104
    Top = 73
    object mtFormButtonsName: TStringField
      DisplayLabel = 'Button Name'
      FieldName = 'Name'
      Size = 100
    end
    object mtFormButtonsCaption: TStringField
      FieldName = 'Caption'
      Size = 200
    end
    object mtFormButtonsCCaptionWithName: TStringField
      DisplayLabel = 'Caption With Name'
      FieldKind = fkCalculated
      FieldName = 'CCaptionWithName'
      Size = 100
      Calculated = True
    end
  end
  object mtFormGrids: TFDMemTable
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 80
    Top = 177
    object mtFormGridsName: TStringField
      DisplayLabel = 'Button Name'
      FieldName = 'Name'
      Size = 100
    end
  end
  object mtFormGridJobAssignments: TFDMemTable
    BeforePost = mtFormGridJobAssignmentsBeforePost
    AfterPost = mtFormGridJobAssignmentsAfterPost
    AfterCancel = mtFormGridJobAssignmentsAfterCancel
    AfterDelete = mtFormGridJobAssignmentsAfterDelete
    OnNewRecord = mtFormGridJobAssignmentsNewRecord
    CachedUpdates = True
    Indexes = <
      item
        Active = True
        Fields = 'FormClass;GridName'
        Options = [soNoCase, soUnique]
      end>
    ConstraintsEnabled = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 296
    Top = 409
    object mtFormGridJobAssignmentsFormClass: TStringField
      DisplayLabel = 'Form Class'
      FieldName = 'FormClass'
      Size = 100
    end
    object mtFormGridJobAssignmentsGridName: TStringField
      DisplayLabel = 'Menu Name'
      FieldName = 'GridName'
      Size = 100
    end
    object mtFormGridJobAssignmentsLGridName: TStringField
      DisplayLabel = 'Grid Name'
      FieldKind = fkLookup
      FieldName = 'LGridName'
      LookupDataSet = mtFormGrids
      LookupKeyFields = 'Name'
      LookupResultField = 'Name'
      KeyFields = 'GridName'
      Size = 200
      Lookup = True
    end
    object mtFormGridJobAssignmentsJobGroupID: TStringField
      DisplayLabel = 'Job Group ID'
      FieldName = 'JobGroupID'
      OnValidate = mtFormGridJobAssignmentsJobGroupIDValidate
      Size = 200
    end
    object mtFormGridJobAssignmentslJobGroupID: TStringField
      DisplayLabel = 'Job Group ID'
      FieldKind = fkLookup
      FieldName = 'lJobGroupID'
      LookupDataSet = mtJobGroups
      LookupKeyFields = 'JobGroupID'
      LookupResultField = 'JobGroupID'
      KeyFields = 'JobGroupID'
      Size = 200
      Lookup = True
    end
    object mtFormGridJobAssignmentsAllJobs: TBooleanField
      DisplayLabel = 'All Jobs'
      FieldName = 'AllJobs'
      OnValidate = mtFormGridJobAssignmentsAllJobsValidate
    end
  end
  object dsFormGridJobAssignments: TDataSource
    DataSet = mtFormGridJobAssignments
    Left = 416
    Top = 408
  end
  object mtJobGroups: TFDMemTable
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 112
    Top = 409
    object mtJobGroupsJobGroupID: TStringField
      DisplayLabel = 'Job Group ID'
      FieldName = 'JobGroupID'
      Size = 200
    end
  end
  object mtFormMenus: TFDMemTable
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 168
    Top = 513
    object mtFormMenusName: TStringField
      DisplayLabel = 'Button Name'
      FieldName = 'Name'
      Size = 100
    end
  end
  object mtFormMenuJobAssignments: TFDMemTable
    AfterInsert = mtFormMenuJobAssignmentsAfterInsert
    AfterEdit = mtFormMenuJobAssignmentsAfterEdit
    BeforePost = mtFormMenuJobAssignmentsBeforePost
    AfterPost = mtFormMenuJobAssignmentsAfterPost
    AfterCancel = mtFormMenuJobAssignmentsAfterCancel
    AfterDelete = mtFormMenuJobAssignmentsAfterDelete
    OnNewRecord = mtFormMenuJobAssignmentsNewRecord
    CachedUpdates = True
    Indexes = <
      item
        Active = True
        Fields = 'FormClass;MenuName'
        Options = [soNoCase, soUnique]
      end>
    ConstraintsEnabled = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 352
    Top = 497
    object mtFormMenuJobAssignmentsFormClass: TStringField
      DisplayLabel = 'Form Class'
      FieldName = 'FormClass'
      Size = 100
    end
    object mtFormMenuJobAssignmentsMenuName: TStringField
      DisplayLabel = 'Menu Name'
      FieldName = 'MenuName'
      Size = 100
    end
    object mtFormMenuJobAssignmentsLMenuName: TStringField
      DisplayLabel = 'Menu Name'
      FieldKind = fkLookup
      FieldName = 'LMenuName'
      LookupDataSet = mtFormMenus
      LookupKeyFields = 'Name'
      LookupResultField = 'Name'
      KeyFields = 'MenuName'
      Size = 200
      Lookup = True
    end
    object mtFormMenuJobAssignmentsJobGroupID: TStringField
      DisplayLabel = 'Job Group ID'
      FieldName = 'JobGroupID'
      OnValidate = mtFormMenuJobAssignmentsJobGroupIDValidate
      Size = 200
    end
    object mtFormMenuJobAssignmentslJobGroupID: TStringField
      DisplayLabel = 'Job Group ID'
      FieldKind = fkLookup
      FieldName = 'lJobGroupID'
      LookupDataSet = mtJobGroups
      LookupKeyFields = 'JobGroupID'
      LookupResultField = 'JobGroupID'
      KeyFields = 'JobGroupID'
      Size = 200
      Lookup = True
    end
    object mtFormMenuJobAssignmentsAllJobs: TBooleanField
      DisplayLabel = 'All Jobs'
      FieldName = 'AllJobs'
      OnValidate = mtFormMenuJobAssignmentsAllJobsValidate
    end
  end
  object dsFormMenuJobAssignments: TDataSource
    DataSet = mtFormMenuJobAssignments
    Left = 488
    Top = 496
  end
  object mtButtonJobAssignmentResponseFields: TFDMemTable
    AfterInsert = mtButtonJobAssignmentResponseFieldsAfterInsert
    AfterEdit = mtButtonJobAssignmentResponseFieldsAfterEdit
    AfterPost = mtButtonJobAssignmentResponseFieldsAfterPost
    AfterCancel = mtButtonJobAssignmentResponseFieldsAfterCancel
    AfterDelete = mtButtonJobAssignmentResponseFieldsAfterDelete
    CachedUpdates = True
    IndexFieldNames = 'JobID;ButtonName;OrderNo'
    Aggregates = <
      item
        Name = 'MaxOrderNoDefault'
        Expression = 'Max(OrderNo)'
        GroupingLevel = 2
        Active = True
      end>
    AggregatesActive = True
    MasterSource = dsFormButtonJobAssignments
    MasterFields = 'JobID;ButtonName'
    DetailFields = 'JobID;ButtonName'
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 808
    Top = 128
    object mtButtonJobAssignmentResponseFieldsJobID: TGuidField
      DisplayLabel = 'Job ID'
      FieldName = 'JobID'
      Size = 38
    end
    object mtButtonJobAssignmentResponseFieldsButtonName: TStringField
      DisplayLabel = 'Button Name'
      FieldName = 'ButtonName'
      Size = 100
    end
    object mtButtonJobAssignmentResponseFieldsResponseFieldName: TStringField
      DisplayLabel = 'Response Field Name'
      FieldName = 'ResponseFieldName'
      Size = 100
    end
    object mtButtonJobAssignmentResponseFieldsShowInResponseGrid: TBooleanField
      DisplayLabel = 'Show In Response Grid'
      FieldName = 'ShowInResponseGrid'
    end
    object mtButtonJobAssignmentResponseFieldsOrderNo: TSmallintField
      DisplayLabel = 'Order No'
      FieldName = 'OrderNo'
    end
    object mtButtonJobAssignmentResponseFieldsResponseGridColumnWidth: TIntegerField
      DisplayLabel = 'Response Grid Column Width'
      FieldName = 'ResponseGridColumnWidth'
    end
  end
  object dsButtonJobAssignmentResponseFields: TDataSource
    DataSet = mtButtonJobAssignmentResponseFields
    Left = 736
    Top = 120
  end
  object cxImageList1: TcxImageList
    SourceDPI = 96
    FormatVersion = 1
    DesignInfo = 7340592
    ImageInfo = <
      item
        ImageClass = 'TdxPNGImage'
        Image.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          6100000024744558745469746C6500526570656174416C6C4974656D4C616265
          6C735069766F745461626C653B4976752E0000014849444154785E8593CB4AC3
          501086DB9064E3A2F82CBE9408A2882045AA14114514940A826EBA10152A8AA8
          88E0C68DE0E545A452AB6D9A7BC699211325CEA1810F06FE2F3F93704E05002A
          F8545BA72F07FB676F300EF2C8A7F70829B0F68E9F21CB00923433C13979E497
          0BECEDF613C4490653D3374C10A5D0EEBC1232734E1EF9E50267E3F0118514FC
          3031C13979E4970BDC66EB01A2382D36F0824436909973F2C8FF57B0B27B0F21
          0A433F36C1397A7AC1F2CE1D0451526CF0ED45B281CC9CA3A717D4B76EF93BFB
          C3D004E7E8E9054B9BD7300A7F37E80D42D94066CED1D30B16D7AFC00B62F8F8
          0A4C708E9E5EB0B076099E1F171B74FBBE6C2033E7E8E905F3CD0BFED3EF9FBE
          09CED1530B9CB9D5733EAE8351648273F4D48364CFD44F8E661B1D180779DA51
          B6101799406AC8A4422DCF5DED3255F3121B71485270F2DCFA7B9D7F006D1797
          5CAE46A7290000000049454E44AE426082}
      end
      item
        ImageClass = 'TdxPNGImage'
        Image.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001B744558745469746C6500436C656172416C6C3B436C6561723B4572
          6173653B90F649EC0000027C49444154785EA5916D48535118C76F3ADDCC4656
          7E308922CB4C9170A691961A4C918DA052D05E2C5748362DCD8C9A9A56BE52A8
          94647EC9966D830A254CD7B4146A443170E6CB6C335F6A628911A4CE5B98B57F
          E75EBCB2B420E8811FCFE11C7EFF07CE43FD632D2138137804A74582FEF81EEA
          7DD949EA5D493A355C92460D15CBA9C1223935507882935D8C0A59656FDE3163
          B33C7E171364CA394AF52A64145B6DC9520AC04238D9D5909D74DB6E1B0008E6
          E2745A9324896642D977A6AA0B2AFE26F35F64242A7F4E5840EB6A09B7609FEC
          474F69269D1FEC17CB84B301D99A377F9205EDA971777E7C36817E54836902DD
          5483D9D10EBC4D96E28178EBD7AC8075522E60A1ECA693EDAE9B1D7B0D5B4315
          A61BAE13AAF07DF825068E4830785802F3C158DC08F69D6003B254264779E9C3
          FD317767460CB0DD2B2754B07DC6F20CFD89D1E84F1013A2A19784E1928FB79E
          0D38A5ECE2647759659BAAC73A8E29551926D5656CFFD6D58ABEBD917344411F
          138ACBEBBD7AB6B8F37DB84D3A31F2A12B3AF5F3111AF9CDE368B9AFC5A4B210
          B4A111DD645AB7349CF470B44705E1C21ACF5E7F81AB2FB3096EF2B284E226CD
          D3A12956E6B07676A3531C022381E94FC20271DECBC3E4C7E76D725CA3605F41
          7D6D93F9CB6FB2DAF80991795A942665A3234A8496507F9CF514F66D74E1F971
          32B9A79812C615B5DA721B3FCECBB5AFC61091FB98A065C98B97E3F40AF7BE0D
          3CE7CDCCEE39990B1044A45C531C28D7DBCFD55B71533F8A1D395AEC9C23EC4C
          0344C9D596D5C255018E3207F7814251C2C5B4C80CF587F0740DB6CB55D8965A
          879014E56C60E255DDF2B541F39333DD5C28471CB7E0465849F0227833CC9D3D
          1C3F6C1100FE8B5F5E8AB24DCA40F5DB0000000049454E44AE426082}
      end>
  end
end
