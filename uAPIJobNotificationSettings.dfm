object frmAPIJobNotificationSettings: TfrmAPIJobNotificationSettings
  Left = 0
  Top = 0
  Caption = 'API Job Notification Settings'
  ClientHeight = 427
  ClientWidth = 827
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gbSuccessNotification: TcxGroupBox
    Left = 8
    Top = 40
    Caption = 'Success Notification'
    TabOrder = 0
    Height = 185
    Width = 508
    object lblSuccessNotificationTitle: TLabel
      Left = 37
      Top = 32
      Width = 27
      Height = 13
      Caption = 'Title :'
      FocusControl = dteSuccessNotificationTitle
    end
    object lblSuccessNotificationHeader: TLabel
      Left = 22
      Top = 59
      Width = 42
      Height = 13
      Caption = 'Header :'
      FocusControl = dmmSuccessNotificationHeader
    end
    object lblSuccessNotificationMessage: TLabel
      Left = 15
      Top = 106
      Width = 49
      Height = 13
      Caption = 'Message :'
      FocusControl = dmmSuccessNotificationMessage
    end
    object dteSuccessNotificationTitle: TcxDBTextEdit
      Left = 70
      Top = 29
      DataBinding.DataField = 'SuccessNotificationTitle'
      DataBinding.DataSource = dsNotificationSettings
      TabOrder = 0
      OnEnter = dteSuccessNotificationTitleEnter
      Width = 427
    end
    object dmmSuccessNotificationHeader: TcxDBMemo
      Left = 70
      Top = 56
      DataBinding.DataField = 'SuccessNotificationHeader'
      DataBinding.DataSource = dsNotificationSettings
      TabOrder = 1
      OnEnter = dmmSuccessNotificationHeaderEnter
      Height = 41
      Width = 427
    end
    object dmmSuccessNotificationMessage: TcxDBMemo
      Left = 70
      Top = 103
      DataBinding.DataField = 'SuccessNotificationMessage'
      DataBinding.DataSource = dsNotificationSettings
      TabOrder = 2
      OnEnter = dmmSuccessNotificationMessageEnter
      Height = 57
      Width = 427
    end
  end
  object gbErrorNotification: TcxGroupBox
    Left = 8
    Top = 231
    Caption = 'Error Notification'
    TabOrder = 1
    Height = 185
    Width = 508
    object Label1: TLabel
      Left = 37
      Top = 37
      Width = 27
      Height = 13
      Caption = 'Title :'
      FocusControl = dteErrorNotificationTitle
    end
    object Label2: TLabel
      Left = 22
      Top = 59
      Width = 42
      Height = 13
      Caption = 'Header :'
      FocusControl = dmmErrorNotificationHeader
    end
    object Label3: TLabel
      Left = 15
      Top = 106
      Width = 49
      Height = 13
      Caption = 'Message :'
      FocusControl = dmmErrorNotificationMessage
    end
    object dteErrorNotificationTitle: TcxDBTextEdit
      Left = 70
      Top = 29
      DataBinding.DataField = 'ErrorNotificationTitle'
      DataBinding.DataSource = dsNotificationSettings
      TabOrder = 0
      OnEnter = dteErrorNotificationTitleEnter
      Width = 427
    end
    object dmmErrorNotificationHeader: TcxDBMemo
      Left = 70
      Top = 56
      DataBinding.DataField = 'ErrorNotificationHeader'
      DataBinding.DataSource = dsNotificationSettings
      TabOrder = 1
      OnEnter = dmmErrorNotificationHeaderEnter
      Height = 41
      Width = 427
    end
    object dmmErrorNotificationMessage: TcxDBMemo
      Left = 70
      Top = 103
      DataBinding.DataField = 'ErrorNotificationMessage'
      DataBinding.DataSource = dsNotificationSettings
      TabOrder = 2
      OnEnter = dmmErrorNotificationMessageEnter
      Height = 57
      Width = 427
    end
  end
  object cxGroupBox1: TcxGroupBox
    Left = 522
    Top = 40
    Caption = 'Fields That Could Be Translated'
    TabOrder = 2
    Height = 377
    Width = 295
    object lblJobFields: TLabel
      Left = 19
      Top = 20
      Width = 97
      Height = 13
      Caption = 'Job Execution Fields'
    end
    object llResponseFields: TLabel
      Left = 19
      Top = 166
      Width = 77
      Height = 13
      Caption = 'Response Fields'
    end
    object lblRegisteredFields: TLabel
      Left = 163
      Top = 20
      Width = 82
      Height = 13
      Caption = 'Registered Fields'
    end
    object mmJobExecFields: TcxMemo
      Left = 10
      Top = 39
      Lines.Strings = (
        'Location'
        'Resource'
        'JobId'
        'FormCaption'
        'FormName'
        'Params'
        'Response')
      Properties.ReadOnly = True
      Properties.ScrollBars = ssHorizontal
      TabOrder = 0
      OnClick = mmJobExecFieldsClick
      Height = 121
      Width = 127
    end
    object mmResponseFields: TcxMemo
      Left = 10
      Top = 185
      Properties.ReadOnly = True
      Properties.ScrollBars = ssHorizontal
      TabOrder = 1
      OnClick = mmResponseFieldsClick
      Height = 178
      Width = 127
    end
    object mmRegisteredFields: TcxMemo
      Left = 154
      Top = 39
      Properties.ReadOnly = True
      Properties.ScrollBars = ssHorizontal
      TabOrder = 2
      OnClick = mmRegisteredFieldsClick
      Height = 324
      Width = 127
    end
  end
  object dcbSendsWindowsNotification: TcxDBCheckBox
    Left = 23
    Top = 10
    Caption = 'Sends Windows Notification'
    DataBinding.DataField = 'SendsWindowsNotification'
    DataBinding.DataSource = dsNotificationSettings
    Style.TransparentBorder = False
    TabOrder = 3
  end
  object dcbCustomizeNotification: TcxDBCheckBox
    Left = 320
    Top = 10
    Caption = 'Customize Notification'
    DataBinding.DataField = 'CustomizeNotification'
    DataBinding.DataSource = dsNotificationSettings
    Style.TransparentBorder = False
    TabOrder = 4
  end
  object dsNotificationSettings: TDataSource
    DataSet = frmAPIInProgSettings.mtAPIJobs
    Left = 192
    Top = 48
  end
end
