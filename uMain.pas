unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.Stan.Error,
  Data.Bind.GenData, Data.DB, Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Data.Bind.ObjectScope, Data.Bind.Components,
  FireDAC.Comp.DataSet, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, Vcl.Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges, dxScrollbarAnnotations, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, dxBarBuiltInMenu,
  cxGridCustomPopupMenu, cxGridPopupMenu;

type
  TForm1 = class(TForm)
    FDMemTable1: TFDMemTable;
    DataSource1: TDataSource;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    FDMemTable2: TFDMemTable;
    DataSource2: TDataSource;
    DBGrid2: TDBGrid;
    btnFrm2: TButton;
    btnSetup: TButton;
    btnDemoAPI1: TButton;
    btnDemoAPI2: TButton;
    FDConnection1: TFDConnection;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    PopupMenu1: TPopupMenu;
    SampleMenuItem1: TMenuItem;
    //procedure btnFillDataClick(Sender: TObject);
    procedure btnFrm2Click(Sender: TObject);
    procedure btnSetupClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnDemoAPI2Click(Sender: TObject);
    procedure btnDemoAPI1Click(Sender: TObject);
  private
    procedure FillallData;
    { Private declarations }
  public
    { Public declarations }

  end;



var
  Form1: TForm1;

implementation
uses
uGlobal,uFrm2 , uAPIInProg;

{$R *.dfm}

procedure TForm1.btnDemoAPI1Click(Sender: TObject);
begin
  ShowMessage('Demo API 1 Onclick has been run!');
end;

procedure TForm1.btnFrm2Click(Sender: TObject);
begin
  form2.Show ;
end;

procedure TForm1.btnSetupClick(Sender: TObject);
begin
  //ShowMessage('This should popup Setup screen');
  uAPIInProg.Show_API_SetupForm(self);
end;

procedure TForm1.btnDemoAPI2Click(Sender: TObject);
begin
  ShowMessage('Demo API 2 Onclick has been run!');
end;

procedure TForm1.FillallData;
begin
  FillData(FDMemTable1);
  FillData(FDMemTable2);

  cxGrid1DBTableView1.DataController.CreateAllItems;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  FillallData;
      // apiinprog
  RegisterAPIDataset(Self, 'Data1', FDMemTable1);
  RegisterAPIDataset(Self, 'Data2', FDMemTable2);

  RegisterAPIConnection(Self, FDConnection1);

  //UpdateButtonCodeAndGUI(btnDemoAPI1, TGUID.Create('{FF0270B6-DDB5-4308-9123-2433657B7B27}'),True,'API Call');
  //UpdateButtonCodeAndGUI(btnDemoAPI2, TGUID.Create('{685D4C7D-D12C-46CB-8C43-471D89C6E41F}'),
  // brjeBeforeOriginalOnClickEvent, True, 'Test');

  //CreateAPIJobContextMenuItem(Self, 'Rest API Call Group 1');
  //CreateAPIJobContextMenuItem(Self);

  //CreateGridAPIJobContextMenuItem(DBGrid1);
  //CreateGridAPIJobContextMenuItem(DBGrid2);

  //CreatecxGridAPIJobContextMenuItem(cxGrid1);

  //CreatecxGridPopupMenuAPIJobContextMenuItem(cxGridPopupMenu1);
  try
  if dmAPIInProg  = nil then
    dmAPIInProg := TdmAPIInProg.Create(Application);
  except
    on e: Exception do
    begin
      ShowMessage(e.Message);
      exit;
    end;
  end;
  dmAPIInProg.APIJobUpdateGUI(self);
end;

end.
