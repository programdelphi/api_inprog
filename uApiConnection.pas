unit uApiConnection;

interface

uses
  REST.Client,
  REST.Types,
  uRecords;


const
  F_BASE_URL = 'http://connect.compudime.com:60606/api/v1/TServerMethods1/';

type
  TRESTApiConnectionManager = class
  private
    FRESTClient: TRESTClient;
  public
    procedure ConfigureRESTComponents(var LCurrRestCall: TRestCall; const AMethod: TRESTRequestMethod; var ARequest: TRESTRequest; var AResponse: TRESTResponse); virtual;
    constructor Create;
    destructor Destroy; override;
  end;

implementation

{ TApiConnectionManager }

procedure TRESTApiConnectionManager.ConfigureRESTComponents(var LCurrRestCall:
    TRestCall; const AMethod: TRESTRequestMethod; var ARequest: TRESTRequest;
    var AResponse: TRESTResponse);
begin
  ARequest := nil;
  AResponse := nil;

  if (not Assigned(ARequest)) then
    ARequest := TRESTRequest.Create(nil);
  if (not Assigned(AResponse)) then
    AResponse := TRESTResponse.Create(nil);

  ARequest.Client := FRESTClient;
  ARequest.Client.BaseURL := LCurrRestCall.BaseURL ;
  ARequest.Accept := 'application/json, text/plain; q=0.9, text/html;q=0.8';
  ARequest.AcceptCharset := 'UTF-8, *;q=0.8';
  ARequest.Method := AMethod;
  ARequest.Resource := LCurrRestCall.Resource+'?'+LCurrRestCall.Params ;

  ARequest.Response := AResponse;

  with ARequest.Params.AddItem do
  begin
    name := 'Content-Type';
    Kind := pkHTTPHEADER;
    Options := [poDoNotEncode];
    ContentType := ctNone;
    Value := 'application/json';
  end;

  with ARequest.Params.AddItem do
  begin
    name := 'Authorization';
    Kind := pkHTTPHEADER;
    Options := [poDoNotEncode];
    ContentType := ctNone;
    Value := '';
  end;

  with ARequest.Params.AddItem do
  begin
    name := 'body';
    Kind := pkREQUESTBODY;
    Options := [poDoNotEncode];
    ContentType := ctAPPLICATION_JSON;
    Value := '';
  end;
end;

constructor TRESTApiConnectionManager.Create;
begin
  FRESTClient := TRESTClient.Create(F_BASE_URL);
  FRESTClient.Accept := 'application/json, text/plain; q=0.9, text/html;q=0.8';
  FRESTClient.AcceptCharset := 'UTF-8, *;q=0.8';
  FRESTClient.ContentType := 'application/json';
  FRESTClient.FallbackCharsetEncoding := 'UTF-8';
  FRESTClient.UserAgent := 'Embarcadero RESTClient/1.0';
end;

destructor TRESTApiConnectionManager.Destroy;
begin
  FRESTClient.DisposeOf;
  inherited;
end;

end.

