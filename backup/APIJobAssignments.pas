unit APIJobAssignments;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges, dxScrollbarAnnotations, Data.DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, cxContainer, cxGroupBox,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.Menus, cxDBExtLookupComboBox, cxButtons,
  cxDropDownEdit;

type
  TfrmAPIJobAssignments = class(TForm)
    gbButtonAPIJobAssignments: TcxGroupBox;
    grdButtonAPIJobAssinments: TcxGrid;
    grdButtonAPIJobAssinmentsDBTV: TcxGridDBTableView;
    grdButtonAPIJobAssinmentsL: TcxGridLevel;
    mtFormButtonJobAssignments: TFDMemTable;
    mtFormButtonJobAssignmentsFormClass: TStringField;
    mtFormButtonJobAssignmentsID: TGuidField;
    dsFormButtonJobAssignments: TDataSource;
    mtFormButtonJobAssignmentsLJobCaption: TStringField;
    mtFormButtonJobAssignmentsButtonName: TStringField;
    grdButtonAPIJobAssinmentsDBTVFormClass: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVJobID: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVLJobCaption: TcxGridDBColumn;
    mtFormButtons: TFDMemTable;
    mtFormButtonsCaption: TStringField;
    mtFormButtonsName: TStringField;
    mtFormMenus: TFDMemTable;
    mtFormButtonJobAssignmentsLButtonName: TStringField;
    mtFormButtonJobAssignmentsLButtonCaption: TStringField;
    grdButtonAPIJobAssinmentsDBTVLButtonName: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVLButtonCaption: TcxGridDBColumn;
    gbMenuAPIJobAssignmentts: TcxGroupBox;
    grdMenuAPIJobAssinments: TcxGrid;
    grdMenuAPIJobAssinmentsDBTV: TcxGridDBTableView;
    grdMenuAPIJobAssinmentsL: TcxGridLevel;
    mtFormMenuJobAssignments: TFDMemTable;
    dsFormMenuJobAssignments: TDataSource;
    grdMenuAPIJobAssinmentsDBTVFormClass: TcxGridDBColumn;
    mtFormMenuJobAssignmentsFormClass: TStringField;
    mtFormMenuJobAssignmentsMenuName: TStringField;
    mtFormMenuJobAssignmentsLMenuName: TStringField;
    grdMenuAPIJobAssinmentsDBTVMenuName: TcxGridDBColumn;
    grdMenuAPIJobAssinmentsDBTVLMenuName: TcxGridDBColumn;
    mtFormMenuJobAssignmentsJobGroupID: TStringField;
    mtJobGroups: TFDMemTable;
    mtJobGroupsJobGroupID: TStringField;
    mtFormMenuJobAssignmentslJobGroupID: TStringField;
    grdMenuAPIJobAssinmentsDBTVJobGroupID: TcxGridDBColumn;
    grdMenuAPIJobAssinmentsDBTVlJobGroupID: TcxGridDBColumn;
    mtFormMenusName: TStringField;
    mtFormMenuJobAssignmentsAllJobs: TBooleanField;
    grdMenuAPIJobAssinmentsDBTVAllJobs: TcxGridDBColumn;
    gbButtons: TcxGroupBox;
    btnReload: TcxButton;
    btnSave: TcxButton;
    mtFormButtonJobAssignmentsJobExecutionType: TStringField;
    grdButtonAPIJobAssinmentsDBTVJobExecutionType: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure mtFormButtonJobAssignmentsNewRecord(DataSet: TDataSet);
    procedure mtFormMenuJobAssignmentsNewRecord(DataSet: TDataSet);
    procedure mtFormMenuJobAssignmentsAllJobsValidate(Sender: TField);
    procedure mtFormMenuJobAssignmentsBeforePost(DataSet: TDataSet);
    procedure btnReloadClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure mtFormButtonJobAssignmentsAfterPost(DataSet: TDataSet);
    procedure mtFormButtonJobAssignmentsAfterDelete(DataSet: TDataSet);
    procedure mtFormMenuJobAssignmentsAfterPost(DataSet: TDataSet);
    procedure mtFormMenuJobAssignmentsAfterDelete(DataSet: TDataSet);
    procedure mtFormButtonJobAssignmentsBeforePost(DataSet: TDataSet);
    procedure mtFormMenuJobAssignmentsJobGroupIDValidate(Sender: TField);
    procedure mtFormButtonJobAssignmentsAfterEdit(DataSet: TDataSet);
    procedure mtFormButtonJobAssignmentsAfterInsert(DataSet: TDataSet);
    procedure mtFormButtonJobAssignmentsAfterCancel(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    procedure LoadAssignments;
    procedure SaveAssigments;
    { Private declarations }
  public
    { Public declarations }
    frmAPIInProgSettings: TForm;
  end;

var
  frmAPIJobAssignments: TfrmAPIJobAssignments;

implementation

{$R *.dfm}

uses uAPIInProgSettings, System.JSON, System.StrUtils, System.IOUtils, uRecords, uAPIInProg;

procedure TfrmAPIJobAssignments.btnReloadClick(Sender: TObject);
begin
  LoadAssignments;
end;

procedure TfrmAPIJobAssignments.btnSaveClick(Sender: TObject);
begin
  SaveAssigments;
end;

procedure TfrmAPIJobAssignments.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if (btnSave.Enabled or (mtFormButtonJobAssignments.State in [dsInsert, dsEdit]) or (mtFormMenuJobAssignments.State in [dsInsert, dsEdit])) and
  (MessageBox(0, PChar('Save changes ?'), PChar('Close'), MB_ICONWARNING or MB_YESNO) = ID_YES) then
  begin
    if (mtFormButtonJobAssignments.State in [dsInsert, dsEdit])  then
      mtFormButtonJobAssignments.Post;
    if (mtFormMenuJobAssignments.State in [dsInsert, dsEdit])  then
      mtFormMenuJobAssignments.Post;
    btnSave.Click;
    CanClose := CanClose and not btnSave.Enabled;
    if CanClose then
      Close;
  end;
end;

procedure TfrmAPIJobAssignments.FormCreate(Sender: TObject);
var
  i: Integer;
  RequestForm: TForm;
begin
  inherited;

  if self.Owner = nil then
    raise Exception.Create(Format('Form of %s must be created with owner!', [Self.caption ]));

  mtFormButtons.Active := True;
  mtFormMenus.Active := True;

  RequestForm := TfrmAPIInProgSettings(self.Owner).RequestForm;
  if RequestForm <> nil then
  begin
    for i := 0 to RequestForm.ComponentCount - 1 do
    begin
      if RequestForm.Components[i] is TCustomButton then
      begin
        mtFormButtons.Insert;
        mtFormButtonsName.AsString := RequestForm.Components[i].Name;
        // to access caption TButton is used
        mtFormButtonsCaption.AsString := TButton(RequestForm.Components[i]).Caption;
        mtFormButtons.Post;
      end;

      if (RequestForm.Components[i] is TPopupMenu) and
         (RequestForm.Components[i].Name <> 'pumRestAPIJobs') and
         (RequestForm.Components[i].Name <> 'pumRestAPIStatus') and
         (RightStr(RequestForm.Components[i].Name, Length('_pumRestAPIJobs')) <> '_pumRestAPIJobs')  then
      begin
        mtFormMenus.Insert;
        mtFormMenusName.AsString := TPopupMenu(RequestForm.Components[i]).Name;
        mtFormMenus.Post;
      end;

    end;

    mtJobGroups.Active := True;

    TfrmAPIInProgSettings(self.Owner).mtAPIJobs.First;
    while not TfrmAPIInProgSettings(self.Owner).mtAPIJobs.Eof do
    begin
      if not mtJobGroups.Locate('JobGroupID', TfrmAPIInProgSettings(self.Owner).mtAPIJobsGroupID.AsString.Trim, [loCaseInsensitive]) then
      begin
        mtJobGroups.Insert;
        mtJobGroupsJobGroupID.AsString := TfrmAPIInProgSettings(self.Owner).mtAPIJobsGroupID.AsString.Trim;
        mtJobGroups.Post;
      end;
      TfrmAPIInProgSettings(self.Owner).mtAPIJobs.Next;
    end;
  end;

  LoadAssignments;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsAfterCancel(DataSet: TDataSet);
begin
  btnSave.Enabled := TFDMemTable(DataSet).UpdatesPending;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsAfterDelete(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsAfterEdit(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsAfterInsert(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsAfterPost(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsBeforePost(DataSet: TDataSet);
begin
  if mtFormButtonJobAssignmentsFormClass.AsString = '' then
    raise Exception.Create('Form class of button job assignment can not be empty!');
  if mtFormButtonJobAssignmentsButtonName.AsString = '' then
    raise Exception.Create('Button name of button job assignment can not be empty!');
  if mtFormButtonJobAssignmentsID.AsString = '' then
    raise Exception.Create('Job of button job assignment can not be empty!');
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsNewRecord(DataSet: TDataSet);
begin
  if (Self.Owner <> nil) and (TfrmAPIInProgSettings(Self.Owner).RequestForm <> nil) then
    mtFormButtonJobAssignmentsFormClass.AsString := TfrmAPIInProgSettings(Self.Owner).RequestForm.ClassName;
  mtFormButtonJobAssignmentsJobExecutionType.AsString := 'Not Run OnClick';
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsAfterDelete(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsAfterPost(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsAllJobsValidate(Sender: TField);
begin
  if mtFormMenuJobAssignmentsAllJobs.AsBoolean and not mtFormMenuJobAssignmentsMenuName.IsNull then
    mtFormMenuJobAssignmentsMenuName.AsVariant := null;
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsBeforePost(DataSet: TDataSet);
begin
  if mtFormMenuJobAssignmentsFormClass.AsString = '' then
    raise Exception.Create('Form class of menu API job assignment can not be empty!');
  if mtFormMenuJobAssignmentsMenuName.AsString = '' then
    raise Exception.Create('Menu name of menu API job assignment can not be empty!');
  if not mtFormMenuJobAssignmentsAllJobs.AsBoolean and (mtFormMenuJobAssignmentsJobGroupID.AsString = '') then
    raise Exception.Create('Either all jobs must be checked or a job group ID must be selected for the popup menu!');
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsJobGroupIDValidate(Sender: TField);
begin
 if mtFormMenuJobAssignmentsAllJobs.AsBoolean and (mtFormMenuJobAssignmentsJobGroupID.AsString <> '') then
   raise Exception.Create('While All Jobs is selected, Job group ID must be empty!');
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsNewRecord(DataSet: TDataSet);
begin
  if (Self.Owner <> nil) and (TfrmAPIInProgSettings(Self.Owner).RequestForm <> nil) then
    mtFormMenuJobAssignmentsFormClass.AsString := TfrmAPIInProgSettings(Self.Owner).RequestForm.ClassName;
  mtFormMenuJobAssignmentsAllJobs.AsBoolean := False;
end;

procedure TfrmAPIJobAssignments.LoadAssignments;
var
  SavedName: string;
  js: TJSONObject;
begin
  if TFile.Exists(BUTTON_JOB_ASSIGNMENTS_FILENAME) then
  begin
    if mtFormButtonJobAssignments.RecordCount > 0 then
      SavedName := mtFormButtonJobAssignmentsButtonName.AsString
    else
      SavedName := '';

    mtFormButtonJobAssignments.Filtered := False;

    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(BUTTON_JOB_ASSIGNMENTS_FILENAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtFormButtonJobAssignments);
      finally
        js.Free;
      end;
    except
    end;

    if (Self.Owner <> nil) and (TfrmAPIInProgSettings(Self.Owner).RequestForm <> nil) then
      mtFormButtonJobAssignments.Filter := 'FormClass = ' + QuotedStr(TfrmAPIInProgSettings(Self.Owner).RequestForm.ClassName)
    else
      mtFormButtonJobAssignments.Filter := '1=0';
    mtFormButtonJobAssignments.Filtered := mtFormButtonJobAssignments.Filter <> '';

    if (SavedName <> '') and mtFormButtonJobAssignments.Active then
      mtFormButtonJobAssignments.Locate('ButtonName', SavedName, []);
  end;

  if not mtFormButtonJobAssignments.Active  then
    mtFormButtonJobAssignments.Active := True;

  if TFile.Exists(MENU_JOB_ASSIGNMENTS_FILENAME) then
  begin
    if mtFormMenuJobAssignments.RecordCount > 0 then
      SavedName := mtFormMenuJobAssignmentsMenuName.AsString
    else
      SavedName := '';

    mtFormMenuJobAssignments.Filtered := False;

    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(MENU_JOB_ASSIGNMENTS_FILENAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtFormMenuJobAssignments);
      finally
        js.Free;
      end;
    except
    end;

    if (Self.Owner <> nil) and (TfrmAPIInProgSettings(Self.Owner).RequestForm <> nil) then
      mtFormMenuJobAssignments.Filter := 'FormClass = ' + QuotedStr(TfrmAPIInProgSettings(Self.Owner).RequestForm.ClassName)
    else
      mtFormMenuJobAssignments.Filter := '1=0';
    mtFormMenuJobAssignments.Filtered := mtFormMenuJobAssignments.Filter <> '';

    if (SavedName <> '') and mtFormMenuJobAssignments.Active then
      mtFormMenuJobAssignments.Locate('MenuName', SavedName, []);
  end;

  if not mtFormMenuJobAssignments.Active  then
    mtFormMenuJobAssignments.Active := True;

  btnSave.Enabled := False;
end;

procedure TfrmAPIJobAssignments.SaveAssigments;
var
  js: TJSONObject;
begin
  if mtFormButtonJobAssignments.State in dsEditModes then
    mtFormButtonJobAssignments.Post;

  if mtFormMenuJobAssignments.State in dsEditModes then
    mtFormMenuJobAssignments.Post;

  js := MT2JS(mtFormButtonJobAssignments);
  try
    TFile.WriteAllText(BUTTON_JOB_ASSIGNMENTS_FILENAME, js.ToString);
  finally
    js.Free;
  end;

  if mtFormButtonJobAssignments.UpdatesPending then
    mtFormButtonJobAssignments.CommitUpdates;

  if (dmAPIInProg <> nil) and dmAPIInProg.mtFormButtonJobAssignments.Active then
    dmAPIInProg.mtFormButtonJobAssignments.Active := False;

  js := MT2JS(mtFormMenuJobAssignments);
  try
    TFile.WriteAllText(MENU_JOB_ASSIGNMENTS_FILENAME, js.ToString);
  finally
    js.Free;
  end;

  if mtFormMenuJobAssignments.UpdatesPending then
    mtFormMenuJobAssignments.CommitUpdates;

  if (dmAPIInProg <> nil) and dmAPIInProg.mtFormMenuJobAssignments.Active then
    dmAPIInProg.mtFormMenuJobAssignments.Active := False;

  btnSave.Enabled := False;
end;

end.
