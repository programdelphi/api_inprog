object frmAPIJobAssignments: TfrmAPIJobAssignments
  Left = 0
  Top = 0
  Caption = 'UI API Job Assignments'
  ClientHeight = 689
  ClientWidth = 966
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object gbButtonAPIJobAssignments: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    Caption = 'Button API Job Assignmentts'
    TabOrder = 0
    Height = 379
    Width = 966
    object grdButtonAPIJobAssinments: TcxGrid
      Left = 2
      Top = 18
      Width = 962
      Height = 359
      Align = alClient
      TabOrder = 0
      object grdButtonAPIJobAssinmentsDBTV: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Visible = True
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dsFormButtonJobAssignments
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.GroupByBox = False
        object grdButtonAPIJobAssinmentsDBTVFormClass: TcxGridDBColumn
          DataBinding.FieldName = 'FormClass'
          Visible = False
          Options.Editing = False
        end
        object grdButtonAPIJobAssinmentsDBTVJobID: TcxGridDBColumn
          DataBinding.FieldName = 'JobID'
          Visible = False
          Options.Editing = False
        end
        object grdButtonAPIJobAssinmentsDBTVLButtonName: TcxGridDBColumn
          DataBinding.FieldName = 'LButtonName'
          Width = 153
        end
        object grdButtonAPIJobAssinmentsDBTVLButtonCaption: TcxGridDBColumn
          DataBinding.FieldName = 'LButtonCaption'
          Options.Editing = False
          Width = 224
        end
        object grdButtonAPIJobAssinmentsDBTVLJobCaption: TcxGridDBColumn
          DataBinding.FieldName = 'LJobCaption'
          Width = 371
        end
        object grdButtonAPIJobAssinmentsDBTVJobExecutionType: TcxGridDBColumn
          DataBinding.FieldName = 'JobExecutionType'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.Items.Strings = (
            'Before OnClick'
            'After OnClick'
            'Not Run OnClick')
          Width = 186
        end
      end
      object grdButtonAPIJobAssinmentsL: TcxGridLevel
        Caption = 'Button Rule Assignments'
        GridView = grdButtonAPIJobAssinmentsDBTV
      end
    end
  end
  object gbMenuAPIJobAssignmentts: TcxGroupBox
    Left = 0
    Top = 379
    Align = alBottom
    Caption = 'Menu API Job Assignmentts'
    TabOrder = 1
    Height = 273
    Width = 966
    object grdMenuAPIJobAssinments: TcxGrid
      Left = 2
      Top = 18
      Width = 962
      Height = 253
      Align = alClient
      TabOrder = 0
      object grdMenuAPIJobAssinmentsDBTV: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Visible = True
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dsFormMenuJobAssignments
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.GroupByBox = False
        object grdMenuAPIJobAssinmentsDBTVFormClass: TcxGridDBColumn
          DataBinding.FieldName = 'FormClass'
          Visible = False
          Options.Editing = False
        end
        object grdMenuAPIJobAssinmentsDBTVMenuName: TcxGridDBColumn
          DataBinding.FieldName = 'MenuName'
          Visible = False
          Options.Editing = False
        end
        object grdMenuAPIJobAssinmentsDBTVLMenuName: TcxGridDBColumn
          DataBinding.FieldName = 'LMenuName'
          Width = 400
        end
        object grdMenuAPIJobAssinmentsDBTVAllJobs: TcxGridDBColumn
          DataBinding.FieldName = 'AllJobs'
          Width = 66
        end
        object grdMenuAPIJobAssinmentsDBTVJobGroupID: TcxGridDBColumn
          DataBinding.FieldName = 'JobGroupID'
          Visible = False
          Options.Editing = False
          Width = 300
        end
        object grdMenuAPIJobAssinmentsDBTVlJobGroupID: TcxGridDBColumn
          DataBinding.FieldName = 'lJobGroupID'
          Width = 463
        end
      end
      object grdMenuAPIJobAssinmentsL: TcxGridLevel
        Caption = 'Button Rule Assignments'
        GridView = grdMenuAPIJobAssinmentsDBTV
      end
    end
  end
  object gbButtons: TcxGroupBox
    Left = 0
    Top = 652
    Align = alBottom
    PanelStyle.Active = True
    TabOrder = 2
    Height = 37
    Width = 966
    object btnReload: TcxButton
      Left = 11
      Top = 6
      Width = 118
      Height = 25
      Caption = '&Reload Assigments'
      TabOrder = 0
      OnClick = btnReloadClick
    end
    object btnSave: TcxButton
      Left = 151
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Save'
      Enabled = False
      TabOrder = 1
      OnClick = btnSaveClick
    end
  end
  object mtFormButtonJobAssignments: TFDMemTable
    AfterInsert = mtFormButtonJobAssignmentsAfterInsert
    AfterEdit = mtFormButtonJobAssignmentsAfterEdit
    BeforePost = mtFormButtonJobAssignmentsBeforePost
    AfterPost = mtFormButtonJobAssignmentsAfterPost
    AfterCancel = mtFormButtonJobAssignmentsAfterCancel
    AfterDelete = mtFormButtonJobAssignmentsAfterDelete
    OnNewRecord = mtFormButtonJobAssignmentsNewRecord
    CachedUpdates = True
    Indexes = <
      item
        Active = True
        Fields = 'FormClass;ButtonName'
        Options = [soNoCase, soUnique]
      end>
    ConstraintsEnabled = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 280
    Top = 81
    object mtFormButtonJobAssignmentsFormClass: TStringField
      DisplayLabel = 'Form Class'
      FieldName = 'FormClass'
      Size = 100
    end
    object mtFormButtonJobAssignmentsID: TGuidField
      DisplayLabel = 'Job ID'
      FieldName = 'JobID'
      Size = 38
    end
    object mtFormButtonJobAssignmentsLJobCaption: TStringField
      DisplayLabel = 'Job Caption'
      FieldKind = fkLookup
      FieldName = 'LJobCaption'
      LookupDataSet = frmAPIInProgSettings.mtAPIJobs
      LookupKeyFields = 'ID'
      LookupResultField = 'Caption'
      KeyFields = 'JobID'
      Size = 100
      Lookup = True
    end
    object mtFormButtonJobAssignmentsButtonName: TStringField
      DisplayLabel = 'Button Name'
      FieldName = 'ButtonName'
      Size = 100
    end
    object mtFormButtonJobAssignmentsLButtonName: TStringField
      DisplayLabel = 'Button Name'
      FieldKind = fkLookup
      FieldName = 'LButtonName'
      LookupDataSet = mtFormButtons
      LookupKeyFields = 'Name'
      LookupResultField = 'Name'
      KeyFields = 'ButtonName'
      Size = 100
      Lookup = True
    end
    object mtFormButtonJobAssignmentsLButtonCaption: TStringField
      DisplayLabel = 'Button Caption'
      FieldKind = fkLookup
      FieldName = 'LButtonCaption'
      LookupDataSet = mtFormButtons
      LookupKeyFields = 'Name'
      LookupResultField = 'Caption'
      KeyFields = 'ButtonName'
      Size = 200
      Lookup = True
    end
    object mtFormButtonJobAssignmentsJobExecutionType: TStringField
      DisplayLabel = 'Job Execution Type'
      FieldName = 'JobExecutionType'
      Size = 100
    end
  end
  object dsFormButtonJobAssignments: TDataSource
    DataSet = mtFormButtonJobAssignments
    Left = 424
    Top = 80
  end
  object mtFormButtons: TFDMemTable
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 104
    Top = 73
    object mtFormButtonsName: TStringField
      DisplayLabel = 'Button Name'
      FieldName = 'Name'
      Size = 100
    end
    object mtFormButtonsCaption: TStringField
      FieldName = 'Caption'
      Size = 200
    end
  end
  object mtFormMenus: TFDMemTable
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 104
    Top = 201
    object mtFormMenusName: TStringField
      DisplayLabel = 'Button Name'
      FieldName = 'Name'
      Size = 100
    end
  end
  object mtFormMenuJobAssignments: TFDMemTable
    BeforePost = mtFormMenuJobAssignmentsBeforePost
    AfterPost = mtFormMenuJobAssignmentsAfterPost
    AfterDelete = mtFormMenuJobAssignmentsAfterDelete
    OnNewRecord = mtFormMenuJobAssignmentsNewRecord
    CachedUpdates = True
    Indexes = <
      item
        Active = True
        Fields = 'FormClass;MenuName'
        Options = [soNoCase, soUnique]
      end>
    ConstraintsEnabled = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 328
    Top = 401
    object mtFormMenuJobAssignmentsFormClass: TStringField
      DisplayLabel = 'Form Class'
      FieldName = 'FormClass'
      Size = 100
    end
    object mtFormMenuJobAssignmentsMenuName: TStringField
      DisplayLabel = 'Menu Name'
      FieldName = 'MenuName'
      Size = 100
    end
    object mtFormMenuJobAssignmentsLMenuName: TStringField
      DisplayLabel = 'Menu Name'
      FieldKind = fkLookup
      FieldName = 'LMenuName'
      LookupDataSet = mtFormMenus
      LookupKeyFields = 'Name'
      LookupResultField = 'Name'
      KeyFields = 'MenuName'
      Size = 200
      Lookup = True
    end
    object mtFormMenuJobAssignmentsJobGroupID: TStringField
      DisplayLabel = 'Job Group ID'
      FieldName = 'JobGroupID'
      OnValidate = mtFormMenuJobAssignmentsJobGroupIDValidate
      Size = 200
    end
    object mtFormMenuJobAssignmentslJobGroupID: TStringField
      DisplayLabel = 'Job Group ID'
      FieldKind = fkLookup
      FieldName = 'lJobGroupID'
      LookupDataSet = mtJobGroups
      LookupKeyFields = 'JobGroupID'
      LookupResultField = 'JobGroupID'
      KeyFields = 'JobGroupID'
      Size = 200
      Lookup = True
    end
    object mtFormMenuJobAssignmentsAllJobs: TBooleanField
      DisplayLabel = 'All Jobs'
      FieldName = 'AllJobs'
      OnValidate = mtFormMenuJobAssignmentsAllJobsValidate
    end
  end
  object dsFormMenuJobAssignments: TDataSource
    DataSet = mtFormMenuJobAssignments
    Left = 416
    Top = 408
  end
  object mtJobGroups: TFDMemTable
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 112
    Top = 409
    object mtJobGroupsJobGroupID: TStringField
      DisplayLabel = 'Job Group ID'
      FieldName = 'JobGroupID'
      Size = 200
    end
  end
end
