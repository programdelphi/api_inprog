unit uAddFields;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxCheckBox, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxGroupBox, cxCustomListBox, cxCheckListBox,
  System.Types, Spring, Spring.Collections, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  dxDateRanges, dxScrollbarAnnotations, cxTextEdit, cxGridCustomTableView, cxGridTableView, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid;

type
  TfAddFields = class(TForm)
    cxGroupBox1: TcxGroupBox;
    btnUnselect: TcxButton;
    btnSelect: TcxButton;
    btnReverse: TcxButton;
    cxGroupBox2: TcxGroupBox;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    grdFieldsLevel1: TcxGridLevel;
    grdFields: TcxGrid;
    viewFields: TcxGridTableView;
    colCheck: TcxGridColumn;
    colName: TcxGridColumn;
    procedure btnSelectClick(Sender: TObject);
    procedure btnUnselectClick(Sender: TObject);
    procedure btnReverseClick(Sender: TObject);
    procedure colCheckPropertiesValidate(Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
  public
    class function Run(AFieldsChecks: IList<Tuple<string, Boolean>>): Boolean;
  end;

implementation

{$R *.dfm}

{ TfAddFields }

procedure TfAddFields.btnReverseClick(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to viewFields.DataController.RecordCount - 1 do
    viewFields.DataController.Values[I, colCheck.Index] := not viewFields.DataController.Values[I, colCheck.Index]
end;

procedure TfAddFields.btnSelectClick(Sender: TObject);
var
  I, j: Integer;
  Name, FieldName, RecName, RecFieldName: string;
  SameFieldNameAlreadySelected: Boolean;
begin
  for I := 0 to viewFields.DataController.RecordCount - 1 do
  begin
    if viewFields.DataController.Values[I, colCheck.Index] = False then
    begin
      Name := viewFields.DataController.Values[I, colName.Index];
      FieldName := Name.Substring(Name.IndexOf('.') + 1);
      SameFieldNameAlreadySelected := False;
      for j := 0 to viewFields.DataController.RecordCount - 1 do
      begin
        if (j <> i) and
           (viewFields.DataController.Values[j, colCheck.Index] = True) then
        begin
          RecName :=viewFields.DataController.Values[j, colName.Index];
          RecFieldName := RecName.Substring(RecName.IndexOf('.') + 1);
          if FieldName = RecFieldName then
          begin
            SameFieldNameAlreadySelected := True;
            break;
          end;
        end;
      end;
      if not SameFieldNameAlreadySelected then
        viewFields.DataController.Values[I, colCheck.Index] := True;
    end;
  end;
end;

procedure TfAddFields.btnUnselectClick(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to viewFields.DataController.RecordCount - 1 do
    viewFields.DataController.Values[I, colCheck.Index] := False
end;

procedure TfAddFields.colCheckPropertiesValidate(Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var
  i: Integer;
  Name, FieldName, RecName, RecFieldName: string;
begin
  if DisplayValue = True then
  begin
    Name := viewFields.DataController.Values[viewFields.DataController.FocusedRecordIndex, colName.Index];
    FieldName := Name.Substring(Name.IndexOf('.') + 1);
    for i := 0 to viewFields.DataController.RecordCount - 1 do
    begin
      if (i <> viewFields.DataController.FocusedRecordIndex) and
         (viewFields.DataController.Values[I, colCheck.Index] = True) then
      begin
        RecName :=viewFields.DataController.Values[I, colName.Index];
        RecFieldName := RecName.Substring(RecName.IndexOf('.') + 1);
        if FieldName = RecFieldName then
          raise Exception.Create('You can not select more than one field with the same field name!');
      end;
    end;
  end;
end;

class function TfAddFields.Run(AFieldsChecks: IList<Tuple<string, Boolean>>): Boolean;
var
  Form: TfAddFields;
  I: Integer;
begin
  Form := TfAddFields.Create(nil);
  try
    Form.viewFields.DataController.RecordCount := AFieldsChecks.Count;
    for I := 0 to AFieldsChecks.Count - 1 do
    begin
      Form.viewFields.DataController.Values[I, Form.colCheck.Index] := AFieldsChecks[I].Value2;
      Form.viewFields.DataController.Values[I, Form.colName.Index] := AFieldsChecks[I].Value1;
    end;
    Result := Form.ShowModal = mrOk;
    if Result then
      for I := 0 to AFieldsChecks.Count - 1 do
        AFieldsChecks[I] := Tuple.Create<string, Boolean>(Form.viewFields.DataController.Values[I,
          Form.colName.Index], Form.viewFields.DataController.Values[I, Form.colCheck.Index]);
  finally
    Form.Free;
  end;
end;

end.
