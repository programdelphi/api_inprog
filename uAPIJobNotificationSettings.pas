unit uAPIJobNotificationSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxMemo, cxDBEdit, cxTextEdit, Vcl.StdCtrls, cxGroupBox, Data.DB, cxCustomListBox, cxListBox, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, dxDateRanges, dxScrollbarAnnotations, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, cxCheckBox;

type
  TfrmAPIJobNotificationSettings = class(TForm)
    gbSuccessNotification: TcxGroupBox;
    lblSuccessNotificationTitle: TLabel;
    dteSuccessNotificationTitle: TcxDBTextEdit;
    lblSuccessNotificationHeader: TLabel;
    lblSuccessNotificationMessage: TLabel;
    dmmSuccessNotificationHeader: TcxDBMemo;
    dmmSuccessNotificationMessage: TcxDBMemo;
    gbErrorNotification: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    dteErrorNotificationTitle: TcxDBTextEdit;
    dmmErrorNotificationHeader: TcxDBMemo;
    dmmErrorNotificationMessage: TcxDBMemo;
    dsNotificationSettings: TDataSource;
    cxGroupBox1: TcxGroupBox;
    mmJobExecFields: TcxMemo;
    lblJobFields: TLabel;
    mmResponseFields: TcxMemo;
    llResponseFields: TLabel;
    mmRegisteredFields: TcxMemo;
    lblRegisteredFields: TLabel;
    dcbSendsWindowsNotification: TcxDBCheckBox;
    dcbCustomizeNotification: TcxDBCheckBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure mmJobExecFieldsClick(Sender: TObject);
    procedure dteSuccessNotificationTitleEnter(Sender: TObject);
    procedure dmmSuccessNotificationHeaderEnter(Sender: TObject);
    procedure dmmSuccessNotificationMessageEnter(Sender: TObject);
    procedure dteErrorNotificationTitleEnter(Sender: TObject);
    procedure dmmErrorNotificationHeaderEnter(Sender: TObject);
    procedure dmmErrorNotificationMessageEnter(Sender: TObject);
    procedure mmResponseFieldsClick(Sender: TObject);
    procedure mmRegisteredFieldsClick(Sender: TObject);
  private
    { Private declarations }
    TextEdit: TcxCustomTextEdit;
    procedure InsertFieldToEditText(SourceMemo: TcxMemo);
  public
    { Public declarations }
    JobID: TGUID;
    SourceForm: TForm;
  end;

var
  frmAPIJobNotificationSettings: TfrmAPIJobNotificationSettings;

implementation

{$R *.dfm}

uses uAPIInProg, System.Generics.Collections;

procedure TfrmAPIJobNotificationSettings.dmmErrorNotificationHeaderEnter(Sender: TObject);
begin
  TextEdit := dmmErrorNotificationHeader;
end;

procedure TfrmAPIJobNotificationSettings.dmmErrorNotificationMessageEnter(Sender: TObject);
begin
  TextEdit := dmmErrorNotificationMessage;
end;

procedure TfrmAPIJobNotificationSettings.dmmSuccessNotificationHeaderEnter(Sender: TObject);
begin
  TextEdit := dmmSuccessNotificationHeader;
end;

procedure TfrmAPIJobNotificationSettings.dmmSuccessNotificationMessageEnter(Sender: TObject);
begin
  TextEdit := dmmSuccessNotificationMessage;
end;

procedure TfrmAPIJobNotificationSettings.dteErrorNotificationTitleEnter(Sender: TObject);
begin
  TextEdit := dteErrorNotificationTitle;
end;

procedure TfrmAPIJobNotificationSettings.dteSuccessNotificationTitleEnter(Sender: TObject);
begin
  TextEdit := dteSuccessNotificationTitle;
end;

procedure TfrmAPIJobNotificationSettings.FormClose(Sender: TObject; var Action: TCloseAction);
var
  WCtrl: TWinControl;
begin
  if ActiveControl <> nil then
  begin
    WCtrl := Self.FindNextControl(ActiveControl, True, True, False);
    WCtrl.SetFocus;
  end;
end;

procedure TfrmAPIJobNotificationSettings.FormShow(Sender: TObject);
var
  i, j : integer;
  DomainName, FieldName: String;
  ADictDomain: TDictionary<string, TDictionary<string, TField>>;
  //ADictDomainItem: TPair<string, TDictionary<string, TField>>;
  ADictDomainFields: TDictionary<string, TField>;
  //ADictDomainFieldsItem : TPair<string, TField>;
  DomainList, Fldlist: TList<string>;
begin
  mmResponseFields.Lines.Clear;
  mmRegisteredFields.Lines.Clear;

  if dsNotificationSettings.DataSet = nil then
    exit;

  if ((dmAPIInProg.mtAPIJobsID.AsString <> JobID.ToString) and
     not dmAPIInProg.mtAPIJobs.Locate('ID', JobID.ToString, [])) then
    exit;

  if dmAPIInProg.mtAPIJobResponseFields.RecordCount > 0 then
  begin
    try
      dmAPIInProg.mtAPIJobResponseFields.IndexName := dmAPIInProg.mtAPIJobResponseFields.Indexes[1].Name;
      // IX_FC_Job_OrderNoDef - FormClass;JobID;OrderNo
      dmAPIInProg.mtAPIJobResponseFields.First;
      while not dmAPIInProg.mtAPIJobResponseFields.Eof do
      begin
        mmResponseFields.Lines.Add(dmAPIInProg.mtAPIJobResponseFieldsResponseFieldName.AsString);
        dmAPIInProg.mtAPIJobResponseFields.Next;
      end;
    finally
      dmAPIInProg.mtAPIJobResponseFields.IndexName := dmAPIInProg.mtAPIJobResponseFields.Indexes[0].Name;
      // IX_FC_Job_ResponseFieldName - FormClass;JobID;ResponseFieldName
    end;
  end;

  if DictFormRestAPIDomain.TryGetValue(Sourceform, ADictDomain) then
  begin
    DomainList := TList<string>.Create(ADictDomain.Keys);
    Domainlist.Sort;
    for i := 0 to DomainList.Count - 1 do
    begin
      DomainName := DomainList[i];
      ADictDomainFields := ADictDomain[DomainName];

      FldList := TList<string>.Create(ADictDomainFields.Keys);
      FldList.Sort;

      for j := 0 to FldList.Count - 1 do
      begin
        FieldName := FldList[j];
        mmRegisteredFields.Lines.Add(DomainName + '.' + FieldName);
      end;
    end;
  end;
end;

procedure TfrmAPIJobNotificationSettings.mmJobExecFieldsClick(Sender: TObject);
begin
  InsertFieldToEditText(mmJobExecFields);
end;

procedure TfrmAPIJobNotificationSettings.mmRegisteredFieldsClick(Sender: TObject);
begin
  InsertFieldToEditText(mmRegisteredFields);
end;

procedure TfrmAPIJobNotificationSettings.mmResponseFieldsClick(Sender: TObject);
var
  dset: TDataset;
  txt: TCaption;
  InsertStr : string;
  OldCursorPos: integer;
begin
  InsertFieldToEditText(mmResponseFields);
end;

procedure TfrmAPIJobNotificationSettings.InsertFieldToEditText(SourceMemo: TcxMemo);
var
  InsertStr: string;
  OldCursorPos: Integer;
  dset: TDataSet;
  txt: TCaption;
begin
  if (TextEdit is TcxDBMemo) then
  begin
    if (TcxDBMemo(TextEdit).DataBinding.DataSource = nil) or
       (TcxDBMemo(TextEdit).DataBinding.DataSource.DataSet = nil) or
       (TcxDBMemo(TextEdit).DataBinding.DataField = '') then
    else if (SourceMemo.CaretPos.Y <> -1) and (TcxDBMemo(TextEdit).CaretPos.Y <> -1) then
    begin
      TcxDBMemo(TextEdit).SetFocus;
      if SourceMemo <> mmResponseFields then
        InsertStr := ' ' + STokenBegin + SourceMemo.Lines[SourceMemo.CaretPos.Y] + STokenEnd
      else
        InsertStr := ' ' + STokenBegin + 'Response.' + SourceMemo.Lines[SourceMemo.CaretPos.Y] + STokenEnd;
      OldCursorPos := TcxDBMemo(TextEdit).SelStart;
      TcxDBMemo(TextEdit).Lines[TcxDBMemo(TextEdit).CaretPos.Y] := TcxDBMemo(TextEdit).Lines[TcxDBMemo(TextEdit).CaretPos.Y].Insert(TcxDBMemo(TextEdit).CaretPos.X, InsertStr);
      TcxDBTextEdit(TextEdit).SetSelection(OldCursorPos + InsertStr.Length, 0);
    end;
  end;
  if (TextEdit is TcxDBTextEdit) then
  begin
    if (TcxDBTextEdit(TextEdit).DataBinding.DataSource = nil) or
       (TcxDBTextEdit(TextEdit).DataBinding.DataSource.DataSet = nil) or
       (TcxDBTextEdit(TextEdit).DataBinding.DataField = '') then
    else if (SourceMemo.CaretPos.Y <> -1) then
    begin
      dset := TcxDBTextEdit(TextEdit).DataBinding.DataSource.DataSet;
      if not (dset.State in [dsInsert, dsEdit]) then
        dset.Edit;
      txt := TextEdit.Text;
      OldCursorPos := TcxDBTextEdit(TextEdit).SelStart;
      InsertStr := ' ' + STokenBegin + SourceMemo.Lines[SourceMemo.CaretPos.Y] + STokenEnd;
      Insert(InsertStr, txt, OldCursorPos + 1);
      dset.FieldByName(TcxDBTextEdit(TextEdit).DataBinding.DataField).AsString := txt;
      TcxDBTextEdit(TextEdit).SetFocus;
      TcxDBTextEdit(TextEdit).SetSelection(OldCursorPos + InsertStr.Length, 0);
    end;
  end;
end;

end.
