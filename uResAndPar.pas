unit uResAndPar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.StorageBin, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Stan.StorageJSON, dxSkinsForm,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkroom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinOffice2019Colorful, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringtime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinTheBezier, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, Vcl.ExtCtrls, Vcl.StdCtrls, Spring,
  Spring.Collections;

type
  TfResAndPar = class(TForm)
    mtEndpoint: TFDMemTable;
    mtEndpointendpoint: TStringField;
    mtEndpointremark: TMemoField;
    mtEndpointid: TIntegerField;
    mtParam: TFDMemTable;
    mtParamendpoint_id: TIntegerField;
    mtParamsel: TBooleanField;
    mtParamname: TStringField;
    mtParamdesc: TMemoField;
    mtParamdefault: TStringField;
    tvEndpoint: TcxGridDBTableView;
    grdDefLevel1: TcxGridLevel;
    grdDef: TcxGrid;
    dsEnpoint: TDataSource;
    tvEndpointid: TcxGridDBColumn;
    tvEndpointendpoint: TcxGridDBColumn;
    tvEndpointremark: TcxGridDBColumn;
    dsParam: TDataSource;
    tvParam: TcxGridDBTableView;
    tvParamendpoint_id: TcxGridDBColumn;
    tvParamname: TcxGridDBColumn;
    tvParamdesc: TcxGridDBColumn;
    tvParamdefault: TcxGridDBColumn;
    grdDefLevel2: TcxGridLevel;
    tvParamid: TcxGridDBColumn;
    tvParamsel: TcxGridDBColumn;
    mtParamid: TIntegerField;
    btnUseAll: TButton;
    Button2: TButton;
    Panel1: TPanel;
    btnUseSel: TButton;
    procedure btnUseAllClick(Sender: TObject);
    procedure btnUseSelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure mtParamAfterInsert(DataSet: TDataSet);
  private
    FResource: string;
    FParams: IList<string>;
  public
    class function List(const ACurrent: string; out AResource: string; AParams: IList<string>): Boolean;
  end;

implementation

uses
  System.JSON, System.JSON.Serializers, System.IOUtils, cxGridDBDataDefinitions, uRecords;

const
  RES_AND_PAR_FILE_NAME = 'API_InProg_Resources.json';

{$R *.dfm}


procedure TfResAndPar.btnUseAllClick(Sender: TObject);
var
  mi: Integer;
  i: Integer;
  ADC: TcxGridDBDataController;
begin
  if mtEndpoint.IsEmpty then
    Abort;
  FResource := mtEndpointendpoint.AsString;
  mi := tvEndpoint.DataController.FocusedRecordIndex;
  ADC := tvEndpoint.DataController.GetDetailDataController(mi, 0) as TcxGridDBDataController;
  for i := 0 to ADC.RecordCount - 1 do
    FParams.Add(Format('%s=%s', [VarToStr(ADC.Values[i, tvParamname.Index]), VarToStr(ADC.Values[i, tvParamdefault.Index])]));
  ModalResult := mrOk;
end;

procedure TfResAndPar.btnUseSelClick(Sender: TObject);
var
  mi: Integer;
  i: Integer;
  ADC: TcxGridDBDataController;
begin
  if mtEndpoint.IsEmpty then
    Abort;
  FResource := mtEndpointendpoint.AsString;
  mi := tvEndpoint.DataController.FocusedRecordIndex;
  ADC := tvEndpoint.DataController.GetDetailDataController(mi, 0) as TcxGridDBDataController;
  for i := 0 to ADC.RecordCount - 1 do
    if ADC.Values[i, tvParamsel.Index] then
      FParams.Add(Format('%s=%s', [VarToStr(ADC.Values[i, tvParamname.Index]),
        VarToStr(ADC.Values[i, tvParamdefault.Index])]));
  ModalResult := mrOk;
end;

procedure TfResAndPar.FormClose(Sender: TObject; var Action: TCloseAction);
var
  js: TJSONObject;
begin
  mtEndpoint.MergeChangeLog;
  mtParam.MergeChangeLog;
  js := TJSONObject.Create;
  try
    js.AddPair(TJSONPair.Create('resource', MT2JS(mtEndpoint)));
    js.AddPair(TJSONPair.Create('param', MT2JS(mtParam)));
    TFile.WriteAllText(RES_AND_PAR_FILE_NAME, js.ToString);
  finally
    js.Free;
  end;
end;

procedure TfResAndPar.FormCreate(Sender: TObject);
var
  js: TJSONObject;
begin
  if TFile.Exists(RES_AND_PAR_FILE_NAME) then
    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(RES_AND_PAR_FILE_NAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js.Values['resource'] as TJSONObject, mtEndpoint);
        JS2MT(js.Values['param'] as TJSONObject, mtParam);
      finally
        js.Free;
      end;
    except
      on e: Exception do
        MessageDlg('An error occured while loading "Resources" and/or "Parameters" datasets. Error: '+ e.Message,
                   TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
    end;

  mtParam.IndexFieldNames := 'endpoint_id';
  mtEndpoint.Active := True;
  mtParam.Active := True;
  mtParam.First;
  while not mtParam.Eof do
  begin
    if mtParamid.IsNull then
    begin
      mtParam.Edit;
      mtParamid.AsInteger := mtParam.RecNo;
      mtParam.Post;
    end;
    mtParam.Next;
  end;
end;

procedure TfResAndPar.mtParamAfterInsert(DataSet: TDataSet);
begin
  DataSet['endpoint_id'] := mtEndpoint['id'];
end;

class function TfResAndPar.List(const ACurrent: string; out AResource: string; AParams: IList<string>): Boolean;
begin
  with Create(nil) do
  try
    FParams := AParams;
    Result := ShowModal = mrOk;
    if Result then
      AResource := FResource;
  finally
    Free;
  end;
end;

end.
