unit APIJobAssignments;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges, dxScrollbarAnnotations, Data.DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, cxContainer, cxGroupBox,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls, cxDBExtLookupComboBox, cxButtons,
  cxDropDownEdit, Vcl.Menus, cxCheckBox, System.ImageList, Vcl.ImgList, cxImageList, cxTextEdit, cxMaskEdit, cxSpinEdit,
  cxDBEdit, Vcl.ExtCtrls;

type
  TfrmAPIJobAssignments = class(TForm)
    gbButtonAPIJobAssignments: TcxGroupBox;
    mtFormButtonJobAssignments: TFDMemTable;
    mtFormButtonJobAssignmentsFormClass: TStringField;
    mtFormButtonJobAssignmentsJobID: TGuidField;
    dsFormButtonJobAssignments: TDataSource;
    mtFormButtonJobAssignmentsLJobCaption: TStringField;
    mtFormButtonJobAssignmentsButtonName: TStringField;
    mtFormButtons: TFDMemTable;
    mtFormButtonsCaption: TStringField;
    mtFormButtonsName: TStringField;
    mtFormGrids: TFDMemTable;
    mtFormButtonJobAssignmentsLButtonName: TStringField;
    gbGridAPIJobAssignmentts: TcxGroupBox;
    grdGridAPIJobAssinments: TcxGrid;
    grdGridAPIJobAssinmentsDBTV: TcxGridDBTableView;
    grdGridAPIJobAssinmentsL: TcxGridLevel;
    mtFormGridJobAssignments: TFDMemTable;
    dsFormGridJobAssignments: TDataSource;
    grdGridAPIJobAssinmentsDBTVFormClass: TcxGridDBColumn;
    mtFormGridJobAssignmentsFormClass: TStringField;
    mtFormGridJobAssignmentsGridName: TStringField;
    mtFormGridJobAssignmentsLGridName: TStringField;
    grdGridAPIJobAssinmentsDBTVGridName: TcxGridDBColumn;
    grdGridAPIJobAssinmentsDBTVLGridName: TcxGridDBColumn;
    mtFormGridJobAssignmentsJobGroupID: TStringField;
    mtJobGroups: TFDMemTable;
    mtJobGroupsJobGroupID: TStringField;
    mtFormGridJobAssignmentslJobGroupID: TStringField;
    grdGridAPIJobAssinmentsDBTVJobGroupID: TcxGridDBColumn;
    grdGridAPIJobAssinmentsDBTVlJobGroupID: TcxGridDBColumn;
    mtFormGridsName: TStringField;
    mtFormGridJobAssignmentsAllJobs: TBooleanField;
    grdGridAPIJobAssinmentsDBTVAllJobs: TcxGridDBColumn;
    gbButtons: TcxGroupBox;
    btnReload: TcxButton;
    btnSave: TcxButton;
    mtFormButtonJobAssignmentsJobExecutionType: TStringField;
    mtFormButtonsCCaptionWithName: TStringField;
    mtFormButtonJobAssignmentsNewButtonCaption: TStringField;
    mtFormMenus: TFDMemTable;
    mtFormMenusName: TStringField;
    mtFormMenuJobAssignments: TFDMemTable;
    mtFormMenuJobAssignmentsFormClass: TStringField;
    mtFormMenuJobAssignmentsMenuName: TStringField;
    mtFormMenuJobAssignmentsLMenuName: TStringField;
    mtFormMenuJobAssignmentsJobGroupID: TStringField;
    mtFormMenuJobAssignmentslJobGroupID: TStringField;
    mtFormMenuJobAssignmentsAllJobs: TBooleanField;
    dsFormMenuJobAssignments: TDataSource;
    gbMenuAPIJobAssignmentts: TcxGroupBox;
    grdMenuAPIJobAssinments: TcxGrid;
    grdMenuAPIJobAssinmentsDBTV: TcxGridDBTableView;
    grdMenuAPIJobAssinmentsDBTVFormClass: TcxGridDBColumn;
    grdMenuAPIJobAssinmentsDBTVMenuName: TcxGridDBColumn;
    grdMenuAPIJobAssinmentsDBTVLMenuName: TcxGridDBColumn;
    grdMenuAPIJobAssinmentsDBTVAllJobs: TcxGridDBColumn;
    grdMenuAPIJobAssinmentsDBTVJobGroupID: TcxGridDBColumn;
    grdMenuAPIJobAssinmentsDBTVlJobGroupID: TcxGridDBColumn;
    grdMenuAPIJobAssinmentsL: TcxGridLevel;
    mtFormButtonJobAssignmentsChangeCaption: TBooleanField;
    mtButtonJobAssignmentResponseFields: TFDMemTable;
    mtButtonJobAssignmentResponseFieldsJobID: TGuidField;
    mtButtonJobAssignmentResponseFieldsResponseFieldName: TStringField;
    mtButtonJobAssignmentResponseFieldsShowInResponseGrid: TBooleanField;
    mtButtonJobAssignmentResponseFieldsOrderNo: TSmallintField;
    dsButtonJobAssignmentResponseFields: TDataSource;
    mtButtonJobAssignmentResponseFieldsButtonName: TStringField;
    cxImageList1: TcxImageList;
    mtFormButtonJobAssignmentsResponseGridWidth: TIntegerField;
    mtFormButtonJobAssignmentsResponseGridHeight: TIntegerField;
    mtFormButtonJobAssignmentsSuccessNotificationTitle: TStringField;
    mtFormButtonJobAssignmentsSuccessNotificationHeader: TStringField;
    mtFormButtonJobAssignmentsSuccessNotificationMessage: TStringField;
    mtFormButtonJobAssignmentsErrorNotificationTitle: TStringField;
    mtFormButtonJobAssignmentsErrorNotificationHeader: TStringField;
    mtFormButtonJobAssignmentsErrorNotificationMessage: TStringField;
    gbResponseSettings: TcxGroupBox;
    gbResponseFields: TcxGroupBox;
    grdResponseFields: TcxGrid;
    grdResponseFieldsDBTV: TcxGridDBTableView;
    grdResponseFieldsDBTVResponseFieldName: TcxGridDBColumn;
    grdResponseFieldsDBTVOrderNo: TcxGridDBColumn;
    grdResponseFieldsDBTVShowInResponseGrid: TcxGridDBColumn;
    grdResponseFieldsL: TcxGridLevel;
    gbButtonsOfButtonJobAssignmentResponseFields: TcxGroupBox;
    btnAddAllResponseFieldsForButtonJobAssignment: TcxButton;
    btnDeleteAllResponseFieldsOfButtonJobAssignment: TcxButton;
    gbResponseGridSize: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxDBSpinEdit2: TcxDBSpinEdit;
    pnlButtonAssignments: TPanel;
    cxGroupBox1: TcxGroupBox;
    grdButtonAPIJobAssinments: TcxGrid;
    grdButtonAPIJobAssinmentsDBTV: TcxGridDBTableView;
    grdButtonAPIJobAssinmentsDBTVFormClass: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVJobID: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVLCapitonWithName: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVLJobCaption: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVJobExecutionType: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVChangeCaption: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVNewButtonCaption: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVResponseGridWidth: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVResponseGridHeight: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsL: TcxGridLevel;
    btnNotificationSettings: TcxButton;
    grdButtonAPIJobAssinmentsDBTVSuccessNotificationTitle: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVSuccessNotificationHeader: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVSuccessNotificationMessage: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVErrorNotificationTitle: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVErrorNotificationHeader: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVErrorNotificationMessage: TcxGridDBColumn;
    mtButtonJobAssignmentResponseFieldsResponseGridColumnWidth: TIntegerField;
    grdResponseFieldsDBTVResponseGridColumnWidth: TcxGridDBColumn;
    mtFormButtonJobAssignmentsCustomizeResponseGrid: TBooleanField;
    dcbCustomizeResponseGrid: TcxDBCheckBox;
    mtFormButtonJobAssignmentsCustomizeNotification: TBooleanField;
    grdButtonAPIJobAssinmentsDBTVCustomizeNotification: TcxGridDBColumn;
    grdButtonAPIJobAssinmentsDBTVCustomizeResponseGrid: TcxGridDBColumn;
    mtFormButtonJobAssignmentsSendsWindowsNotification: TBooleanField;
    grdButtonAPIJobAssinmentsDBTVSendsWindowsNotification: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure mtFormButtonJobAssignmentsNewRecord(DataSet: TDataSet);
    procedure mtFormGridJobAssignmentsNewRecord(DataSet: TDataSet);
    procedure mtFormGridJobAssignmentsAllJobsValidate(Sender: TField);
    procedure mtFormGridJobAssignmentsBeforePost(DataSet: TDataSet);
    procedure btnReloadClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure mtFormButtonJobAssignmentsAfterPost(DataSet: TDataSet);
    procedure mtFormButtonJobAssignmentsAfterDelete(DataSet: TDataSet);
    procedure mtFormGridJobAssignmentsAfterPost(DataSet: TDataSet);
    procedure mtFormGridJobAssignmentsAfterDelete(DataSet: TDataSet);
    procedure mtFormButtonJobAssignmentsBeforePost(DataSet: TDataSet);
    procedure mtFormGridJobAssignmentsJobGroupIDValidate(Sender: TField);
    procedure mtFormButtonJobAssignmentsAfterEdit(DataSet: TDataSet);
    procedure mtFormButtonJobAssignmentsAfterInsert(DataSet: TDataSet);
    procedure mtFormButtonJobAssignmentsAfterCancel(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure mtFormButtonsCalcFields(DataSet: TDataSet);
    procedure mtFormMenuJobAssignmentsAfterPost(DataSet: TDataSet);
    procedure mtFormMenuJobAssignmentsAfterDelete(DataSet: TDataSet);
    procedure mtFormMenuJobAssignmentsAllJobsValidate(Sender: TField);
    procedure mtFormMenuJobAssignmentsBeforePost(DataSet: TDataSet);
    procedure mtFormMenuJobAssignmentsJobGroupIDValidate(Sender: TField);
    procedure mtFormMenuJobAssignmentsNewRecord(DataSet: TDataSet);
    procedure mtFormMenuJobAssignmentsAfterEdit(DataSet: TDataSet);
    procedure mtFormMenuJobAssignmentsAfterInsert(DataSet: TDataSet);
    procedure mtFormButtonJobAssignmentsChangeCaptionValidate(Sender: TField);
    procedure mtFormButtonJobAssignmentsJobIDValidate(Sender: TField);
    procedure grdButtonAPIJobAssinmentsDBTVNavigatorButtonsButtonClick(Sender: TObject; AButtonIndex: Integer;
      var ADone: Boolean);
    procedure mtButtonJobAssignmentResponseFieldsAfterCancel(DataSet: TDataSet);
    procedure mtFormGridJobAssignmentsAfterCancel(DataSet: TDataSet);
    procedure mtFormMenuJobAssignmentsAfterCancel(DataSet: TDataSet);
    procedure mtButtonJobAssignmentResponseFieldsAfterDelete(DataSet: TDataSet);
    procedure mtButtonJobAssignmentResponseFieldsAfterInsert(DataSet: TDataSet);
    procedure mtButtonJobAssignmentResponseFieldsAfterEdit(DataSet: TDataSet);
    procedure mtButtonJobAssignmentResponseFieldsAfterPost(DataSet: TDataSet);
    procedure btnAddAllResponseFieldsForButtonJobAssignmentClick(Sender: TObject);
    procedure btnDeleteAllResponseFieldsOfButtonJobAssignmentClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mtFormButtonJobAssignmentsBeforeDelete(DataSet: TDataSet);
    procedure btnNotificationSettingsClick(Sender: TObject);
    procedure mtFormButtonJobAssignmentsCustomizeResponseGridValidate(Sender: TField);
    procedure mtFormButtonJobAssignmentsCustomizeNotificationValidate(Sender: TField);
  private
    procedure LoadAssignments;
    procedure SaveAssigments;
    procedure DatasetAfterCancelSetEnabledofbtnSave;
    procedure AddAllResponseFieldsForButtonJobAssginment;
    procedure DeleteAllResponseFieldsOfButtonJobAssignment;
    { Private declarations }
  public
    { Public declarations }
    frmAPIInProgSettings: TForm;
  end;

var
  frmAPIJobAssignments: TfrmAPIJobAssignments;

implementation

{$R *.dfm}

uses System.JSON, System.StrUtils, System.IOUtils, uRecords, uAPIInProg, vcl.Grids, uAPIInProgSettings,
uAPIJobNotificationSettings;

procedure TfrmAPIJobAssignments.btnAddAllResponseFieldsForButtonJobAssignmentClick(Sender: TObject);
begin
  AddAllResponseFieldsForButtonJobAssginment;
end;

procedure TfrmAPIJobAssignments.btnDeleteAllResponseFieldsOfButtonJobAssignmentClick(Sender: TObject);
begin
  DeleteAllResponseFieldsOfButtonJobAssignment;
end;

procedure TfrmAPIJobAssignments.btnNotificationSettingsClick(Sender: TObject);
begin
  frmAPIJobNotificationSettings := TfrmAPIJobNotificationSettings.Create(self);
  try
    frmAPIJobNotificationSettings.Caption := 'Notification Settings of Button Job Assignment';
    frmAPIJobNotificationSettings.dsNotificationSettings.DataSet := mtFormButtonJobAssignments;
    frmAPIJobNotificationSettings.JobID := mtFormButtonJobAssignmentsJobID.AsGuid;
    frmAPIJobNotificationSettings.SourceForm := TfrmAPIInProgSettings(frmAPIInProgSettings).RequestForm;
    frmAPIJobNotificationSettings.dcbCustomizeNotification.Visible := True;
    frmAPIJobNotificationSettings.dcbCustomizeNotification.DataBinding.DataSource := frmAPIJobNotificationSettings.dsNotificationSettings;
    frmAPIJobNotificationSettings.ShowModal;
  finally
    frmAPIJobNotificationSettings.Free;
  end;
end;

procedure TfrmAPIJobAssignments.btnReloadClick(Sender: TObject);
begin
  LoadAssignments;
end;

procedure TfrmAPIJobAssignments.btnSaveClick(Sender: TObject);
begin
  SaveAssigments;
end;

procedure TfrmAPIJobAssignments.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (dmAPIInProg <> nil) and
     (not dmAPIInProg.mtFormButtonJobAssignments.Active or
      not dmAPIInProg.mtButtonJobAssignmentResponseFields.Active or
      not dmAPIInProg.mtFormGridJobAssignments.Active or
      not dmAPIInProg.mtFormMenuJobAssignments.Active) then
    dmAPIInProg.LoadAssignments;
end;

procedure TfrmAPIJobAssignments.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if (btnSave.Enabled or
      (mtFormButtonJobAssignments.State in [dsInsert, dsEdit]) or
      (mtFormGridJobAssignments.State in [dsInsert, dsEdit]) or
      (mtFormMenuJobAssignments.State in [dsInsert, dsEdit])) and
  (MessageBox(0, PChar('Save changes ?'), PChar('Close'), MB_ICONWARNING or MB_YESNO) = ID_YES) then
  begin
    if (mtFormButtonJobAssignments.State in [dsInsert, dsEdit])  then
      mtFormButtonJobAssignments.Post;
    if (mtFormGridJobAssignments.State in [dsInsert, dsEdit])  then
      mtFormGridJobAssignments.Post;
    if (mtFormMenuJobAssignments.State in [dsInsert, dsEdit])  then
      mtFormMenuJobAssignments.Post;
    btnSave.Click;
    CanClose := CanClose and not btnSave.Enabled;
    if CanClose then
      Close;
  end;
end;

procedure TfrmAPIJobAssignments.FormCreate(Sender: TObject);
var
  i: Integer;
  RequestForm: TForm;
begin
  inherited;

  if self.Owner = nil then
    raise Exception.Create(Format('Form of %s must be created with owner!', [Self.caption ]));

  if not dmAPIInProg.mtAPIJobs.Active then
    dmAPIInProg.LoadAPIJobs;

  mtFormButtons.Active := True;
  mtFormGrids.Active := True;
  mtFormMenus.Active := True;

  RequestForm := TfrmAPIInProgSettings(self.Owner).RequestForm;
  if RequestForm <> nil then
  begin
    for i := 0 to RequestForm.ComponentCount - 1 do
    begin
      if RequestForm.Components[i] is TCustomButton then
      begin
        mtFormButtons.Insert;
        mtFormButtonsName.AsString := RequestForm.Components[i].Name;
        // to access caption TButton is used
        mtFormButtonsCaption.AsString := TButton(RequestForm.Components[i]).Caption;
        mtFormButtons.Post;
      end;

      if ((RequestForm.Components[i] is TCustomGrid) or (RequestForm.Components[i] is TcxCustomGrid)) and
         (RequestForm.Components[i].Name <> 'grdAPIStatus')
      then
      begin
        mtFormGrids.Insert;
        mtFormGridsName.AsString := RequestForm.Components[i].Name;
        mtFormGrids.Post;
      end;

      if (RequestForm.Components[i] is TPopupMenu) and
         (RequestForm.Components[i].Name <> 'pumRestAPIJobs') and
         (RequestForm.Components[i].Name <> 'pumRestAPIStatus') and
         (RightStr(RequestForm.Components[i].Name, Length('_pumRestAPIJobs')) <> '_pumRestAPIJobs')  then
      begin
        mtFormMenus.Insert;
        mtFormMenusName.AsString := TPopupMenu(RequestForm.Components[i]).Name;
        mtFormMenus.Post;
      end;
    end;

    mtJobGroups.Active := True;

    TfrmAPIInProgSettings(self.Owner).mtAPIJobs.First;
    while not TfrmAPIInProgSettings(self.Owner).mtAPIJobs.Eof do
    begin
      if not mtJobGroups.Locate('JobGroupID', TfrmAPIInProgSettings(self.Owner).mtAPIJobsGroupID.AsString.Trim, [loCaseInsensitive]) then
      begin
        mtJobGroups.Insert;
        mtJobGroupsJobGroupID.AsString := TfrmAPIInProgSettings(self.Owner).mtAPIJobsGroupID.AsString.Trim;
        mtJobGroups.Post;
      end;
      TfrmAPIInProgSettings(self.Owner).mtAPIJobs.Next;
    end;
  end;

  LoadAssignments;
end;

procedure TfrmAPIJobAssignments.grdButtonAPIJobAssinmentsDBTVNavigatorButtonsButtonClick(Sender: TObject;
  AButtonIndex: Integer; var ADone: Boolean);
begin

  if (AButtonIndex - grdButtonAPIJobAssinmentsDBTV.NavigatorButtons.ButtonCount +
      grdButtonAPIJobAssinmentsDBTV.NavigatorButtons.CustomButtons.Count) =
     grdButtonAPIJobAssinmentsDBTV.NavigatorButtons.CustomButtons[0].Index then
  begin
    AddAllResponseFieldsForButtonJobAssginment;
  end
  else
  if (AButtonIndex - grdButtonAPIJobAssinmentsDBTV.NavigatorButtons.ButtonCount +
      grdButtonAPIJobAssinmentsDBTV.NavigatorButtons.CustomButtons.Count) =
     grdButtonAPIJobAssinmentsDBTV.NavigatorButtons.CustomButtons[1].Index then
  begin
    DeleteAllResponseFieldsOfButtonJobAssignment;
  end;
end;

procedure TfrmAPIJobAssignments.mtButtonJobAssignmentResponseFieldsAfterCancel(DataSet: TDataSet);
begin
  DatasetAfterCancelSetEnabledofbtnSave;
end;

procedure TfrmAPIJobAssignments.mtButtonJobAssignmentResponseFieldsAfterDelete(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtButtonJobAssignmentResponseFieldsAfterEdit(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtButtonJobAssignmentResponseFieldsAfterInsert(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtButtonJobAssignmentResponseFieldsAfterPost(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsAfterCancel(DataSet: TDataSet);
begin
  DatasetAfterCancelSetEnabledofbtnSave;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsAfterDelete(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsAfterEdit(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsAfterInsert(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsAfterPost(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsBeforeDelete(DataSet: TDataSet);
begin
  if mtButtonJobAssignmentResponseFields.RecordCount > 0 then
    raise Exception.Create('Response field record exists for the button job assignment. Button job assignment can not be deleted!');
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsBeforePost(DataSet: TDataSet);
begin
  if mtFormButtonJobAssignmentsFormClass.AsString = '' then
    raise Exception.Create('Form class of button job assignment can not be empty!');
  if mtFormButtonJobAssignmentsButtonName.AsString = '' then
    raise Exception.Create('Button name of button job assignment can not be empty!');
  if mtFormButtonJobAssignmentsJobID.AsString = '' then
    raise Exception.Create('Job of button job assignment can not be empty!');
  if mtFormButtonJobAssignmentsChangeCaption.AsBoolean and (mtFormButtonJobAssignmentsNewButtonCaption.AsString = '') then
    raise Exception.Create('While Change Caption is checked New Button Caption must be entered!');

  if not dmAPIInProg.mtAPIJobs.Locate('ID', mtFormButtonJobAssignmentsJobID.AsVariant, []) then
    raise Exception.Create('Job could not be found in the list of stored jobs');

  if mtFormButtonJobAssignmentsSendsWindowsNotification.AsBoolean and mtFormButtonJobAssignmentsCustomizeNotification.AsBoolean then
  begin
    if mtFormButtonJobAssignmentsSuccessNotificationTitle.AsString = '' then
      raise Exception.Create('While "Sends windows notification" is selected, "Success Notification Title" can not be empty!'#13#10+
        'You can show notification message settings form by clicking "Notification Message Settings" button.');
    if mtFormButtonJobAssignmentsSuccessNotificationMessage.AsString = '' then
      raise Exception.Create('While "Sends windows notification" is selected, "Success Notification Message" can not be empty!'#13#10+
        'You can show notification message settings form by clicking "Notification Message Settings" button.');

    if mtFormButtonJobAssignmentsErrorNotificationTitle.AsString = '' then
      raise Exception.Create('While "Sends windows notification" is selected, "Error Notification Title" can not be empty!'#13#10+
        'You can show notification message settings form by clicking "Notification Message Settings" button.');
//    if mtFormButtonJobAssignmentsErrorNotificationMessage.AsString = '' then
//      raise Exception.Create('While "Sends windows notification" is selected, "Error Notification Message" can not be empty!'#13#10+
//        'You can show notification message settings form by clicking "Notification Message Settings" button.');
  end;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsChangeCaptionValidate(Sender: TField);
begin
  if mtFormButtonJobAssignmentsChangeCaption.AsBoolean and (mtFormButtonJobAssignmentsNewButtonCaption.AsString = '') then
    mtFormButtonJobAssignmentsNewButtonCaption.AsString := mtFormButtonJobAssignmentsLJobCaption.AsString;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsCustomizeNotificationValidate(Sender: TField);
begin
  if mtFormButtonJobAssignmentsCustomizeNotification.AsBoolean then
  begin
    if (mtFormButtonJobAssignmentsSuccessNotificationTitle.AsString = '') and
       (mtFormButtonJobAssignmentsSuccessNotificationHeader.AsString = '') and
       (mtFormButtonJobAssignmentsSuccessNotificationMessage.AsString = '') then
    begin
      if not dmAPIInProg.mtAPIJobs.Locate('ID', mtFormButtonJobAssignmentsJobID.AsVariant, []) then
        raise Exception.Create('Job could not be found in the list of stored jobs');
       mtFormButtonJobAssignmentsSuccessNotificationTitle.AsString := dmAPIInProg.mtAPIJobsSuccessNotificationTitle.AsString;
       mtFormButtonJobAssignmentsSuccessNotificationHeader.AsString := dmAPIInProg.mtAPIJobsSuccessNotificationHeader.AsString;
       mtFormButtonJobAssignmentsSuccessNotificationMessage.AsString := dmAPIInProg.mtAPIJobsSuccessNotificationMessage.AsString;
    end;
    if (mtFormButtonJobAssignmentsErrorNotificationTitle.AsString = '') and
       (mtFormButtonJobAssignmentsErrorNotificationHeader.AsString = '') and
       (mtFormButtonJobAssignmentsErrorNotificationMessage.AsString = '') then
    begin
      if not dmAPIInProg.mtAPIJobs.Locate('ID', mtFormButtonJobAssignmentsJobID.AsVariant, []) then
        raise Exception.Create('Job could not be found in the list of stored jobs');
       mtFormButtonJobAssignmentsErrorNotificationTitle.AsString := dmAPIInProg.mtAPIJobsErrorNotificationTitle.AsString;
       mtFormButtonJobAssignmentsErrorNotificationHeader.AsString := dmAPIInProg.mtAPIJobsErrorNotificationHeader.AsString;
       mtFormButtonJobAssignmentsErrorNotificationMessage.AsString := dmAPIInProg.mtAPIJobsErrorNotificationMessage.AsString;
    end;
  end;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsCustomizeResponseGridValidate(Sender: TField);
begin
  if mtFormButtonJobAssignmentsCustomizeResponseGrid.AsBoolean then
  begin
    if (mtFormButtonJobAssignmentsResponseGridWidth.AsInteger = 0) and
       (mtFormButtonJobAssignmentsResponseGridHeight.AsInteger = 0) then
    begin
      if not dmAPIInProg.mtAPIJobs.Locate('ID', mtFormButtonJobAssignmentsJobID.AsVariant, []) then
        raise Exception.Create('Job could not be found in the list of stored jobs');

      mtFormButtonJobAssignmentsResponseGridWidth.AsInteger := dmAPIInProg.mtAPIJobsResponseGridWidth.AsInteger;
      mtFormButtonJobAssignmentsResponseGridHeight.AsInteger := dmAPIInProg.mtAPIJobsResponseGridHeight.AsInteger;
    end;

    AddAllResponseFieldsForButtonJobAssginment;
  end;
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsJobIDValidate(Sender: TField);
begin
  if mtFormButtonJobAssignmentsChangeCaption.AsBoolean then
    if dmAPIInProg.mtAPIJobs.Locate('ID', mtFormButtonJobAssignmentsJobID.AsVariant, []) then
    begin
      mtFormButtonJobAssignmentsNewButtonCaption.AsString := dmAPIInProg.mtAPIJobsCaption.AsString;
      mtFormButtonJobAssignmentsSendsWindowsNotification.AsBoolean := dmAPIInProg.mtAPIJobsSendsWindowsNotification.AsBoolean;
    end
    else
      Raise Exception.Create('Job is not found in the list of stored jobs!');
end;

procedure TfrmAPIJobAssignments.mtFormButtonJobAssignmentsNewRecord(DataSet: TDataSet);
begin
  if (Self.Owner <> nil) and (TfrmAPIInProgSettings(Self.Owner).RequestForm <> nil) then
    mtFormButtonJobAssignmentsFormClass.AsString := TfrmAPIInProgSettings(Self.Owner).RequestForm.ClassName;
  mtFormButtonJobAssignmentsChangeCaption.AsBoolean := False;
  mtFormButtonJobAssignmentsSendsWindowsNotification.AsBoolean := True;
  mtFormButtonJobAssignmentsJobExecutionType.AsString := 'Not Run OnClick';
end;

procedure TfrmAPIJobAssignments.mtFormButtonsCalcFields(DataSet: TDataSet);
begin
  mtFormButtonsCCaptionWithName.asstring := mtFormButtonsCaption.asString + ' (' + mtFormButtonsName.AsString +')';
end;

procedure TfrmAPIJobAssignments.mtFormGridJobAssignmentsAfterCancel(DataSet: TDataSet);
begin
  DatasetAfterCancelSetEnabledofbtnSave;
end;

procedure TfrmAPIJobAssignments.mtFormGridJobAssignmentsAfterDelete(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormGridJobAssignmentsAfterPost(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormGridJobAssignmentsAllJobsValidate(Sender: TField);
begin
  if mtFormGridJobAssignmentsAllJobs.AsBoolean and not mtFormGridJobAssignmentsJobGroupID.IsNull then
    mtFormGridJobAssignmentsJobGroupID.AsVariant := null;
end;

procedure TfrmAPIJobAssignments.mtFormGridJobAssignmentsBeforePost(DataSet: TDataSet);
begin
  if mtFormGridJobAssignmentsFormClass.AsString = '' then
    raise Exception.Create('Form class of grid API job assignment can not be empty!');
  if mtFormGridJobAssignmentsGridName.AsString = '' then
    raise Exception.Create('Grid name of grid API job assignment can not be empty!');
  if not mtFormGridJobAssignmentsAllJobs.AsBoolean and (mtFormGridJobAssignmentsJobGroupID.AsString = '') then
    raise Exception.Create('Either all jobs must be checked or a job group ID must be selected for the grid!');
end;

procedure TfrmAPIJobAssignments.mtFormGridJobAssignmentsJobGroupIDValidate(Sender: TField);
begin
 if mtFormGridJobAssignmentsAllJobs.AsBoolean and (mtFormGridJobAssignmentsJobGroupID.AsString <> '') then
   raise Exception.Create('While All Jobs is selected, Job group ID must be empty!');
end;

procedure TfrmAPIJobAssignments.mtFormGridJobAssignmentsNewRecord(DataSet: TDataSet);
begin
  if (Self.Owner <> nil) and (TfrmAPIInProgSettings(Self.Owner).RequestForm <> nil) then
    mtFormGridJobAssignmentsFormClass.AsString := TfrmAPIInProgSettings(Self.Owner).RequestForm.ClassName;
  mtFormGridJobAssignmentsAllJobs.AsBoolean := False;
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsAfterCancel(DataSet: TDataSet);
begin
  DatasetAfterCancelSetEnabledofbtnSave;
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsAfterDelete(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsAfterEdit(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsAfterInsert(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsAfterPost(DataSet: TDataSet);
begin
  btnSave.Enabled := True;
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsAllJobsValidate(Sender: TField);
begin
  if mtFormMenuJobAssignmentsAllJobs.AsBoolean and not mtFormMenuJobAssignmentsJobGroupID.IsNull then
    mtFormMenuJobAssignmentsJobGroupID.AsVariant := null;
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsBeforePost(DataSet: TDataSet);
begin
  if mtFormMenuJobAssignmentsFormClass.AsString = '' then
    raise Exception.Create('Form class of menu API job assignment can not be empty!');
  if mtFormMenuJobAssignmentsMenuName.AsString = '' then
    raise Exception.Create('Menu name of menu API job assignment can not be empty!');
  if not mtFormMenuJobAssignmentsAllJobs.AsBoolean and (mtFormMenuJobAssignmentsJobGroupID.AsString = '') then
    raise Exception.Create('Either all jobs must be checked or a job group ID must be selected for the popup menu!');
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsJobGroupIDValidate(Sender: TField);
begin
 if mtFormMenuJobAssignmentsAllJobs.AsBoolean and (mtFormMenuJobAssignmentsJobGroupID.AsString <> '') then
   raise Exception.Create('While All Jobs is selected, Job group ID must be empty!');
end;

procedure TfrmAPIJobAssignments.mtFormMenuJobAssignmentsNewRecord(DataSet: TDataSet);
begin
  if (Self.Owner <> nil) and (TfrmAPIInProgSettings(Self.Owner).RequestForm <> nil) then
    mtFormMenuJobAssignmentsFormClass.AsString := TfrmAPIInProgSettings(Self.Owner).RequestForm.ClassName;
  mtFormMenuJobAssignmentsAllJobs.AsBoolean := False;
end;

procedure TfrmAPIJobAssignments.LoadAssignments;
var
  SavedName: string;
  js: TJSONObject;
begin
  if TFile.Exists(BUTTON_JOB_ASSIGNMENTS_FILENAME) then
  begin
    if mtFormButtonJobAssignments.RecordCount > 0 then
      SavedName := mtFormButtonJobAssignmentsButtonName.AsString
    else
      SavedName := '';

    mtFormButtonJobAssignments.Filtered := False;

    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(BUTTON_JOB_ASSIGNMENTS_FILENAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtFormButtonJobAssignments);
      finally
        js.Free;
      end;
    except
      on e1: Exception do
        MessageDlg('An error occured while loading "Button Job Assignments" dataset. Error: '+ e1.Message,
                   TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
    end;

    //mtFormButtonJobAssignments.CloneCursor(dmAPIInProg.mtFormButtonJobAssignments, False, True);

    if (Self.Owner <> nil) and (TfrmAPIInProgSettings(Self.Owner).RequestForm <> nil) then
      mtFormButtonJobAssignments.Filter := 'FormClass = ' + QuotedStr(TfrmAPIInProgSettings(Self.Owner).RequestForm.ClassName)
    else
      mtFormButtonJobAssignments.Filter := '1=0';
    mtFormButtonJobAssignments.Filtered := mtFormButtonJobAssignments.Filter <> '';

    if (SavedName <> '') and mtFormButtonJobAssignments.Active then
      mtFormButtonJobAssignments.Locate('ButtonName', SavedName, []);
  end;

  if not mtFormButtonJobAssignments.Active  then
    mtFormButtonJobAssignments.Active := True;

  if TFile.Exists(BUTTON_JOB_ASSIGNMENTS_RESPONSE_FIELDS_FILENAME) then
  begin
    if mtButtonJobAssignmentResponseFields.RecordCount > 0 then
      SavedName := mtButtonJobAssignmentResponseFieldsResponseFieldName.AsString
    else
      SavedName := '';

    mtButtonJobAssignmentResponseFields.Filtered := False;

    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(BUTTON_JOB_ASSIGNMENTS_RESPONSE_FIELDS_FILENAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtButtonJobAssignmentResponseFields);
      finally
        js.Free;
      end;
    except
      on e2: Exception do
        MessageDlg('An error occured while loading "Response Fields of Button Job Assignments" dataset. Error: '+ e2.Message,
                   TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
    end;
    //mtButtonJobAssignmentResponseFields.CloneCursor(dmAPIInProg.mtButtonJobAssignmentResponseFields, False, True);

    if (SavedName <> '') and mtButtonJobAssignmentResponseFields.Active then
      mtButtonJobAssignmentResponseFields.Locate('ResponseFieldName', SavedName, []);
  end;

  if not mtButtonJobAssignmentResponseFields.Active  then
    mtButtonJobAssignmentResponseFields.Active := True;

  if TFile.Exists(GRID_JOB_ASSIGNMENTS_FILENAME) then
  begin
    if mtFormGridJobAssignments.RecordCount > 0 then
      SavedName := mtFormGridJobAssignmentsGridName.AsString
    else
      SavedName := '';

    mtFormGridJobAssignments.Filtered := False;

    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(GRID_JOB_ASSIGNMENTS_FILENAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtFormGridJobAssignments);
      finally
        js.Free;
      end;
    except
      on e3: Exception do
        MessageDlg('An error occured while loading "Grid Job Assignments" dataset. Error: '+ e3.Message,
                   TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
    end;

    if (Self.Owner <> nil) and (TfrmAPIInProgSettings(Self.Owner).RequestForm <> nil) then
      mtFormGridJobAssignments.Filter := 'FormClass = ' + QuotedStr(TfrmAPIInProgSettings(Self.Owner).RequestForm.ClassName)
    else
      mtFormGridJobAssignments.Filter := '1=0';
    mtFormGridJobAssignments.Filtered := mtFormGridJobAssignments.Filter <> '';

    if (SavedName <> '') and mtFormGridJobAssignments.Active then
      mtFormGridJobAssignments.Locate('GridName', SavedName, []);
  end;

  if not mtFormGridJobAssignments.Active  then
    mtFormGridJobAssignments.Active := True;

  if TFile.Exists(MENU_JOB_ASSIGNMENTS_FILENAME) then
  begin
    if mtFormMenuJobAssignments.RecordCount > 0 then
      SavedName := mtFormMenuJobAssignmentsMenuName.AsString
    else
      SavedName := '';

    mtFormMenuJobAssignments.Filtered := False;

    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(MENU_JOB_ASSIGNMENTS_FILENAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtFormMenuJobAssignments);
      finally
        js.Free;
      end;
    except
      on e4: Exception do
        MessageDlg('An error occured while loading "Menu Job Assignments" dataset. Error: '+ e4.Message,
                   TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
    end;

    if (Self.Owner <> nil) and (TfrmAPIInProgSettings(Self.Owner).RequestForm <> nil) then
      mtFormMenuJobAssignments.Filter := 'FormClass = ' + QuotedStr(TfrmAPIInProgSettings(Self.Owner).RequestForm.ClassName)
    else
      mtFormMenuJobAssignments.Filter := '1=0';
    mtFormMenuJobAssignments.Filtered := mtFormMenuJobAssignments.Filter <> '';

    if (SavedName <> '') and mtFormMenuJobAssignments.Active then
      mtFormMenuJobAssignments.Locate('MenuName', SavedName, []);
  end;

  if not mtFormMenuJobAssignments.Active  then
    mtFormMenuJobAssignments.Active := True;

  btnSave.Enabled := False;
end;

procedure TfrmAPIJobAssignments.SaveAssigments;
var
  js: TJSONObject;
begin
  if mtFormButtonJobAssignments.State in dsEditModes then
    mtFormButtonJobAssignments.Post;

  if mtButtonJobAssignmentResponseFields.State in dsEditModes then
    mtButtonJobAssignmentResponseFields.Post;

  if mtFormGridJobAssignments.State in dsEditModes then
    mtFormGridJobAssignments.Post;

  if mtFormMenuJobAssignments.State in dsEditModes then
    mtFormMenuJobAssignments.Post;

  js := MT2JS(mtFormButtonJobAssignments);
  try
    TFile.WriteAllText(BUTTON_JOB_ASSIGNMENTS_FILENAME, js.ToString);
  finally
    js.Free;
  end;

  if mtFormButtonJobAssignments.UpdatesPending then
    mtFormButtonJobAssignments.CommitUpdates;

  if (dmAPIInProg <> nil) and dmAPIInProg.mtFormButtonJobAssignments.Active then
    dmAPIInProg.mtFormButtonJobAssignments.Active := False;

  js := MT2JS(mtButtonJobAssignmentResponseFields);
  try
    TFile.WriteAllText(BUTTON_JOB_ASSIGNMENTS_RESPONSE_FIELDS_FILENAME, js.ToString);
  finally
    js.Free;
  end;

  if mtButtonJobAssignmentResponseFields.UpdatesPending then
    mtButtonJobAssignmentResponseFields.CommitUpdates;

  if (dmAPIInProg <> nil) and dmAPIInProg.mtButtonJobAssignmentResponseFields.Active then
    dmAPIInProg.mtButtonJobAssignmentResponseFields.Active := False;

  js := MT2JS(mtFormGridJobAssignments);
  try
    TFile.WriteAllText(GRID_JOB_ASSIGNMENTS_FILENAME, js.ToString);
  finally
    js.Free;
  end;

  if mtFormGridJobAssignments.UpdatesPending then
    mtFormGridJobAssignments.CommitUpdates;

  if (dmAPIInProg <> nil) and dmAPIInProg.mtFormGridJobAssignments.Active then
    dmAPIInProg.mtFormGridJobAssignments.Active := False;

  js := MT2JS(mtFormMenuJobAssignments);
  try
    TFile.WriteAllText(MENU_JOB_ASSIGNMENTS_FILENAME, js.ToString);
  finally
    js.Free;
  end;

  if mtFormMenuJobAssignments.UpdatesPending then
    mtFormMenuJobAssignments.CommitUpdates;

  if (dmAPIInProg <> nil) and dmAPIInProg.mtFormMenuJobAssignments.Active then
    dmAPIInProg.mtFormMenuJobAssignments.Active := False;

  if (dmAPIInProg <> nil) then
    dmAPIInProg.LoadAssignments;

  btnSave.Enabled := False;
end;

procedure TfrmAPIJobAssignments.DatasetAfterCancelSetEnabledofbtnSave;
begin
  btnSave.Enabled := mtFormButtonJobAssignments.UpdatesPending or mtButtonJobAssignmentResponseFields.UpdatesPending or mtFormGridJobAssignments.UpdatesPending or mtFormMenuJobAssignments.UpdatesPending;
end;

procedure TfrmAPIJobAssignments.AddAllResponseFieldsForButtonJobAssginment;
begin
  if not dmAPIInProg.mtAPIJobs.Locate('ID', mtFormButtonJobAssignmentsJobID.AsVariant, []) then
    raise Exception.Create('Job could not be found in the list of stored jobs');
  dmAPIInProg.mtAPIJobResponseFields.First;
  while not dmAPIInProg.mtAPIJobResponseFields.Eof do
  begin
    if not mtButtonJobAssignmentResponseFields.Locate('ResponseFieldName', dmAPIInProg.mtAPIJobResponseFieldsResponseFieldName.AsString, []) then
    begin
      mtButtonJobAssignmentResponseFields.Insert;
      mtButtonJobAssignmentResponseFieldsButtonName.AsString := mtFormButtonJobAssignmentsButtonName.AsString;
      mtButtonJobAssignmentResponseFieldsJobID.AsGuid := dmAPIInProg.mtAPIJobResponseFieldsJobID.AsGuid;
      mtButtonJobAssignmentResponseFieldsResponseFieldName.AsString := dmAPIInProg.mtAPIJobResponseFieldsResponseFieldName.AsString;
      mtButtonJobAssignmentResponseFieldsOrderNo.AsInteger := dmAPIInProg.mtAPIJobResponseFieldsOrderNo.AsInteger;
      mtButtonJobAssignmentResponseFieldsShowInResponseGrid.AsBoolean := dmAPIInProg.mtAPIJobResponseFieldsShowInResponseGrid.AsBoolean;
      mtButtonJobAssignmentResponseFields.Post;
    end;
    dmAPIInProg.mtAPIJobResponseFields.Next;
  end;
end;

procedure TfrmAPIJobAssignments.DeleteAllResponseFieldsOfButtonJobAssignment;
begin
  if mtFormButtonJobAssignmentsCustomizeResponseGrid.AsBoolean then
    raise Exception.Create('While "Customize Response Grid" is selected, response fields of button job assignment can not be deleted!');

  while not mtButtonJobAssignmentResponseFields.eof do
    mtButtonJobAssignmentResponseFields.delete;
end;

end.
