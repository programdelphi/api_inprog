program API_coder;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {Form1},
  uGlobal in 'uGlobal.pas',
  uFrm2 in 'uFrm2.pas' {Form2},
  uAPIInProg in 'uAPIInProg.pas' {dmAPIInProg: TDataModule},
  uAPIInProgSettings in 'uAPIInProgSettings.pas' {frmAPIInProgSettings},
  APIJobAssignments in 'APIJobAssignments.pas' {frmAPIJobAssignments},
  uAPIJobNotificationSettings in 'uAPIJobNotificationSettings.pas' {frmAPIJobNotificationSettings};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TfrmAPIJobNotificationSettings, frmAPIJobNotificationSettings);
  Application.Run;
end.
