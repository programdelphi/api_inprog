unit uDataManager;

interface

uses
  uApiConnection,
  uRecords;

type
  TDataManager = class
  private
    FRESTApi: TRESTApiConnectionManager;
  public
    function RunApi(var LCurrRestCall: TRestCall): Boolean;

    constructor Create;
    destructor Destroy; override;
  end;

function EncodeUrl(const ASource: string): string;
function DecodeUrl(const ASource: string): string;

implementation

uses
  REST.Client,
  REST.Types,
  System.JSON,
  System.Classes,
  System.SysUtils,
  uExceptions,
  uRestAPIQueueeThread, System.NetEncoding;


{ TDataManager }

constructor TDataManager.Create;
begin
  inherited;
  FRESTApi := TRESTApiConnectionManager.Create;
end;

destructor TDataManager.Destroy;
begin
  FRESTApi.Free;
  inherited;
end;

(*
json response:
{"result": [{"Status": "Success"}]}
{"Status": "Success"}
*)
function TDataManager.RunApi(var LCurrRestCall: TRestCall): Boolean;
var
  LRequest: TRESTRequest;
  LResponse: TRESTResponse;
  Status: TJSONValue;
  Error: TJSONValue;
begin
  Result := False;
  FRESTApi.ConfigureRESTComponents(LCurrRestCall, rmGET, LRequest, LResponse);
  try
    try
      LRequest.Execute;
      LCurrRestCall.ResponseText := LResponse.JSONText;
      if LResponse.Status.SuccessOK_200 then
      begin
        if Assigned(LResponse.JSONValue) then
        begin
          Status := (LResponse.JSONValue as TJSONObject).FindValue('Status');
          if not Assigned(Status) then
            Status := (LResponse.JSONValue as TJSONObject).FindValue('result[0].Status');

          if Assigned(Status) then
          begin
            if SameText(Status.Value, STATUS_ERROR) then
            begin
              Error := (LResponse.JSONValue as TJSONObject).FindValue('Error');
              if not Assigned(Error) then
                Error := (LResponse.JSONValue as TJSONObject).FindValue('result[0].Error');
              if Assigned(Error) then
                raise TApiException.Create(Error.Value)
              else
                raise TApiException.Create('???');
            end
            else if SameText(Status.Value, STATUS_SUCCESS) then
              Result := True
            else
              Result := True;
          end;
        end
        else
          Result := True; // need to play with each result depending on call
      end;
    except
      raise;
    end;
  finally
    LRequest.Free;
    LResponse.Free;
  end;
end;


function EncodeUrl(const ASource: string): string;
//var
//  I: integer;
//  B: TStringBuilder;
begin
//  result := '';
//  B := TStringBuilder.Create;
//  try
//    for I := 1 to length(ASource) do
//      if CharInSet(ASource[I], ['A' .. 'Z', 'a' .. 'z', '0', '1' .. '9', '-', '_', '~', '.']) then
//        B.Append(ASource[I])
//      else
//        B.Append('%' + IntToHex(Ord(ASource[I]), 2));
//    Result := B.ToString;
//  finally
//    B.Free;
//  end;
  Result := TNetEncoding.URL.Encode(ASource);
end;

function DecodeUrl(const ASource: string): string;
begin
  Result := TNetEncoding.URL.Decode(ASource, []);
end;

end.
