unit uAddFields;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxCheckBox, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxGroupBox, cxCustomListBox, cxCheckListBox,
  System.Types, Spring, Spring.Collections, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  dxDateRanges, dxScrollbarAnnotations, cxTextEdit, cxGridCustomTableView, cxGridTableView, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid;

type
  TfAddFields = class(TForm)
    cxGroupBox1: TcxGroupBox;
    btnUnselect: TcxButton;
    btnSelect: TcxButton;
    btnReverse: TcxButton;
    cxGroupBox2: TcxGroupBox;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    grdFieldsLevel1: TcxGridLevel;
    grdFields: TcxGrid;
    viewFields: TcxGridTableView;
    colCheck: TcxGridColumn;
    colName: TcxGridColumn;
    colParamName: TcxGridColumn;
    procedure btnSelectClick(Sender: TObject);
    procedure btnUnselectClick(Sender: TObject);
    procedure btnReverseClick(Sender: TObject);
    procedure colCheckPropertiesValidate(Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure colParamNamePropertiesValidate(Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
  private
    procedure CheckDuplicateParamName(const ParamName: string);
  public
    class function Run(AFieldsChecks: IList<Tuple<string, string, Boolean>>): Boolean;
  end;

implementation

{$R *.dfm}

{ TfAddFields }

procedure TfAddFields.btnReverseClick(Sender: TObject);
var
  i, j: Integer;
  ParamName, RecParamName, FieldName: string;
  SameParamNameAlreadySelected: Boolean;
begin
  for i := 0 to viewFields.DataController.RecordCount - 1 do
  begin
    if not viewFields.DataController.Values[i, colCheck.Index] then
    begin
      ParamName := viewFields.DataController.Values[i, colParamName.Index];
      FieldName := viewFields.DataController.Values[i, colName.Index];
      SameParamNameAlreadySelected := False;
      for j := 0 to i - 1 do
      begin
        if (j <> i) and
           (viewFields.DataController.Values[j, colCheck.Index] = True) then
        begin
          RecParamName := viewFields.DataController.Values[j, colParamName.Index];          
          if ParamName = RecParamName then
          begin
            SameParamNameAlreadySelected := True;
            break;
          end;
        end;
      end;
      if SameParamNameAlreadySelected then
        ShowMessage(Format('%s parameter is selected before. It can not be selected again for the %s!', 
                    [ParamName, FieldName]))
      else
        viewFields.DataController.Values[i, colCheck.Index] := not viewFields.DataController.Values[i, colCheck.Index];
    end
    else
      viewFields.DataController.Values[i, colCheck.Index] := not viewFields.DataController.Values[i, colCheck.Index];
  end;
end;

procedure TfAddFields.btnSelectClick(Sender: TObject);
var
  i, j: Integer;
  ParamName, RecParamName, FieldName: string;
  SameParamNameAlreadySelected: Boolean;
begin
  for i := 0 to viewFields.DataController.RecordCount - 1 do
  begin
    if viewFields.DataController.Values[i, colCheck.Index] = False then
    begin
      ParamName := viewFields.DataController.Values[i, colParamName.Index];
      FieldName := viewFields.DataController.Values[i, colName.Index];
      SameParamNameAlreadySelected := False;
      for j := 0 to viewFields.DataController.RecordCount - 1 do
      begin
        if (j <> i) and
           (viewFields.DataController.Values[j, colCheck.Index] = True) then
        begin
          RecParamName := viewFields.DataController.Values[j, colParamName.Index];
          if ParamName = RecParamName then
          begin
            SameParamNameAlreadySelected := True;
            break;
          end;
        end;
      end;
      if SameParamNameAlreadySelected then
        ShowMessage(Format('%s parameter is selected before. It can not be selected again for the %s!', 
                    [ParamName, FieldName]))
      else
        viewFields.DataController.Values[i, colCheck.Index] := True;
    end;
  end;
end;

procedure TfAddFields.btnUnselectClick(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to viewFields.DataController.RecordCount - 1 do
    viewFields.DataController.Values[I, colCheck.Index] := False
end;

procedure TfAddFields.colCheckPropertiesValidate(Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var
  ParamName: string;
begin
  if DisplayValue = True then
  begin
    ParamName := viewFields.DataController.Values[viewFields.DataController.FocusedRecordIndex, colParamName.Index];
    try
      CheckDuplicateParamName(ParamName);      
    except
      on e: Exception do
      begin
        Error := True;
        ErrorText := e.Message;
      end;
    end;
  end;
end;

procedure TfAddFields.colParamNamePropertiesValidate(Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if viewFields.DataController.Values[viewFields.DataController.FocusedRecordIndex, colCheck.Index] = True then
  begin
    try
      CheckDuplicateParamName(VarToStr(DisplayValue));      
    except
      on e: Exception do
      begin
        Error := True;
        ErrorText := e.Message;
      end;
    end;
  end;
end;

class function TfAddFields.Run(AFieldsChecks: IList<Tuple<string, string, Boolean>>): Boolean;
var
  Form: TfAddFields;
  I: Integer;
begin
  Form := TfAddFields.Create(nil);
  try
    Form.viewFields.DataController.RecordCount := AFieldsChecks.Count;
    for I := 0 to AFieldsChecks.Count - 1 do
    begin
      Form.viewFields.DataController.Values[I, Form.colCheck.Index] := AFieldsChecks[I].Value3;
      Form.viewFields.DataController.Values[I, Form.colParamName.Index] := AFieldsChecks[I].Value2;
      Form.viewFields.DataController.Values[I, Form.colName.Index] := AFieldsChecks[I].Value1;
    end;
    Result := Form.ShowModal = mrOk;
    if Result then
      for I := 0 to AFieldsChecks.Count - 1 do
        AFieldsChecks[I] := Tuple.Create<string, string, Boolean>
        (Form.viewFields.DataController.Values[I,Form.colName.Index],
         Form.viewFields.DataController.Values[I,Form.colParamName.Index],
         Form.viewFields.DataController.Values[I, Form.colCheck.Index]);
  finally
    Form.Free;
  end;
end;

procedure TfAddFields.CheckDuplicateParamName(const ParamName: string);
var
  i: Integer;
  RecParamName: string;
begin
  for i := 0 to viewFields.DataController.RecordCount - 1 do
  begin
    if (i <> viewFields.DataController.FocusedRecordIndex) and (viewFields.DataController.Values[I, colCheck.Index] = True) then
    begin
      RecParamName := viewFields.DataController.Values[I, colParamName.Index];
      if ParamName = RecParamName then
        raise Exception.Create('You can not select more than one field with the same parameter name!');
    end;
  end;
end;

end.
